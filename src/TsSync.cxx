//
// TsSync.cxx
// Timestamp synchronization
// K.Olchanski
//

#include <stdio.h>
#include <math.h>
#include <assert.h> // assert()
#include <time.h> // time()

#include "TsSync.h"

TsSyncEntry::TsSyncEntry(uint32_t xts, int xepoch, double xtime) // ctor
      : ts(xts), epoch(xepoch), time(xtime)
{
}

TsSyncModule::TsSyncModule() // ctor
{
}

void TsSync::Print(unsigned imodule) const
{
   printf("ts 0x%08x, prev 0x%08x, first 0x%08x, offset %f, time %f %f, diff %f, buf %d", fModules[imodule].fLastTs, fModules[imodule].fPrevTs, fModules[imodule].fFirstTs, fModules[imodule].fOffsetSec, fModules[imodule].fLastTimeSec, fModules[imodule].fPrevTimeSec, fModules[imodule].fLastTimeSec-fModules[imodule].fPrevTimeSec, (int)fBuf[imodule].size());
}

void TsSync::DumpBuf(unsigned imodule) const
{
   for (unsigned i=0; i<fBuf[imodule].size(); i++) {
      printf("XXX TsSyncEntry %d: 0x%08x %d %f\n",
             i,
             fBuf[imodule][i].ts,
             fBuf[imodule][i].epoch,
             fBuf[imodule][i].time);
   }
}

double TsSyncModule::GetTime(uint32_t ts, int epoch) const
{
   // must do all computations in double precision to avoid trouble with 32-bit timestamp wraparound.
   //return ts/fFreqHz - fFirstTs/fFreqHz + fOffsetSec + epoch*2.0*0x80000000/fFreqHz;
   return ts/fFreqHz - fFirstTs/fFreqHz + fOffsetSec + epoch*fEpochTs/fFreqHz;
}

bool TsSyncModule::IsDupe(uint32_t ts) const
{
   // ignore duplicate timestamps
   if (ts == fLastTs)
      return true;

   // ignore duplicate timestamps
   if (ts+1 == fLastTs)
      return true;

   // ignore duplicate timestamps
   if (ts == fLastTs+1)
      return true;

   // ignore duplicate timestamps
   if (ts+2 == fLastTs)
      return true;

   // ignore duplicate timestamps
   if (ts == fLastTs+2)
      return true;

   return false;
}

int TsSyncModule::NextEpoch(uint32_t ts)
{
   if (fFirstTs == 0) {
      fFirstTs = ts;
   }

   if (IsDupe(ts)) {
      return fEpoch;
   }

   fPrevTs = fLastTs;
   fPrevTimeSec = fLastTimeSec;
   fLastTs = ts;
   
   if (ts < fPrevTs) {
      //uint32_t diff0 = ts - fPrevTs;
      //uint32_t diff1 = fPrevTs - ts;
      //if (diff0 > diff1) {
      //   if (fModule == 1) {
      //   printf("module %d, ts wraparound 0x%08x -> 0x%08x epoch %d, diff 0x%08x 0x%08x, time %.6f, out of order\n", fModule, fPrevTs, ts, fEpoch, diff0, diff1, GetTime(ts, fEpoch));
      //   }
      //} else {
      //   printf("module %d, ts wraparound 0x%08x -> 0x%08x epoch %d, diff 0x%08x 0x%08x, time %.6f\n", fModule, fPrevTs, ts, fEpoch, diff0, diff1, GetTime(ts, fEpoch+1));
      fEpoch += 1;
      //}
   }
   
   // must do all computations in double precision to avoid trouble with 32-bit timestamp wraparound.
   fLastTimeSec = GetTime(fLastTs, fEpoch); // fLastTs/fFreqHz + fOffsetSec + fEpoch*2.0*0x80000000/fFreqHz;
   
   return fEpoch;
}

#if 0
bool TsSyncModule::Add(uint32_t ts)
{
   if (fFirstTs == 0) {
      fFirstTs = ts;
   }

   if (IsDupe(ts)) {
      return false;
   }

   fPrevTs = fLastTs;
   fPrevTimeSec = fLastTimeSec;
   fLastTs = ts;
   
   if (ts < fPrevTs) {
      //uint32_t diff0 = ts - fPrevTs;
      //uint32_t diff1 = fPrevTs - ts;
      //if (diff0 > diff1) {
      //   if (fModule == 1) {
      //   printf("module %d, ts wraparound 0x%08x -> 0x%08x epoch %d, diff 0x%08x 0x%08x, time %.6f, out of order\n", fModule, fPrevTs, ts, fEpoch, diff0, diff1, GetTime(ts, fEpoch));
      //   }
      //} else {
      //   printf("module %d, ts wraparound 0x%08x -> 0x%08x epoch %d, diff 0x%08x 0x%08x, time %.6f\n", fModule, fPrevTs, ts, fEpoch, diff0, diff1, GetTime(ts, fEpoch+1));
      fEpoch += 1;
      //}
   }
   
   // must do all computations in double precision to avoid trouble with 32-bit timestamp wraparound.
   fLastTimeSec = GetTime(fLastTs, fEpoch); // fLastTs/fFreqHz + fOffsetSec + fEpoch*2.0*0x80000000/fFreqHz;
   
   return true;
}
#endif

void TsSync::Retime(unsigned imodule)
{
   for (unsigned i=0; i<fBuf[imodule].size(); i++) {
      fBuf[imodule][i].time = fModules[imodule].GetTime(fBuf[imodule][i].ts, fBuf[imodule][i].epoch);
   }
}

double TsSync::GetDt(unsigned imodule, unsigned j)
{
   assert(j>0);
   assert(j<fBuf[imodule].size());
   return fBuf[imodule][j].time - fBuf[imodule][j-1].time;
}

unsigned TsSync::FindDt(unsigned imodule, double dt)
{
   //printf("FindDt: fBuf.size %d\n", (int)fBuf.size());
   assert(fBuf[imodule].size() > 0);
   for (unsigned j=fBuf[imodule].size()-1; j>1; j--) {
      double jdt = GetDt(imodule, j);
      double jdiff = dt - jdt;
      double rdiff = (dt-jdt)/dt;
      double ajdiff = fabs(jdiff);
      double ardiff = fabs(rdiff);

      //printf("size %d, buf %d, jdt %f, dt %f, diff %.0f ns, diff %f\n", (int)fBuf.size(), j, jdt, dt, jdiff*1e9, rdiff);

      if (fConfEpsSec > 0 && ajdiff < fConfEpsSec) {
         if (ajdiff > fMaxDtSec[imodule]) {
            //printf("update max dt %f %f, eps %f\n", ajdiff*1e9, fMaxDtSec*1e9, fEpsSec*1e9);
            fMaxDtSec[imodule] = ajdiff;
         }
         if (ardiff > fMaxRelDt[imodule])
            fMaxRelDt[imodule] = ardiff;
         //printf("found %d %f\n", j, jdt);
         return j;
      }

      if (fConfRelEps > 0 && ardiff < fConfRelEps) {
         if (ajdiff > fMaxDtSec[imodule])
            fMaxDtSec[imodule] = ajdiff;
         if (ardiff > fMaxRelDt[imodule])
            fMaxRelDt[imodule] = ardiff;
         //printf("found %d %f\n", j, jdt);
         return j;
      }
   }
   //printf("not found!\n");
   return 0;
}

static time_t gKludgeFirst = 0;

TsSync::TsSync() // ctor
{
   gKludgeFirst = 0;
}

TsSync::~TsSync() // dtor
{
}

void TsSync::Configure(unsigned i, double epoch_ts, double freq_hz)
{
   // grow the array if needed
   if (i >= fModules.size()) {
      fModules.resize(i+1);
      fBuf.resize(i+1);
      fMaxDtSec.resize(i+1);
      fMaxRelDt.resize(i+1);
   }

   fModules[i].fModule = i;
   fModules[i].fEpochTs = epoch_ts;
   fModules[i].fFreqHz = freq_hz;
   //fModules[i].fEpsSec = eps_sec;
   //fModules[i].fRelEps = rel_eps;
   //fModules[i].fBufMax = buf_max;
}

bool TsSync::TrySync(unsigned ii, unsigned i)
{
   // try to sync slot ii to slot i

   unsigned ntry = 3;
   unsigned jj = fBuf[ii].size()-1;
   
   double tt = GetDt(ii, jj);
   
   unsigned j = FindDt(i, tt);

   //printf("TsSync::CheckSync: module %d buf %d, dt %f with module %d buf %d\n", ii, jj, tt, i, j);
   
   if (j == 0) {
      if (jj < 2) {
         return false;
      }

      jj -= 1;
      tt = GetDt(ii, jj);
      j = FindDt(i, tt);

      //printf("TsSync::CheckSync: module %d buf %d, dt %f with module %d buf %d (2nd try)\n", ii, jj, tt, i, j);
      
      if (j == 0) {
         return false;
      }
   }
   
   // demand a few more good matches
   for (unsigned itry=1; itry<=ntry; itry++) {
      if (jj-itry == 0)
         return false;
      if (j-itry == 0)
         return false;
      double xtt = GetDt(ii, jj-itry);
      double xt  = GetDt(i, j-itry);
      double dxt = xt - xtt;
      double rdxt = dxt/xt;

      if (fConfEpsSec > 0 && fabs(dxt) > fConfEpsSec) {
         return false;
      }

      if (fConfRelEps > 0 && fabs(rdxt) > fConfRelEps) {
         return false;
      }
   }

   fModules[ii].fSyncedWith = i;

   // check for sync loop
   if (fModules[i].fSyncedWith >= 0)
      fModules[ii].fSyncedWith = fModules[i].fSyncedWith;
   
   double off = fBuf[i][j].time - fBuf[ii][jj].time;
   fModules[ii].fOffsetSec = off;

   Retime(ii);

   printf("TsSync: module %d with %d events synced with module %d with %d events, offset %f\n", ii, jj, i, j, off);

   if (fTrace) {
      Dump();
   }

   return true;
}

bool TsSync::TrySync(unsigned inew)
{
   // already synchronized?
   if (fModules[inew].fSyncedWith >= 0)
      return false;

   // too few events?
   if (fBuf[inew].size() < fConfSyncMinEvents)
      return false;
   
   //time_t now = time(NULL);
   //
   //if (gKludgeFirst == 0)
   //   gKludgeFirst = now;
   //
   //if (now - gKludgeFirst < 5) {
   //   //printf("TsSync: delay checking sync %d!\n", (int)(now - gKludgeFirst));
   //   return;
   //}
   //
   //printf("TsSync: delay %d!\n", (int)(now - gKludgeFirst));

   // find module that has the most events

   bool did_sync = false;
   for (unsigned i=0; i<fModules.size(); i++) {
      // do not try to sync with self
      if (i == inew)
         continue;
      if (fBuf[i].size() >= fConfSyncMinEvents) {
         if (1||fTrace)
            printf("module %d try sync with module %d\n", inew, i);
         did_sync = TrySync(inew, i);
         break;
      }
   }

   if (!did_sync)
      return false;
   
   if (fTrace) {
      Dump();
   }

   return did_sync;
}

void TsSync::Count()
{
   unsigned modules_no_data = 0;
   unsigned modules_no_sync = 0;
   for (unsigned i=0; i<fModules.size(); i++) {
      if (fBuf[i].empty()) {
         modules_no_data++;
      } else if (fModules[i].fSyncedWith < 0) {
         modules_no_sync++;
      }
   }

   fCountNoData = modules_no_data;
   fCountNoSync = modules_no_sync;
}

void TsSync::Sync(unsigned i, uint32_t ts, int epoch)
{
   //if (0 && fTrace) {
   //   printf("Add %d, ts 0x%08x\n", i, ts);
   //}

   //bool added = fModules[i].Add(ts);
   //bool dupe = fModules[i].IsDupe(ts);

   //printf("dupe slot %d, ts 0x%08x epoch %d\n", i, ts, epoch);

   //if (dupe)
   //   return;

   printf("module %d added %d max %d\n", i, (int)fBuf[i].size(), fConfSyncMaxEvents);
   
   fBuf[i].push_back(TsSyncEntry(ts, epoch, fModules[i].GetTime(ts, epoch)));
                     
   if (fBuf[i].size() >= fConfSyncMaxEvents) {
      fSyncFailed = true;
      PostSync();
      return;
   }
   
   bool did_sync = TrySync(i);
   
   if (1||did_sync) {
      Count();
      
      if (1||fTrace)
         printf("TsSync: total modules: %d, no data: %d, no sync: %d\n", (int)fModules.size(), fCountNoData, fCountNoSync);
      
      if (fCountNoData == 0 && fCountNoSync == 0) {
         printf("TsSync: all modules have data, all modules synchronized, synchronization completed\n");
         fSyncOk = true;
         PostSync();
      }
   }
}

void TsSync::PostSync()
{
   assert(fSyncOk||fSyncFailed);

   // sync is done
   fSyncActive = false;

   // mark modules without data as dead
   for (unsigned i=0; i<fModules.size(); i++) {
      if (fBuf[i].empty()) {
         fModules[i].fDead = true;
      }
   }

   Count();

   if (fCountNoSync > 0) {
      fSyncOk = false;
      fSyncFailed = true;
      printf("TsSync: synchronization failed, %d dead modules, %d not synchronized modules\n", fCountNoData, fCountNoSync);
   } else {
      fSyncOk = true;
      fSyncFailed = false;
      if (fCountNoData > 0) {
         printf("TsSync: synchronization completed, %d dead modules\n", fCountNoData);
      } else {
         printf("TsSync: synchronization completed\n");
      }
   }

   fBuf.clear();
}

void TsSync::Print() const
{
   printf("TsSync: ");
   printf("sync_actiove: %d, ", fSyncActive);
   printf("sync_ok: %d, ", fSyncOk);
   printf("sync_failed: %d, ", fSyncFailed);
}

void TsSync::Dump() const
{
   unsigned min = 0;
   unsigned max = 0;
   
   for (unsigned i=0; i<fModules.size(); i++) {
      unsigned s = fBuf[i].size();
      if (s > 0) {
         if (min == 0)
            min = s;
         if (s < min)
            min = s;
         if (s > max)
               max = s;
      }
   }

   for (unsigned j=1; j<max; j++) {
      printf("buf %2d: ", j);
      for (unsigned i=0; i<fModules.size(); i++) {
         if (j<fBuf[i].size()) {
            double dt = fBuf[i][j].time - fBuf[i][j-1].time;
            printf(" %10.6f", dt);
         } else {
            printf(" %10s", "-");
         }
      }
      printf("\n");
   }
   
   printf("buf %2d: ", -1);
   for (unsigned i=0; i<fModules.size(); i++) {
      printf(" %10d", fModules[i].fSyncedWith);
   }
   printf(" (synced with)\n");
   printf("buf %2d: ", -2);
   for (unsigned i=0; i<fModules.size(); i++) {
      printf(" %10.0f", fMaxDtSec[i]*1e9);
   }
   printf(" (max mismatch, ns)\n");
   printf("buf %2d: ", -3);
   for (unsigned i=0; i<fModules.size(); i++) {
      printf(" %10.1f", fMaxRelDt[i]*1e6);
   }
   printf(" (max relative mismatch, ns/ns*1e6)\n");
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
