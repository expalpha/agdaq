#!/usr/bin/perl -w

my $help = grep(/-h/, @ARGV);
my $dryrun = grep(/-n/, @ARGV);
my $factory = grep(/-f/, @ARGV);
my $program = grep(/-p/, @ARGV);
my $testfw    = grep(/-t/, @ARGV);

$help = 1 unless @ARGV > 0;

die "Usage: $0 [-h] [-n] [-f] [-p] [-t] {all|adc01 adc02 ...}\n" if $help;

#my $fw = "/home/agdaq/online/firmware/alpha16/20180327-ko/alpha16_one_page_auto.rpd";
#my $fw = "/home/agdaq/online/firmware/alpha16/20180411-ko/alpha16_one_page_auto.rpd";
#my $fw = "/home/agdaq/online/firmware/alpha16/20180428-ko/alpha16_one_page_auto.rpd";
#my $fw = "/home/agdaq/online/firmware/alpha16/20180504-ko/alpha16_one_page_auto.rpd";
#my $fw = "/home/agdaq/online/firmware/alpha16/20180511-ko/alpha16_one_page_auto.rpd";
#my $fw = "/home/agdaq/online/firmware/alpha16/20180524-ko/alpha16_one_page_auto.rpd";
#my $fw = "/home/agdaq/online/firmware/alpha16/20180927-ko/alpha16_one_page_auto.rpd";
#my $fw = "/home/agdaq/online/firmware/alpha16/20201112-ko/alpha16_one_page_auto.rpd";
my $fw = "/home/agdaq/online/firmware/alpha16/20201122-ko/alpha16_one_page_auto.rpd";

if ($testfw) {
  #$fw = "/home/agdaq/online/firmware/git/adc_firmware/bin/alpha16_one_page_auto.rpd";
  $fw = "/home/olchansk/git/adc_firmware/bin/alpha16_one_page_auto.rpd";
}

die "Cannot read RPD file $fw: $!\n" if ! -r $fw;

foreach my $x (@ARGV) {
  next if $x =~ /^-/;

  if ($x eq "all") {
    update($fw, "adc01");
    update($fw, "adc02");
    update($fw, "adc03");
    update($fw, "adc04");
    update($fw, "adc05");
    update($fw, "adc06");
    update($fw, "adc07");
    update($fw, "adc08");
    update($fw, "adc09");
    update($fw, "adc10");
    update($fw, "adc11");
    update($fw, "adc12");
    update($fw, "adc13");
    update($fw, "adc14");
    update($fw, "adc15");
    update($fw, "adc16");
    update($fw, "adc17");
    update($fw, "adc18");
    update($fw, "adc19");
    update($fw, "adc20");
  } else {
    print "update adc [$x]\n";
    if ($factory) {
      if ($program) {
	print "UPDATING FACTORY IMAGE OF [$x]. ARE YOU SURE? PRESS CTRL-C TO ABORT!\n";
	sleep 10;
      }
      update_factory($fw, $x);
    } else {
      update($fw, $x);
    }
  }
}

exit 0;

#sub update
#{
#   my $fw = shift @_;
#   my $adc = shift @_;
#   my $cmd = sprintf("esper-tool -v upload -f %s http://%s update file_rpd", $fw, $adc);
#   print $cmd,"\n";
#   system $cmd." &";
#}

sub update_factory
{
   my $fw = shift @_;
   my $adc = shift @_;

   die "Hostname \"$adc\" is not adcNN!" unless $adc =~ /^adc\d+/;

   if ($program) {
     print "programming factory page [$adc] ...\n";
     die "ADC FACTORY PAGE UPDATE IS DISABLED HERE. PLEASE COMMENT-OUT THIS LINE TO ENABLE IT!";
     my $cmd = sprintf("esper-tool -v write -d true http://%s update allow_write", $adc);
     print $cmd,"\n";
     system $cmd unless $dryrun;
     $cmd = sprintf("esper-tool -v write -d true http://%s update allow_factory_write", $adc);
     print $cmd,"\n";
     system $cmd unless $dryrun;
   } else {
     print "verifying factory page [$adc] ...\n";
     my $cmd = sprintf("esper-tool -v write -d false http://%s update allow_write", $adc);
     print $cmd,"\n";
     system $cmd unless $dryrun;
     $cmd = sprintf("esper-tool -v write -d false http://%s update allow_factory_write", $adc);
     print $cmd,"\n";
     system $cmd unless $dryrun;
   }

   $cmd = sprintf("esper-tool -v upload -f %s http://%s update factory_rpd", $fw, $adc);
   print $cmd,"\n";
   system $cmd." &" unless $dryrun;
}

sub update
{
   my $fw = shift @_;
   my $adc = shift @_;

   die "Hostname \"$adc\" is not adcNN!" unless $adc =~ /^adc\d+/;

   if ($program) {
     print "programming user page [$adc] ...\n";
     my $cmd = sprintf("esper-tool -v write -d true http://%s update allow_write", $adc);
     print $cmd,"\n";
     system $cmd unless $dryrun;
   } else {
     print "verifying user page [$adc] ...\n";
     my $cmd = sprintf("esper-tool -v write -d false http://%s update allow_write", $adc);
     print $cmd,"\n";
     system $cmd unless $dryrun;
   }

   $cmd = sprintf("esper-tool -v upload -f %s http://%s update file_rpd", $fw, $adc);
   print $cmd,"\n";
   system $cmd." &" unless $dryrun;
}

# end
