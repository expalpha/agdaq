// grifc.cxx
//
// ALPHA-T command line communication tool
//
// K.Olchanski TRIUMF 2018
//

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <signal.h>
#include <math.h> // fabs()

#include <vector>
#include <map>
#include <mutex>
#include <thread>

#include "GrifComm.h"

int main(int argc, char* argv[])
{
   setbuf(stdout, NULL);
   setbuf(stderr, NULL);

   signal(SIGPIPE, SIG_IGN);

   //thread_test();

   TMFE* mfe = TMFE::Instance();

   TMFeResult r = mfe->Connect("grifc");
   if (r.error_flag) {
      printf("Cannot connect, bye.\n");
      return 1;
   }

   std::string hostname = "trg01";

   GrifComm* grifc = new GrifComm();
   grifc->mfe = mfe;
   grifc->fHostname = hostname;
   grifc->OpenSockets();
   grifc->fFailed = false;

   if (argc > 1) {
      printf("argv[1] %s\n", argv[1]);
      if (strcmp(argv[1], "read")==0) {
         int addr = strtoul(argv[2], NULL, 0);
         uint32_t data = 0;
         bool ok = grifc->read_param(addr, 0xFFFF, &data);
         printf("grifc: read_param 0x%x, data 0x%08x (%d), ok %d\n", addr, data, data, ok);
      } else if (strcmp(argv[1], "write")==0) {
         int addr = strtoul(argv[2], NULL, 0);
         uint32_t wdata = strtoul(argv[3], NULL, 0);
         bool ok = grifc->write_param(addr, 0xFFFF, wdata);
         printf("grifc: write_param 0x%x, data 0x%08x (%d), ok %d\n", addr, wdata, wdata, ok);
      } else if (strcmp(argv[1], "reboot")==0) {
         printf("grifc: will reboot grifc in 5 seconds... press ctrl-C to abort.\n");
         ::sleep(5);
         bool ok = true;
         uint32_t scratch_write = 0x12345678;
         ok &= grifc->write_param(0x8, 0xFFFF, scratch_write);
         uint32_t scratch_read = 0;
         ok &= grifc->read_param(0x8, 0xFFFF, &scratch_read);
         uint32_t fwrev = 0;
         ok &= grifc->read_param(0x1F, 0xFFFF, &fwrev);
         printf("grifc: scratch write 0x%08x read 0x%08x, firmware revision 0x%08x, ok %d\n", scratch_write, scratch_read, fwrev, ok);
         printf("grifc: sending reboot command...\n");
         grifc->write_param(0x42, 0xFFFF, ~fwrev);
         printf("grifc: waiting...\n");
         ::sleep(5);
         grifc->fFailed = false;
         uint32_t scratch_read1 = 0;
         ok &= grifc->read_param(0x8, 0xFFFF, &scratch_read1);
         uint32_t fwrev1 = 0;
         ok &= grifc->read_param(0x1F, 0xFFFF, &fwrev1);
         printf("grifc: scratch write 0x%08x, read 0x%08x -> 0x%08x, firmware revision 0x%08x -> 0x%08x, ok %d\n", scratch_write, scratch_read, scratch_read1, fwrev, fwrev1, ok);
         if (scratch_read1 != 0) {
            printf("ERROR: grifc fpga did not reboot!\n");
         }
      } else if (strcmp(argv[1], "cbfifo")==0) {
         bool ok = true;
         while (1) {
            uint32_t cb_fifo_status = 0;
            ok &= grifc->read_param(0x70, 0xFFFF, &cb_fifo_status);
            printf("grifc: cb_fifo_status 0x%08x, ok %d\n", cb_fifo_status, ok);
            int nw = cb_fifo_status & 0x00FFFFFF;
            if (nw == 0) {
               ::sleep(1);
               continue;
            }
            for (int i=0; i<nw; i++) {
               uint32_t cb_fifo_data = 0;
               ok &= grifc->read_param(0x71, 0xFFFF, &cb_fifo_data);
               printf("grifc: %5d: 0x%08x, ok %d\n", i, cb_fifo_data, ok);
            }
         }
      } else if (strcmp(argv[1], "commtest")==0) {
         // communication test
         printf("communication test!\n");
         int addr = 0x23; // pulser period register
         uint32_t v = 0;
         while (!mfe->fShutdownRequested) {
            std::string errstr;
            bool ok = grifc->try_write_param(addr, 0xFFFF, v, &errstr);
            if (!ok) printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d), error: %s\n", addr, ok, v, v, errstr.c_str());
            uint32_t r = 0;
            ok = grifc->read_param(addr, 0xFFFF, &r);
            if (r != v) {
               printf("mismatch: write 0x%08x, read 0x%08x\n", v, r);
            }
            v++;
         }
      } else if (strcmp(argv[1], "commtestw")==0) {
         // communication test
         printf("communication write test!\n");
         int addr = 0x23; // pulser period register
         uint32_t v = 0;
         while (!mfe->fShutdownRequested) {
            std::string errstr;
            bool ok = grifc->try_write_param(addr, 0xFFFF, v, &errstr);
            if (!ok) printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d), error: %s\n", addr, ok, v, v, errstr.c_str());
            //uint32_t r = 0;
            //ok = grifc->read_param(addr, 0xFFFF, &r);
            //if (r != v) {
            //   printf("mismatch: write 0x%08x, read 0x%08x\n", v, r);
            //}
            v++;
         }
      }
   } else if (0) {
      printf("save grifc flash!\n");

      bool ok = true;

      ok &= grifc->flash_status();

      ok &= grifc->flash_read_start(0, NULL);

      FILE *fp = fopen("grifc_flash.dat", "w");

      for (uint32_t addr = 0; addr < 0x2000000; addr++) {
         if ((addr & 0xFFF) == 0) printf("0x%08x.", addr);
         uint16_t data;
         ok &= grifc->flash_read_next(addr, &data);
         fwrite(&data, 2, 1, fp);
      }

      printf("\n");

      ok &= grifc->flash_read_end();

      ok &= grifc->flash_status();
      ok &= grifc->flash_off();

      fclose(fp);

   } else if (1) {
      printf("read grifc flash!\n");

      bool ok = true;

      ok &= grifc->flash_status();

      int col = 0;
      uint32_t start_addr = 0x10000;
      
      ok &= grifc->flash_read_start(start_addr, NULL);

      for (uint32_t addr = start_addr; addr < start_addr + 64; addr++) {
         uint16_t data;

         //ok &= grifc->flash_read_word(addr, &data);
         ok &= grifc->flash_read_next(addr, &data);

         printf("0x%04x", data);

         col++;
         if (col == 8) { col = 0; printf("\n"); } else printf(" ");
      }

      ok &= grifc->flash_read_end();

      ok &= grifc->flash_status();
      ok &= grifc->flash_off();

#if 0
read grifc flash!
grifc flash: control: 0x00000000, a_out: 0x0001003f, d_out: 0x00000000, status 0x0000ffff, a 0x07ffffff, ok 1
0x0001 0x042d 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff
0xffff 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff
0xffff 0xffff 0xf66a 0xf6ff 0xf6f7 0xf3f7 0xd3fa 0xfbfb
0xfbf8 0xd8d8 0xf8fd 0xfcf8 0xf8ff 0xfbf8 0xf8fc 0xf8fd
0xf8fc 0xfcfd 0xfcf8 0xeaf9 0xeeeb 0xfefa 0x5aee 0xff4a
0xffff 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff
0xffff 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff
0xffff 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff 0xffff
grifc flash: control: 0x00000000, a_out: 0x0001003f, d_out: 0x00000000, status 0x0000ffff, a 0x07ffffff, ok 1
#endif

   } else if (1) {
      printf("flash access test!\n");

#if 0
      wire [31:0] flash_control;
      wire [31:0] flash_status;
      
      assign flash_oe_a       = flash_control[0];
      assign flash_oe_d       = flash_control[1];
      assign flash_oe_ce0n    = flash_control[2];
      assign flash_oe_ce1n    = flash_control[3];
      assign flash_oe_oen     = flash_control[4];
      assign flash_oe_byten   = flash_control[5];
      assign flash_oe         = flash_control[15];
      assign flash_busyn_out  = ~flash_control[16];
      assign flash_byten_out  = ~flash_control[17];
      assign flash_ce0n_out   = ~flash_control[18];
      assign flash_ce1n_out   = ~flash_control[19];
      assign flash_oen_out    = ~flash_control[20];
      assign flash_resetn_out = ~flash_control[21];
      assign flash_wen_out    = ~flash_control[22];
      assign flash_wpn_out    = ~flash_control[23];
      
      assign flash_status[15:0] = flash_d_in;
      assign flash_status[16] = ~flash_busyn_in;
      assign flash_status[17] = ~flash_byten_in;
      assign flash_status[18] = ~flash_ce0n_in;
      assign flash_status[19] = ~flash_ce1n_in;
      assign flash_status[20] = ~flash_oen_in;
      assign flash_status[21] = ~flash_resetn_in;
      assign flash_status[22] = ~flash_wen_in;
      assign flash_status[23] = ~flash_wpn_in;
      assign flash_status[31:24] = 0;

      //14'h050:  param_out <= {2'h3, par_id, chan,     flash_control_out };
      //14'h051:  param_out <= {2'h3, par_id, chan,     flash_a_out };
      //14'h052:  param_out <= {2'h3, par_id, chan,     flash_d_out };
      //14'h053:  param_out <= {2'h3, par_id, chan,     flash_status };
      //14'h054:  param_out <= {2'h3, par_id, chan,     flash_a };
#endif
      uint32_t flash_control_out = 0;
      uint32_t flash_a_out = 0;
      uint32_t flash_d_out = 0;
      uint32_t flash_status = 0;
      uint32_t flash_a = 0;

      bool ok = true;

      ok &= grifc->read_param(0x50, 0xFFFF, &flash_control_out);
      ok &= grifc->read_param(0x51, 0xFFFF, &flash_a_out);
      ok &= grifc->read_param(0x52, 0xFFFF, &flash_d_out);
      ok &= grifc->read_param(0x53, 0xFFFF, &flash_status);
      ok &= grifc->read_param(0x54, 0xFFFF, &flash_a);

      printf("flash: control: 0x%08x, a_out: 0x%08x, d_out: 0x%08x, status 0x%08x, a 0x%08x, ok %d\n",
             flash_control_out,
             flash_a_out,
             flash_d_out,
             flash_status,
             flash_a,
             ok);

      int col = 0;
      uint32_t start_addr = 0x10000;
      for (uint32_t addr = start_addr; addr < start_addr + 64; addr++) {
         ok &= grifc->write_param(0x51, 0xFFFF, addr);
         //ok &= grifc->write_param(0x50, 0xFFFF, 1|(1<<5)|(1<<17));
         ok &= grifc->write_param(0x50, 0xFFFF, 1);
         ok &= grifc->write_param(0x50, 0xFFFF, 1|(1<<2)|(1<<4)|(1<<18)|(1<<20));

         ok &= grifc->read_param(0x53, 0xFFFF, &flash_status);

         uint16_t data = flash_status & 0xFFFF;

         //printf("address 0x%08x status 0x%08x\n", addr, flash_status);
         
         printf("0x%04x", data);

         col++;
         if (col == 8) { col = 0; printf("\n"); } else printf(" ");
         
         //for (unsigned i=1; i<128; i++) {
         //   ok &= grifc->write_param(0x51, 0xFFFF, addr + i);
         //   ok &= grifc->read_param(0x53, 0xFFFF, &flash_status);
         //   //if ((flash_status & 0xFFFF) != 0xFFFF) {
         //   printf("address 0x%08x + 0x%02x status 0x%08x\n", addr, i, flash_status);
         //   //}
         //}

         ok &= grifc->write_param(0x50, 0xFFFF, 0);
      }

      printf("\n");

      ok &= grifc->read_param(0x50, 0xFFFF, &flash_control_out);
      ok &= grifc->read_param(0x51, 0xFFFF, &flash_a_out);
      ok &= grifc->read_param(0x52, 0xFFFF, &flash_d_out);
      ok &= grifc->read_param(0x53, 0xFFFF, &flash_status);
      ok &= grifc->read_param(0x54, 0xFFFF, &flash_a);

      printf("flash: control: 0x%08x, a_out: 0x%08x, d_out: 0x%08x, status 0x%08x, a 0x%08x, ok %d\n",
             flash_control_out,
             flash_a_out,
             flash_d_out,
             flash_status,
             flash_a,
             ok);

      // turn off the flash interface
      ok &= grifc->write_param(0x50, 0xFFFF, 0);
      //ok &= grifc->write_param(0x51, 0xFFFF, 0);
      //ok &= grifc->write_param(0x52, 0xFFFF, 0);
      
   } else if (0) {
      int addr = 0x37;
      uint32_t v = 0x80000000;
      bool ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
      v = 0;
      ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);

      v = 0x40000000;
      ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);

      FILE *fp = fopen("test.mlu", "r");

      while (1) {
         char buf[256];
         char*s = fgets(buf, sizeof(buf), fp);
         if (!s)
            break;

         int vaddr = strtoul(s, &s, 0);
         int value = strtoul(s, &s, 0);

         printf("addr 0x%08x, value %d, read: %s", addr, value, buf);

         //v = 0x40010101;
         v = 0x40000000;
         v |= (value<<16);
         v |= vaddr;
         ok = grifc->write_param(addr, 0xFFFF, v);
         printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
      }

      fclose(fp);

      v = 0;
      ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);

   } else if (0) {
      int addr = 0x37;
      uint32_t v = 0x80000000;
      bool ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
      v = 0;
      ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
      v = 0x40000000;
      ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
      v = 0;
      ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
      v = 0x40010101;
      ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
      v = 0;
      ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
   } else if (0) {
      uint32_t v = 0;
      int addr = 0x1F;
      bool ok = grifc->read_param(addr, 0xFFFF, &v);

      printf("grifc: read addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
   } else {
      int addr = 0x37;
      uint32_t v = 0x1234;
      bool ok = grifc->write_param(addr, 0xFFFF, v);
      printf("grifc: write addr 0x%08x, ok %d, value 0x%08x (%d)\n", addr, ok, v, v);
   }

   mfe->Disconnect();

   return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
