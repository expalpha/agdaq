// feCdmCtrl.cxx
//
// MIDAS frontend to control the CDM
//

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <signal.h>
#include <math.h> // fabs()
#include <iostream>
#include <sstream>

#include <vector>
#include <map>
#include <mutex>
#include <thread>

#include <stdexcept> // std::out_of_range

#include "tmfe.h"

#include "KOtcp.h"

#include "midas.h"
#include "mjson.h"

#define GOOD_BUILD 1530315978

class EsperComm
{
public:
   std::string fName;
   KOtcpConnection* s = NULL;
   bool fFailed = false;
   std::string fFailedMessage;
   bool fVerbose = false;

public:
   double fLastHttpTime = 0;
   double fMaxHttpTime = 0;

public:
   EsperComm(const char* name, KOtcpConnection* tcp)
   {
      fName = name;
      s = tcp;
   }

   bool Write(TMFE* mfe, const char* mid, const char* vid, const char* json, bool binaryn=false, bool no_reply=false)
   {
      if (fFailed)
         return false;

      std::string url;
      url += "/write_var?";
      if (binaryn) {
         url += "binary=n";
         url += "&";
      }
      url += "mid=";
      url += mid;
      url += "&";
      url += "vid=";
      url += vid;
      //url += "&";
      //url += "offset=";
      //url += "0";

      //printf("URL: %s\n", url.c_str());
      std::vector<std::string> headers;

      if (no_reply) {
         KOtcpError e = s->HttpPost(headers, url.c_str(), json, NULL, NULL);

         if (e.error) {
            char msg[1024];
            sprintf(msg, "Write() error: HttpPost(write_var %s.%s) error %s", mid, vid, e.message.c_str());
            mfe->Msg(MERROR, "Write", "%s: %s", fName.c_str(), msg);
            fFailed = true;
            fFailedMessage = msg;
            return false;
         }

         return true;
      }

      std::vector<std::string> reply_headers;
      std::string reply_body;

      double t0 = mfe->GetTime();

      KOtcpError e = s->HttpPost(headers, url.c_str(), json, &reply_headers, &reply_body);

      double t1 = mfe->GetTime();
      fLastHttpTime = t1-t0;
      if (fLastHttpTime > fMaxHttpTime)
         fMaxHttpTime = fLastHttpTime;

      if (e.error) {
         char msg[1024];
         sprintf(msg, "Write() error: HttpPost(write_var %s.%s) error %s", mid, vid, e.message.c_str());
         mfe->Msg(MERROR, "Write", "%s: %s", fName.c_str(), msg);
         fFailed = true;
         fFailedMessage = msg;
         return false;
      }

      if (reply_body.find("error") != std::string::npos) {
         mfe->Msg(MERROR, "Write", "%s: AJAX write %s.%s value \"%s\" error: %s", fName.c_str(), mid, vid, json, reply_body.c_str());
         return false;
      }

#if 0
      printf("reply headers for %s:\n", url.c_str());
      for (unsigned i=0; i<reply_headers.size(); i++)
         printf("%d: %s\n", i, reply_headers[i].c_str());

      printf("json: %s\n", reply_body.c_str());
#endif

      return true;
   }

   std::string Read(TMFE* mfe, const char* mid, const char* vid, std::string* last_errmsg = NULL, bool do_msg = true)
   {
      if (fFailed){
         std::cerr << "Esper failure" <<std::endl;
         return std::string();
      }
      std::string url;
      url += "/read_var?";
      url += "mid=";
      url += mid;
      url += "&";
      url += "vid=";
      url += vid;
      url += "&";
      url += "offset=";
      url += "0";
      url += "&";
      url += "len=";
      url += "&";
      url += "len=";
      url += "0";
      url += "&";
      url += "dataOnly=y";

      // "/read_var?vid=elf_build_str&mid=board&offset=0&len=0&dataOnly=y"

      //printf("URL: %s\n", url.c_str());

      std::vector<std::string> headers;
      std::vector<std::string> reply_headers;
      std::string reply_body;

      double t0 = mfe->GetTime();

      KOtcpError e = s->HttpGet(headers, url.c_str(), &reply_headers, &reply_body);

      double t1 = mfe->GetTime();
      fLastHttpTime = t1-t0;
      if (fLastHttpTime > fMaxHttpTime)
         fMaxHttpTime = fLastHttpTime;

      if (e.error) {
         char msg[1024];
         sprintf(msg, "Read %s.%s HttpGet() error %s", mid, vid, e.message.c_str());
         if (!last_errmsg || e.message != *last_errmsg) {
            if (do_msg)
               mfe->Msg(MERROR, "Read", "%s: %s", fName.c_str(), msg);
            if (last_errmsg) {
               *last_errmsg = e.message;
            }
         }
         fFailed = true;
         fFailedMessage = msg;
         return "";
      }

#if 0
      printf("reply headers:\n");
      for (unsigned i=0; i<reply_headers.size(); i++)
         printf("%d: %s\n", i, reply_headers[i].c_str());

      printf("json: %s\n", reply_body.c_str());
#endif

#if 0
      if (strcmp(mid, "board") == 0) {
         printf("mid %s, vid %s, json: %s\n", mid, vid, reply_body.c_str());
      }
#endif

      if (reply_body.length()>0) {
         if (reply_body[0] == '{') {
            if (reply_body.find("{\"error")==0) {
               mfe->Msg(MERROR, "Read", "%s: Read %s.%s esper error %s", fName.c_str(), mid, vid, reply_body.c_str());
               return "";
            }
         }
      }

      return reply_body;
   }
};

class CdmCtrl : public TMFeEquipment
{
public: // settings and configuration
   EsperComm* fEsper = NULL;
   bool fVerbose = false;
   bool fMaster = false;
   bool fCdmHostname;

   std::vector<std::string> fVarNames =
      {
       "system:uptime",
       "cdm:esata_clk",
       "cdm:ext_clk",
       "cdm:esata_trg",
       "cdm:ext_trg",
       "lmk:pll1_ld",
       "lmk:pll2_ld",
       "lmk:ld1_counter",
       "lmk:ld2_counter",
       "lmk:holdover",
       "lmk:dac_value"
      };

   int fLmk_clkin_sel_mode = 0;

   const std::vector<std::string> fClkModes =
      {
         "internal",
         "esata",
         "external"
      };
   
public:
   CdmCtrl(const char* eqname, const char* eqfilename, bool master) // ctor
      : TMFeEquipment(eqname, eqfilename)
   {
      // configure the equipment here:

      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 0;
      fEqConfPeriodMilliSec = 30000;
      fEqConfLogHistory = 1;
      fEqConfWriteEventsToOdb = false;
      fEqConfEnablePoll = false; // enable polled equipment
      //fEqConfPollSleepSec = 0; // to create a "100% CPU busy" polling loop, set poll sleep time to zero
      fEqConfBuffer = "";
      fEqConfReadOnlyWhenRunning = false;

      fMaster = master;
   }

   int xatoi(const char* s)
   {
      if (s == NULL)
         return 0;
      else if (s[0]=='[')
         return atoi(s+1);
      else
         return atoi(s);
   }

   void HandleUsage()
   {
      printf("CdmCtrl::HandleUsage!\n");
   }

   TMFeResult HandleInit(const std::vector<std::string>& args)
   {
      printf("CdmCtrl::HandleInit!\n");

      EqSetStatus("Init...", "#FFFFFF");

      EqSetStatus("Connecting to CDM...", "#FFFFFF");

      TMFeResult err = ConnectEsper();
      while(err.error_flag){
         static int i = 0;
         if(++i > 4) break;
         fMfe->Msg(MERROR, "Init", "Connection failed: %s, retry: %d", err.error_message.c_str(), i);
         DisconnectEsper();
         fMfe->Yield(5);
         if (fMfe->fShutdownRequested) {
            return TMFeErrorMessage("shutdown requested");
         }

         err = ConnectEsper();
      }

      if(err.error_flag)
         return err;

      EqSetStatus("Connected to CDM...", "#FFFFFF");

      EqSetStatus("Writing settings to CDM...", "#FFFFFF");

      // https://daq00.triumf.ca/AgWiki/index.php/Daq#Chronobox_connections
      WriteEsper("cdm", "multi_sync", "2");

      // see https://daq00.triumf.ca/elog-alphag/alphag/5247

      bool enable_holdover = false;
      fOdbEqSettings->RB("enable_holdover", &enable_holdover, true);

      if (enable_holdover) {
         WriteEsper("lmk", "holdover_en", "true");
         WriteEsper("lmk", "holdover_los_de", "true");
      } else {
         WriteEsper("lmk", "holdover_en", "false");
         WriteEsper("lmk", "holdover_los_de", "false");
      }
      WriteEsper("lmk", "man_dac_en", "false");
      WriteEsper("lmk", "man_dac", "512");
      WriteEsper("lmk", "dac_clk_mult", "3");
      WriteEsper("lmk", "dac_clk_cntr", "127");
      WriteEsper("lmk", "track_en", "true");
      WriteEsper("lmk", "los_en", "true");
      WriteEsper("lmk", "los_timeout", "0");
      WriteEsper("lmk", "holdover_pll1_d", "true");

      uint32_t dac_value = ReadVal<uint32_t>("lmk:dac_value");

      fMfe->Msg(MINFO, "HandleInit", "lmk/dac_value: %d", int(dac_value));

      if (fMaster) {
         bool ext_clk = false;

         fOdbEqSettings->RB("ext_clk", &ext_clk, true);

         if (ext_clk) {
            fLmk_clkin_sel_mode = 2;
            WriteEsper("lmk", "clkin_sel_mode", "2");
         } else {
            fLmk_clkin_sel_mode = 0;
            WriteEsper("lmk", "clkin_sel_mode", "0");
         }
      } else {
         // slave CDM uses esata clock
         fLmk_clkin_sel_mode = 1;
         WriteEsper("lmk", "clkin_sel_mode", "1");
      }

      EqSetStatus("Writing ODB...", "#FFFFFF");

      fOdbEqSettings->RSA("Names vars", &fVarNames, true, fVarNames.size());
      // fOdbEqVariables->RU32("uptime",   &build_ts, true);
      // fOdbEqVariables->RU32("esata_clk",   &build_ts, true);
      // fOdbEqVariables->RU32("ext_clk",   &build_ts, true);
      // fOdbEqVariables->RU32("esata_trg",   &build_ts, true);
      // fOdbEqVariables->RU32("ext_trg",   &build_ts, true);

      std::vector<uint32_t> vars;
      fOdbEqVariables->RU32A("vars",   &vars, true, fVarNames.size());
      int esper_err = 0;
      fOdbEqVariables->RI("esper_err", &esper_err, true);
      fOdbEqVariables->WI("esper_err", 0);
      // int tmp;
      // fOdbEqVariables->RI("pll1_ld",   &tmp, true);
      // fOdbEqVariables->RI("pll2_ld",   &tmp, true);
      // fOdbEqVariables->RU32("ld1_counter",   &build_ts, true);
      // fOdbEqVariables->RU32("ld2_counter",   &build_ts, true);

      EqSetStatus("Init done", "#00FF00");

      fMfe->Msg(MINFO, "HandleInit", "Init done, master %d, holdover_enable %d, clkin_sel_mode %d", fMaster, enable_holdover, fLmk_clkin_sel_mode);

      return TMFeOk();
   }

   void DisconnectEsper()
   {
      if(fEsper){
         if(fEsper->s){
            fEsper->s->Close();
            delete fEsper->s;
            fEsper->s = nullptr;
         }
         delete fEsper;
         fEsper = nullptr;
      }
   }

   TMFeResult ConnectEsper(bool do_msg = true)
   {
      DisconnectEsper();

      std::string cdm_hostname;
      double to_connect = 5.0;
      double to_read = 10.0;
      double to_write = 5.0;

      fOdbEqSettings->RS("hostname", &cdm_hostname, true);
      fOdbEqSettings->RD("connect_timeout", &to_connect, true);
      fOdbEqSettings->RD("read_timeout",    &to_read, true);
      fOdbEqSettings->RD("write_timeout",   &to_write, true);

      if (cdm_hostname.empty()) {
         return TMFeErrorMessage("Please set CDM hostname in ODB settings");
      }

      KOtcpConnection* s = new KOtcpConnection(cdm_hostname.c_str(), "http");
      
      s->fConnectTimeoutMilliSec = to_connect*1000;
      s->fReadTimeoutMilliSec    = to_read*1000;
      s->fWriteTimeoutMilliSec   = to_write*1000;
      s->fHttpKeepOpen = false;
      
      assert(fEsper == nullptr);
      
      fEsper = new EsperComm(cdm_hostname.c_str(), s);

      std::string resp = fEsper->Read(fMfe, "build", "build_timestamp", NULL, do_msg);

      if(resp.empty()){
         return TMFeErrorMessage("Esper failure, cannot read build/build_timestamp");
      }

      if (do_msg)
         fMfe->Msg(MINFO, "ConnectEsper", "Connected to CDM %s, build_timestamp \"%s\"", cdm_hostname.c_str(), resp.c_str());

      uint32_t build_ts = xatoi(resp.c_str());
      // std::cout << resp << " -> " << build_ts << std::endl;
      fOdbEqVariables->WU32("build_ts", build_ts);

      switch (build_ts) {
      case 1530895281:
         // cdm00 at TRIUMF
         break;
      case 1530315978:
         // cdm02 and cdm03 at CERN
         break;
      default:
         return TMFeErrorMessage("Unexpected firmware build_timestamp");
      }

      return TMFeOk();
   }

   TMFeResult WriteEsper(const char* mid, const char* vid, const char* val)
   {
      std::string xval = std::string("[") + val + "]";
      std::string rd = fEsper->Read(fMfe, mid, vid);
      if(rd != xval) {
         fMfe->Msg(MINFO, "WriteEsper", "Writing to CDM %s/%s value \"%s\"", mid, vid, val);
         fEsper->Write(fMfe, mid, vid, val);
         std::string rd1 = fEsper->Read(fMfe, mid, vid);
         if (rd1 != xval) {
            fMfe->Msg(MERROR, "WriteEsper", "Write mismatch error: CDM %s/%s wrote \"%s\", read back \"%s\"", mid, vid, val, rd1.c_str());
            return TMFeErrorMessage("Write read back mismatch");
         }
      }
      return TMFeOk();
   }

   template<class T>
   T ReadVal(const std::string var)
   {
      size_t pos = var.find(':');
      std::string mid = var.substr(0, pos);
      std::string vid = var.substr(pos+1);
      //std::cout << "Reading [" << mid << "][" << vid << "]" << std::endl;

      std::string last_msg;

      for (int i=0; i<5; i++) {
         std::string resp = fEsper->Read(fMfe, mid.c_str(), vid.c_str(), NULL, false);

         if (resp.empty()) {
            // timeout, etc
            last_msg = fEsper->fFailedMessage;
            printf("read %s/%s got error or timeout: %s\n", mid.c_str(), vid.c_str(), fEsper->fFailedMessage.c_str());
            DisconnectEsper();

            for (int j=0; j<10; j++) {
               TMFeResult r = ConnectEsper(false);
               if (!r.error_flag)
                  break;
               last_msg = fEsper->fFailedMessage;
               printf("read %s/%s reconnect %d error or timeout: %s\n", mid.c_str(), vid.c_str(), j, fEsper->fFailedMessage.c_str());
               DisconnectEsper();
            }

            if (!fEsper) {
               // reconnect failed
               fMfe->Msg(MERROR, "ReadVal", "Too many retries reconnecting with CDM: %s", last_msg.c_str());
               return 0;
            }

            continue;
         }

         assert(fEsper);

         T val = 0;
         if (strstr(resp.c_str(), "true")) {
            val = 1;
         } else {
            val = xatoi(resp.c_str());
         }
         printf("read %s/%s got \"%s\" value %d\n", mid.c_str(), vid.c_str(), resp.c_str(), int(val));
         return val;
      }

      // too many read failures
      DisconnectEsper();
      fMfe->Msg(MERROR, "ReadVal", "Too many retries reading %s/%s: %s", mid.c_str(), vid.c_str(), last_msg.c_str());
      return 0;
   }

   void HandlePeriodic()
   {
      //printf("CdmCtrl::HandlePeriodic!\n");
      if(!fEsper) ConnectEsper();
      static int esper_err = 0;
      static int err_repeat = 0;
      std::vector<uint32_t> vars;
      fOdbEqVariables->RU32A("vars", &vars);
      // double clk_esata = double(vars[1])*1.e-6;
      // double clk_ext = double(vars[2])*1.e-6;
      uint8_t pll1_ld = vars[5];
      uint8_t pll2_ld = vars[6];
      static uint32_t ld1_counter = vars[7];
      static uint32_t ld2_counter = vars[8];
      uint8_t holdover = vars[9];
      vars.clear();
      for(unsigned int i = 0; i < fVarNames.size(); i++){
         vars.push_back(ReadVal<uint32_t>(fVarNames[i]));
         if(!fEsper){
            esper_err++;
            fOdbEqVariables->WI("esper_err", esper_err);
            EqSetStatus("Communication error", "#FF0000");
            if(err_repeat > 3){
               if (fMaster) {
                  fMfe->TriggerAlarm("MasterCDM", "Cannot talk to the master CDM, see messages", "Alarm");
               } else {
                  fMfe->TriggerAlarm("SlaveCDM", "Cannot talk to the slave CDM, see messages", "Alarm");
               }
            }
            return;
         }
         err_repeat = 0;
         fMfe->Yield(0.001);
         if (fMfe->fShutdownRequested) {
            printf("CdmCtrl::HandlePeriodic: shutdown requested!\n");
            return;
         }
      }
      fOdbEqVariables->WU32A("vars", vars);
      // clk_esata = double(vars[1])*1.e-6;
      // clk_ext = double(vars[2])*1.e-6;
      pll1_ld = vars[5];
      pll2_ld = vars[6];
      static bool first = true;
      if(!first && (vars[7] > ld1_counter || vars[8] > ld2_counter)){
         EqSetStatus("Lost PLL lock", "#FF0000");
         fMfe->Msg(MERROR, "HandlePeriodic", "Lost PLL lock, PLL1 los counter %d -> %d, PLL2 los counter %d -> %d", ld1_counter, vars[7], ld2_counter, vars[8]);
         if (fMaster) {
            fMfe->TriggerAlarm("MasterCDM", "Master CDM lost PLL lock", "Alarm");
         } else {
            fMfe->TriggerAlarm("SlaveCDM", "Slave CDM lost PLL lock", "Alarm");
         }
      }
      first = false;
      ld1_counter = vars[7];
      ld2_counter = vars[8];
      holdover = vars[9];
      if(pll1_ld && pll2_ld && !holdover){
         //if (fMaster)
         //   fMfe->ResetAlarm("MasterCDM");
         //else
         //   fMfe->ResetAlarm("SlaveCDM");
         EqSetStatus((fClkModes[fLmk_clkin_sel_mode] + " clock ok").c_str(), "#00FF00");
      } else if(!pll1_ld && !pll2_ld && !holdover){
         EqSetStatus("No clock, PPL1 not locked, PLL2 not locked, not in holdover mode", "#FF0000");
         if (fMaster) {
            fMfe->TriggerAlarm("MasterCDM", "Master CDM no clock", "Alarm");
         } else {
            fMfe->TriggerAlarm("SlaveCDM", "Slave CDM no clock", "Alarm");
         }
      } else if(!pll1_ld && pll2_ld && holdover){
         EqSetStatus("In holdover mode: no external clock", "yellow");
         if (fMaster) {
            fMfe->TriggerAlarm("MasterCDM", "Master CDM lost external clock", "Alarm");
         } else {
            fMfe->TriggerAlarm("SlaveCDM", "Slave CDM lost esata clock", "Alarm");
         }
      } else if(!pll1_ld && pll2_ld && !holdover){
         EqSetStatus("No external clock, no holdover, clock is drifting", "#ff0000");
         if (fMaster) {
            fMfe->TriggerAlarm("MasterCDM", "Master CDM lost external clock", "Alarm");
         } else {
            fMfe->TriggerAlarm("SlaveCDM", "Slave CDM lost esata clock", "Alarm");
         }
      } else if(pll1_ld && pll2_ld && holdover){
         EqSetStatus("Stuck in holdover mode", "yellow");
         if (fMaster) {
            fMfe->TriggerAlarm("MasterCDM", "Master CDM stuck in holdover mode", "Alarm");
         } else {
            fMfe->TriggerAlarm("SlaveCDM", "Slave CDM stuck in holdover mode", "Alarm");
         }
      } else {
         char buf[256];
         sprintf(buf, "Unexpected CDM state: pll1_ld %d, ppl2_ld %d, holdover %d", pll1_ld, pll2_ld, holdover);
         fMfe->Msg(MERROR, "HandlePeriodic", "%s", buf);
         EqSetStatus(buf, "#FF0000");
      }

      static uint32_t esata_trg = 0;
      // static int runNo = 0;
      // if(fMfe->fRunNumber != runNo){   // run number changed since last period
      //    esata_trg = 0;
      // }
      int run_state = 0;                              
      fMfe->fOdbRoot->RI("Runinfo/State", &run_state);
      fMfe->fStateRunning = (run_state == 3);                
      int run_number = 0;                              
      fMfe->fOdbRoot->RI("Runinfo/Run Number", &run_number);
      if(fMfe->fStateRunning && run_number == fMfe->fRunNumber){
         printf("Run number %d, state %d, esata_trg 0x%08x -> 0x%08x\n", run_number, run_state, esata_trg, vars[3]);
         if(vars[3] == esata_trg){  // during a run esata trigger counter should always increment
            EqSetStatus("Esata trigger counter is not incrementing", "yellow");
            if (fMaster)
               fMfe->TriggerAlarm("MasterCDMesata", "No esata triggers in master CDM", "Warning");
            else
               fMfe->TriggerAlarm("SlaveCDMesata", "No esata triggers in slave CDM", "Warning");
         }
      }
      fMfe->fRunNumber = run_number;
      esata_trg = vars[3];
   }

   //TMFeResult HandleBeginRun(int run_number)
   //{
   //   return TMFeOk();
   //}
};

class FeCdm: public TMFrontend
{
public:
   bool fMaster = false;
   bool fSlave = false;

public:
   FeCdm() // ctor
   {
      printf("FeCdm::ctor!\n");
   }

   void HandleUsage()
   {
      printf("Usage: fecdm [Midas options] -- [--master|--slave]\n");
   };

   TMFeResult HandleArguments(const std::vector<std::string>& args)
   {
      printf("FeCdm::HandleArguments!\n");

      for(auto it = args.begin(); it != args.end(); it++){
         if(*it == "--master"){
            fMaster = true;
         } else if(*it == "--slave"){
            fSlave = true;
         }
      }

      if (fMaster && fSlave) {
         return TMFeErrorMessage("Should not have both --master and --slave");
      }

      if (fMaster) {
         fMfe->fProgramName = "fecdm_master";
      } else if (fSlave) {
         fMfe->fProgramName = "fecdm_slave";
      } else {
         return TMFeErrorMessage("Should have either --master or --slave");
      }

      return TMFeOk();
   };

   TMFeResult HandleFrontendInit(const std::vector<std::string>& args)
   {
      printf("FeCdm::HandleFrontendInit!\n");

      fMfe->SetWatchdogSec(5*60);

      std::string cdm_eq_name;

      if (fMaster) {
         cdm_eq_name = "MasterCDM";
      } else if (fSlave) {
         cdm_eq_name = "SlaveCDM";
      } else {
         return TMFeErrorMessage("Should have either --master or --slave");
      }
      CdmCtrl *eq = new CdmCtrl(cdm_eq_name.c_str(), __FILE__, fMaster);
      eq->fEqConfBuffer.clear();

      FeAddEquipment(eq);
      fMfe->DeregisterTransitions();
      return TMFeOk();
   };

   TMFeResult HandleFrontendReady(const std::vector<std::string>& args)
   {
      printf("FeCdm::HandleFrontendReady!\n");
      //FeStartPeriodicThread();
      //fMfe->StartRpcThread();
      return TMFeOk();
   };

   void HandleFrontendExit()
   {
      printf("FeCdm::HandleFrontendExit!\n");
   };
};

// boilerplate main function

int main(int argc, char* argv[])
{
   FeCdm fe_cdm;
   return fe_cdm.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
