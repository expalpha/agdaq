//
// feevb.cxx
//
// Frontend/event builder for GRIF16 data
//

#include <stdio.h>
#include <netdb.h> // getnameinfo()
#include <stdlib.h> // malloc()
#include <string.h> // memcpy()
#include <errno.h> // errno
//#include <unistd.h>
//#include <time.h>
#include <assert.h> // assert
#include <math.h> // fabs()

#include <unistd.h> // sleep()

#include <string>
#include <vector>
#include <deque>
#include <mutex>
#include <thread>

#include "midas.h"
#include "msystem.h" // rb_get_buffer_level()

#include "TsSync.h"

#include "mvodb.h"

#include "tmfe.h"

#define XALIGN8(x)  (((x)+7) & ~7)

class SgEvent
{
public:
   uint32_t fHeader[6];
   std::vector<char*>  fSgPtr;
   std::vector<size_t> fSgLen;

public:
   SgEvent() // ctor
   {
   }

   ~SgEvent() // dtor
   {
      Clear();
   }

   void Clear()
   {
      //Print();
      for (int i=0; i<6; i++) {
         fHeader[i] = 0;
      }
      assert(fSgPtr.size() == fSgLen.size());
      // count from 1: slot[0] is pointer the header
      for (size_t i=1; i<fSgPtr.size(); i++) {
         if (fSgPtr[i]) {
            free(fSgPtr[i]);
            fSgPtr[i] = NULL;
            fSgLen[i] = 0;
         }
      }
      fSgPtr.clear();
      fSgLen.clear();
   }

   void Init(uint16_t event_id, uint16_t trigger_mask, uint32_t serial, uint32_t timestamp)
   {
      Clear();
      fHeader[0] = trigger_mask<<16 | event_id; // 0x00010001; // event id and trigger mask
      fHeader[1] = serial; // 1; // serial
      fHeader[2] = timestamp; // TMFE::GetTime(); // timestamp in seconds
      fHeader[3] = 2*4;  // data size
      fHeader[4] = 0;    // bank header: data size
      fHeader[5] = 0x31; // bank header: flags: bank32a format
      fSgPtr.push_back(NULL); // header
      fSgLen.push_back(0);
   }

   void AddBank(char* ptr, size_t len)
   {
      fHeader[3] += len;
      fHeader[4] += len;
      fSgPtr.push_back(ptr);
      fSgLen.push_back(len);
   }

   void AddBank32a(char* ptr)
   {
      uint32_t* wptr = (uint32_t*)ptr;
      size_t len = 4*4 + XALIGN8(wptr[2]);
      AddBank(ptr, len);
   }

   size_t GetEventSize() const
   {
      return 4*4 + fHeader[3];
   }

   void Print() const
   {
      printf("Header:\n");
      for (int i=0; i<6; i++) {
         printf("  header[%d] 0x%08x (%d)\n", i, fHeader[i], fHeader[i]);
      }
      printf("Segments: %d\n", (int)fSgPtr.size());
      for (size_t i=0; i<fSgPtr.size(); i++) {
         printf("  segment[%d] %p %6d", (int)i, fSgPtr[i], (int)fSgLen[i]);
         if (fSgPtr[i]) {
            uint32_t* bptr32a = (uint32_t*)fSgPtr[i];
            printf(", bank 0x%08x 0x%08x 0x%08x 0x%08x", bptr32a[0], bptr32a[1], bptr32a[2], bptr32a[3]);
            printf(", [%s] %d", (char*)bptr32a, bptr32a[2]);
         }
         printf("\n");
      }
   }
};

TMFeResult SendSgEvent(TMFeEquipment* eq, SgEvent* e)
{
   //e->Print();
   assert(e->fSgPtr.size() == e->fSgLen.size());
   e->fSgPtr[0] = (char*)e->fHeader;
   e->fSgLen[0] = 6*4;
   return eq->EqSendEvent(e->fSgPtr.size(), (const char**)e->fSgPtr.data(), e->fSgLen.data(), false);
}

#include "trg_packet.h"

////////////////////////////////////////////////////////////////////////////

static int verbose = 0;

////////////////////////////////////////////////////////////////////////////
//                     UNPACKING OF ALPHA16 DATA
////////////////////////////////////////////////////////////////////////////

static uint8_t getUint8(const void* ptr, int offset)
{
  return *(uint8_t*)(((char*)ptr)+offset);
}

static uint16_t getUint16(const void* ptr, int offset)
{
  uint8_t *ptr8 = (uint8_t*)(((char*)ptr)+offset);
  return ((ptr8[0]<<8) | ptr8[1]);
}

static uint32_t getUint32(const void* ptr, int offset)
{
  uint8_t *ptr8 = (uint8_t*)(((char*)ptr)+offset);
  return (ptr8[0]<<24) | (ptr8[1]<<16) | (ptr8[2]<<8) | ptr8[3];
}

// CRC16 from http://stackoverflow.com/questions/10564491/function-to-calculate-a-crc16-checksum
//static unsigned short crc16(const unsigned char* data_p, unsigned char length){
//  unsigned char x;
//  unsigned short crc = 0xFFFF;
//  
//  while (length--){
//    x = crc >> 8 ^ *data_p++;
//    x ^= x>>4;
//    crc = (crc << 8) ^ ((unsigned short)(x << 12)) ^ ((unsigned short)(x <<5)) ^ ((unsigned short)x);
//  }
//  return crc;
//}

struct Alpha16info
{
   int packetType      = 0;
   int packetVersion   = 0;
   int acceptedTrigger = 0;
   uint32_t hardwareId = 0;
   uint32_t buildTimestamp = 0;
   uint32_t eventTimestamp = 0;
   uint32_t triggerOffset  = 0;
   int moduleId      = 0;
   int channelType   = 0;
   int channelId     = 0;
   int nsamples      = 0;
   int nsamples_supp = 0;
   //int checksum; // not implemented in any adc firmware
   //int xcrc16;
   uint32_t footer       = 0;
   bool     supp_enabled = false;
   bool     keep_bit     = false;
   uint16_t keep_last    = 0;
   uint16_t baseline     = 0;

   void PrintAdc() const
   {
      printf("ALPHA16 data packet:\n");
      printf("  packet type:    0x%02x (%d)\n", packetType, packetType);
      printf("  packet version: 0x%02x (%d)\n", packetVersion, packetVersion);
      printf("  hwid:     0x%08x\n", hardwareId);
      printf("  buildts:  0x%08x\n", buildTimestamp);
      printf("  mod id:   0x%02x\n", moduleId);
      printf("  trig no:  0x%04x (%d)\n", acceptedTrigger, acceptedTrigger);
      printf("  event ts: 0x%08x (%d)\n", eventTimestamp, eventTimestamp);
      printf("  trig offset:   %d\n", triggerOffset);
      printf("  channel: type: %d, id: %d\n", channelType, channelId);
      printf("  nsamples: %d, actual %d\n", nsamples, nsamples_supp);
      printf("  footer: 0x%08x: supp enabled %d, keep_bit %d, keep_last %3d, baseline 0x%04x\n", footer, supp_enabled, keep_bit, keep_last, baseline);
      //printf("  checksum: 0x%04x, computed checksum 0x%04x\n", checksum, xcrc16);
      //printf("length: %d, bank length %d\n", length, bankLength);
   };

   void PrintAdcHeader() const
   {
      printf(" t/v: 0x%02x 0x%02x", packetType, packetVersion);
      printf(" mod id: 0x%02x", moduleId);
      printf(" trig no: 0x%04x", acceptedTrigger);
      printf(" ts: 0x%08x", eventTimestamp);
      printf(" chid: %3d %2d", channelType, channelId);
      printf(" ns: %3d %3d", nsamples, nsamples_supp);
      printf(" supp: %d %d %3d, baseline 0x%04x", supp_enabled, keep_bit, keep_last, baseline);
   };

   int UnpackAdc(const void*ptr, int bklen8)
   {
      /*
        ALPHA16 UDP packet data format from Bryerton: this is packet version 1:
        
        Date: Mon, 20 Jun 2016 15:07:08 -0700
        From bryerton@triumf.ca  Mon Jun 20 15:07:05 2016
        From: Bryerton Shaw <bryerton@triumf.ca>
        To: Konstantin Olchanski <olchansk@triumf.ca>
        Subject: Re: eta on udp data?
        
        Hi Konstantin,
        
        Just trying to iron out one last bug, it was (is?) locking up if I
        saturated the link, but I think I just resolved that! So we ve got all
        16 channels outputting pretty steadily up to 40kHz or so, if I reduce
        it to 3 channels, we can get 150+ kHz event rates per channel.
        
        I am going to add a checksum onto the packet structure but it looks as
        follows, broken down by BYTE offset
        
        0 Packet Type - Currently fixed at0x01
        1 Packet Version - Currently fixed at0x01
        2 Accepted Trigger MSB - Inside the firmware logic Accepted Trigger is unsigned 32bits, providing the lower 16bits here for useful as a dropped UDP packet check
        3 Accepted Trigger LSB
        4 MSB Hardware ID - Currently the lower 48 bits of the ArriaV ChipID. It will be the MAC address shortly however
        5 "" ""
        6 "" ""
        7 "" ""
        8 "" ""
        9 LSB Hardware ID
        10 Build Timestamp (UNIX timestamp, aka seconds since Jan 01, 1980 UTC)
        11 "" ""
        12 "" ""
        13 "" ""
        14 0x00
        15 0x00
        16 MSB Event Timestamp
        17 "" ""
        18 "" ""
        19 "" ""
        20 "" ""
        21 LSB Event Timestamp
        22 MSB Trigger Offset - Trigger Point in relation to the start of the waveform packet. Signed 32bit integer
        23
        24
        25 LSB Trigger Offset
        26 ModuleID - Logical Identifier for the Alpha16 board. unsigned byte
        27 [7] Channel Type - Either 0 or 1, for BScint or Anode. The MSB of this byte
        27 [6:0] Channel ID - Unsigned 7bits, identifies the ADC channel (0-15) used
        28 MSB Sample Count - Unsigned 16 bit value indicating the number of samples (1-N)
        29 LSB Sample Count
        30 MSB First Waveform Sample - Signed 16 bit value
        31 LSB First Waveform Sample
        ....
        30 + (SampleCount*2) MSB Checksum
        31 + (SampleCount*2) LSB Checksum
        
        I will give you the checksum details in a moment, I am just adding it in
        now. Most likely will be a crc16 based on 1+x^2+x^15+x^16 .
        The byte positions may not be ideal, but we can sort that out.
        
        Cheers,
        
        Bryerton
      */
      
      //bankLength = bklen8;
      packetType = getUint8(ptr, 0);
      packetVersion = getUint8(ptr, 1);

      if (packetVersion == 1) {
         acceptedTrigger = getUint16(ptr, 2);
         hardwareId = getUint32(ptr, 4);
         buildTimestamp = getUint32(ptr, 10);
         //int zero = getUint16(ptr, 14);
         eventTimestamp = getUint32(ptr, 18);
         triggerOffset = getUint32(ptr, 22);
         moduleId = getUint8(ptr, 26);
         int chanX = getUint8(ptr, 27);
         channelType = chanX & 0x80;
         channelId = chanX & 0x7F;
         nsamples = getUint16(ptr, 28);
         nsamples_supp = nsamples;
         //checksum = getUint16(ptr, 30 + nsamples*2);
         //length = 32 + nsamples*2;
         //xcrc16 = crc16((const unsigned char*)ptr, 32 + nsamples*2);
         footer = 0;
         keep_bit     = true; // everything kept
         supp_enabled = false; // suppression is not implemented
      } else if (packetVersion == 2) {
         acceptedTrigger = getUint16(ptr, 4);
         hardwareId = getUint32(ptr, 6);
         buildTimestamp = getUint32(ptr, 12);
         //int zero = getUint16(ptr, 16);
         eventTimestamp = getUint32(ptr, 20);
         triggerOffset = getUint32(ptr, 24);
         moduleId = getUint8(ptr, 28);
         int chanX = getUint8(ptr, 29);
         channelType = chanX & 0x80;
         channelId = chanX & 0x7F;
         nsamples = getUint16(ptr, 30);
         nsamples_supp = (bklen8 - 32 - 4) / 2;
         //checksum = 0; // getUint16(ptr, 30 + nsamples*2);
         //xcrc16 = 0;
         footer = getUint32(ptr, bklen8 - 4);
         baseline  =   (footer & 0x0000FFFF) >> 0;
         keep_last =   (footer & 0x0FFF0000) >> 16;
         keep_bit     = footer & 0x10000000;
         supp_enabled = footer & 0x20000000;
      } else if (packetVersion == 3) {
         //printf("version 3 bank size %4d: ", bklen8);
         //for (int i=0; i<16; i++) {
         //   printf(" %02x", 0xFF&((char*)ptr)[i]);
         //}
         //printf("\n");

         acceptedTrigger = getUint16(ptr, 2);
         moduleId = getUint8(ptr, 4);
         int chanX = getUint8(ptr, 5);
         channelType = chanX & 0x80;
         channelId = chanX & 0x7F;
         nsamples = getUint16(ptr, 6);
         eventTimestamp = getUint32(ptr, 8);
         if (bklen8 == 16) {
            nsamples_supp = 0;
         } else {
            nsamples_supp = (bklen8 - 32 - 4) / 2;
         }
         footer = getUint32(ptr, bklen8 - 4);
         baseline  =   (footer & 0x0000FFFF) >> 0;
         keep_last =   (footer & 0x0FFF0000) >> 16;
         keep_bit     = footer & 0x10000000;
         supp_enabled = footer & 0x20000000;
      } else {
         // unknown packet version
         return 1;
      }

      //Print();

      return 0;
   };
};

class Bank32aBuf
{
public:
   int   fWaitingIncr = 0; // increment waiting bank count
   bool  fError = false;
   char* fBank = NULL;

   int      fSlot  = 0; // evb->AddBank slot
   uint32_t fTs    = 0; // evb->AddBank ts
   int      fEpoch = 0; // epoch from TsSync
   double   fTime  = 0; // time computed after sync is successful

   Bank32aBuf* fNext = NULL;
   Bank32aBuf* fLast = NULL;

   Bank32aBuf(const char* bankname, int tid, const char* pdata, size_t size, int waiting_incr, int islot, uint32_t ts) // ctor
   {
      //typedef struct {
      //   char name[4];                       /**< - */
      //   DWORD type;                         /**< - */
      //   DWORD data_size;                    /**< - */
      //   DWORD reserved;                     /**< - */
      //} BANK32A;

      // Note: Do not assert on bank size zero. Banks with size zero are explicitely generated by AddPwbBank(). K.O.

      //assert(size > 0);
      //if (size == 0 || size > 20000) {
      //   TMFE::Instance()->Msg(MERROR, "Bank32aBuf::ctor", "Invalid bank size %d for bank \"%s\" tid %d, slot %d, ts 0x%08x", (int)size, bankname, tid,  islot, ts);
      //   size = 0;
      //}

      fBank = (char*)malloc(4*4 + size);
      fBank[0] = bankname[0];
      fBank[1] = bankname[1];
      fBank[2] = bankname[2];
      fBank[3] = bankname[3];
      uint32_t* p32 = (uint32_t*)fBank;
      p32[1] = tid;
      p32[2] = size;
      p32[3] = 0;
      memcpy(fBank+4*4, pdata, size);
      fWaitingIncr = waiting_incr;
      fSlot = islot;
      fTs   = ts;
   }

   ~Bank32aBuf() // dtor
   {
      if (fBank)
         free(fBank);
      fBank = NULL;

      if (fLast)
         fLast = NULL;

      if (fNext) {
         Bank32aBuf* b = fNext;
         fNext = NULL;
         while (b) {
            Bank32aBuf* next = b->fNext;
            b->fNext = NULL; // avoid recursive destructor!
            b->fLast = NULL;
            delete b;
            b = next;
         }
      }
   }

   void Print() const
   {
      //printf("ts 0x%08x, epoch %d, time %f, incr %f, banks %d", ts, epoch, time, timeIncr, (int)buf->size());
      //if (buf->size() == 1) {
      //   printf(", bank [%s]", (*buf)[0]->name.c_str());
      //}
      printf("ts 0x%08x, epoch %d, time %.6f, bank [%.4s]", fTs, fEpoch, fTime, fBank);
      if (fNext) {
         printf(" chained ");
         Bank32aBuf* b = fNext;
         while (b) {
            printf(" [%.4s]", b->fBank);
            b = b->fNext;
         }
      }
   }

   void AddChain(Bank32aBuf* b)
   {
      // add new chain to our fLast
      if (fLast) {
         fLast->fNext = b;
      } else {
         fNext = b;
      }

      // set out fLast to fLast of new chain
      if (b->fLast) {
         fLast = b->fLast;
      } else {
         fLast = b;
      }
   }
};

class SendQueue
{
public:
   TMFE* fMfe = NULL;
   TMFeEquipment* fEq = NULL;

public: // settings
   size_t fMaxQueueSize = 1000;
   bool   fDropQueue = false;

public: // class data
   std::deque<Bank32aBuf*> fBuf;
   std::mutex               fLock;
   size_t fMaxSize = 0;
   size_t fMaxSizeEver = 0;

public: // statistics
   size_t fMaxEventSize = 0;
   size_t fMaxEventSizeEver = 0;

   double fCountQueueFull = 0;
   double fCountDropFull  = 0;

   double fEventCountAll = 0;
   double fByteCountAll = 0;

   double fEventCount = 0;
   double fByteCount = 0;

public: // internal data
   SgEvent fEvent;

public:
   bool fRunThread = false;
   std::thread* fThread = NULL;

public:
   SendQueue(TMFE* mfe, TMFeEquipment* eq) // ctor
   {
      fMfe = mfe;
      fEq = eq;
   }

   ~SendQueue() // dtor
   {
      StopThread();

      {
         std::lock_guard<std::mutex> lock(fLock);
         if (fBuf.size() > 0) {
            fMfe->Msg(MERROR, "SendQueue::dtor", "SendQueue::dtor: Deleting %d events", (int)fBuf.size());
            for (unsigned i=0; i<fBuf.size(); i++) {
               delete fBuf[i];
               fBuf[i] = NULL;
            }
            fBuf.clear();
         }
         // implicit unlock
      }
   }

public:
   void SendQueueThread()
   {
      printf("SendQueueThread: Thread for event sender started\n");
      while (fRunThread && !fMfe->fShutdownRequested) {

         fLock.lock();

         if (fBuf.empty()) {

            fLock.unlock();

            //printf("W");
            ::usleep(10000);
            continue;
         }
            
         Bank32aBuf* b = fBuf.front();
         fBuf.pop_front();

         fLock.unlock();
         
         assert(b != NULL);
         
         SendAndDeleteEvent(b);
         b = NULL;
      }
      printf("SendQueueThread: Thread for event sender stopped\n");
   }

   void StartThread()
   {
      assert(fThread==NULL);
      fRunThread = true;
      fThread = new std::thread(&SendQueue::SendQueueThread, this);
   }

   void StopThread()
   {
      assert(fThread);
      fRunThread = false;
      fThread->join();
      delete fThread;
      fThread = NULL;
   }

   int GetQueueSize()
   {
      std::lock_guard<std::mutex> lock(fLock);
      return fBuf.size();
      // implicit unlock
   }

   void PushEvent(Bank32aBuf* b)
   {
      fLock.lock();

      if (fBuf.size() >= fMaxQueueSize) {
         fCountQueueFull++;

         while (fBuf.size() >= fMaxQueueSize) {
            if (fDropQueue || fMfe->fShutdownRequested) {
               fCountDropFull++;
               delete b;
               b = NULL;
               fLock.unlock();
               return;
            }
            
            fLock.unlock();
            TMFE::Sleep(0.001);
            fLock.lock();
         }
      }

      fBuf.push_back(b);

      size_t size = fBuf.size();

      if (size > fMaxSize)
         fMaxSize = size;
      if (size > fMaxSizeEver)
         fMaxSizeEver = size;

      fLock.unlock();
   }

   void SendAndDeleteEvent(Bank32aBuf* b)
   {
      fEvent.Init(fEq->fEqConfEventID, fEq->fEqConfTriggerMask, fEq->fEqSerial, TMFE::GetTime());

      while (b != NULL) {
         Bank32aBuf* next = b->fNext;
         fEvent.AddBank32a(b->fBank);
         b->fBank = NULL;
         b->fNext = NULL;
         delete b;
         b = next;
      }

      size_t size = fEvent.GetEventSize();

      //if (size == 0) {
      //   return false;
      //}
   
      if (size > fMaxEventSize)
         fMaxEventSize = size;
      if (size > fMaxEventSizeEver)
         fMaxEventSizeEver = size;
      
      fEventCount    += 1;
      fEventCountAll += 1;
      fByteCount     += size;
      fByteCountAll  += size;
      //printf("Send event: size %d\n", size);

      SendSgEvent(fEq, &fEvent);
   }

   void WriteStatus(MVOdb* odb)
   {
      odb->WI("send_queue_size",  fMaxSize);
      fMaxSize = 0;
      odb->WI("send_queue_size_max", fMaxSizeEver);
      odb->WI("send_queue_full", fCountQueueFull);
      odb->WI("send_queue_drop_full", fCountDropFull);
   }

   void WriteVariables(MVOdb* odb)
   {
      odb->WD("event_size", fMaxEventSize);
      fMaxEventSize = 0;
      odb->WD("event_size_max", fMaxEventSizeEver);

      odb->WD("event_count", fEventCount);
      fEventCount = 0;

      odb->WD("byte_count", fByteCount);
      fByteCount = 0;
   }
};

struct EvbEvent
{
   bool   complete = false; // event is complete
   bool   error = false;    // event has an error
   int    counter = 0;  // event sequential counter
   double time = 0;     // event time, sec
   double timeIncr = 0; // time from previous event, sec

   bool maybe_complete = false;

   double create_time = 0;
   double update_time = 0;

   Bank32aBuf *banks = NULL;

   std::vector<int> banks_count;
   std::vector<int> banks_waiting;

   //EvbEvent(); // ctor
   ~EvbEvent(); // dtor
   void Print(int level=0) const;
};

void EvbEvent::Print(int level) const
{
   double now = TMFE::GetTime();

   printf("EvbEvent %d, time %f, incr %f, complete %d, maybe %d, age %.1f/%.1f, error %d, banks: ", counter, time, timeIncr, complete, maybe_complete, now-create_time, now-update_time, error);
   const Bank32aBuf* b = banks;
   for (unsigned i=0; b != NULL; i++) {
      if (i>4) { // truncate the bank list
         printf(" ...");
         break;
      }
      printf(" %.4s", b->fBank);
      b = b->fNext;
   }

   if (level == 0) {
      printf(" evb slots: ");
      for (unsigned i=0; i<banks_count.size(); i++) {
         printf(" %d/%d", banks_count[i], banks_waiting[i]);
      }
   } else if (level == 1) {
      printf(" incomplete evb slots: ");
      for (unsigned i=0; i<banks_count.size(); i++) {
         if (banks_waiting[i]) {
            printf(" slot %d: %d/%d", i, banks_count[i], banks_waiting[i]);
         }
      }
   }
}

EvbEvent::~EvbEvent() // dtor
{
   if (banks) {
      delete banks;
      banks = NULL;
   }
}

struct PwbData
{
   uint32_t cnt = 0;
   uint32_t ts = 0;
   uint32_t sent_bits = 0;
   uint32_t threshold_bits = 0;
   uint32_t pkt_seq = 0;
   uint16_t chunk_id = 0;
   uint32_t count_error = 0;
   uint32_t count_bad_pkt_seq = 0;
   uint32_t count_bad_channel_id = 0;
   uint32_t count_bad_format_revision = 0;
   uint32_t count_bad_chunk_id = 0;
   uint32_t count_lost_header = 0;
   uint32_t count_no_header = 0;
   uint32_t count_lost_footer = 0;
};

class PerSlotInfo
{
public:
   int fNumSlots = 0;

   std::vector<bool>   fAdcSlot;
   std::vector<bool>   fPwbSlot;
   std::vector<bool>   fTdcSlot;

   std::vector<double> fLastTimeSec;
   std::vector<double> fSkewTimeSec;
   std::vector<double> fCountPackets;
   std::vector<double> fCountBytes;
   std::vector<int>    fSentMin;
   std::vector<int>    fSentMax;
   std::vector<double> fCountSent0;
   std::vector<double> fCountSent1;
   std::vector<int>    fThrMin;
   std::vector<int>    fThrMax;
   std::vector<double> fCountThr0;
   std::vector<double> fCountThr1;
   std::vector<int>    fCountErrors;
   std::vector<int>    fPwbScaFifoMaxUsed;
   std::vector<int>    fPwbEventFifoWrUsed;
   std::vector<int>    fPwbEventFifoRdUsed;
   std::vector<int>    fPwbEventFifoWrMaxUsed;
   std::vector<int>    fPwbEventFifoRdMaxUsed;
   std::vector<int>    fPwbEventFifoOverflows;

public: // computed by ComputePerSecond()
   std::vector<double> fPacketsPerSec;
   std::vector<double> fBytesPerSec;
   std::vector<double> fSentAve;
   std::vector<double> fThrAveMax;
   std::vector<double> fSentAveMax;
   std::vector<double> fThrAve;
   double fAdcThrAvePerEvent = 0;
   double fAdcSentAvePerEvent = 0;
   double fAdcThrAvePerEventMax = 0;
   double fAdcSentAvePerEventMax = 0;
   double fPwbThrAvePerEvent = 0;
   double fPwbSentAvePerEvent = 0;
   double fPwbThrAvePerEventMax = 0;
   double fPwbSentAvePerEventMax = 0;
   double fTdcSentAvePerEvent = 0;
   double fTdcSentAvePerEventMax = 0;
   double fTdcThrAvePerEvent = 0;
   double fTdcThrAvePerEventMax = 0;

public: // PWB data stream state
   std::vector<PwbData> fPwbData;

public: // these counters do not have any locking, glitch possible
   int fPwbScaFifoMaxUsedEver = 0;
   int fPwbEventFifoWrMaxUsedAll = 0;
   int fPwbEventFifoWrMaxUsedEver = 0;
   int fPwbEventFifoOverflowsAll  = 0;
   int fPwbEventFifoOverflowsEver = 0;

public: // rate counters
   double fPrevTime = 0;
   std::vector<double> fPrevCountPackets;
   std::vector<double> fPrevCountBytes;
   std::vector<double> fPrevCountSent0;
   std::vector<double> fPrevCountSent1;
   std::vector<double> fPrevCountThr0;
   std::vector<double> fPrevCountThr1;

public: // computed in CheckSkew()
   double fSkewMax = 0;
   double fSkewMaxEver = 0;

public: // computed in SumSlotErrors()
   int fSumSlotErrors = 0; // errors counted in AddXxxBank() that are not attached to any event. Sum of fCountError[]

public:
   void Init(int num_slots, const std::vector<int>& slotType)
   {
      fNumSlots = num_slots;

      fAdcSlot.resize(fNumSlots);
      fPwbSlot.resize(fNumSlots);
      fTdcSlot.resize(fNumSlots);

      for (int i=0; i<num_slots; i++) {
         if (slotType[i] == 2)
            fAdcSlot[i] = true;
         else if (slotType[i] == 5)
            fPwbSlot[i] = true;
         else if (slotType[i] == 6)
            fTdcSlot[i] = true;
      }
      
      fLastTimeSec.resize(fNumSlots);
      fSkewTimeSec.resize(fNumSlots);
      
      fCountPackets.resize(fNumSlots);
      fCountBytes.resize(fNumSlots);
      
      fPacketsPerSec.resize(fNumSlots);
      fBytesPerSec.resize(fNumSlots);
      
      fPrevCountPackets.resize(fNumSlots);
      fPrevCountBytes.resize(fNumSlots);
      
      fSentMin.resize(fNumSlots);
      fSentMax.resize(fNumSlots);
      fSentAve.resize(fNumSlots);
      fSentAveMax.resize(fNumSlots);
      fCountSent0.resize(fNumSlots);
      fCountSent1.resize(fNumSlots);
      fPrevCountSent0.resize(fNumSlots);
      fPrevCountSent1.resize(fNumSlots);
      
      fThrMin.resize(fNumSlots);
      fThrMax.resize(fNumSlots);
      fThrAve.resize(fNumSlots);
      fThrAveMax.resize(fNumSlots);
      fCountThr0.resize(fNumSlots);
      fCountThr1.resize(fNumSlots);
      fPrevCountThr0.resize(fNumSlots);
      fPrevCountThr1.resize(fNumSlots);
      
      fCountErrors.resize(fNumSlots);

      fPwbData.resize(fNumSlots);
      
      fPwbScaFifoMaxUsed.resize(fNumSlots);
      fPwbEventFifoWrUsed.resize(fNumSlots);
      fPwbEventFifoRdUsed.resize(fNumSlots);
      fPwbEventFifoWrMaxUsed.resize(fNumSlots);
      fPwbEventFifoRdMaxUsed.resize(fNumSlots);
      fPwbEventFifoOverflows.resize(fNumSlots);

      ResetPerSecond();
   }

   void SumSlotErrors()
   {
      int sum = 0;
      for (int i=0; i<fNumSlots; i++) {
         sum += fCountErrors[i];
      }
      fSumSlotErrors = sum;
   }

   void CheckSkew(TMFE* mfe, double max_dead_sec, const std::vector<std::string>& slotNames, TsSync* sync)
   {
      double maxtime = 0;
      
      for (int slot=0; slot<fNumSlots; slot++) {
         if (sync->fModules[slot].fDead)
            continue;
         if (fLastTimeSec[slot]) {
            if (fLastTimeSec[slot] > maxtime)
               maxtime = fLastTimeSec[slot];
         }
      }
      
      for (int slot=0; slot<fNumSlots; slot++) {
         if (sync->fModules[slot].fDead) {
            // fSkewTimeSec[slot] = 0; // mark dead slot
         } else if (fLastTimeSec[slot]) {
            fSkewTimeSec[slot] = maxtime - fLastTimeSec[slot];
            
            if (fSkewTimeSec[slot] > fSkewMax)
               fSkewMax = fSkewTimeSec[slot];
            
            if (fSkewTimeSec[slot] > fSkewMaxEver)
               fSkewMaxEver = fSkewTimeSec[slot];

            if (fSkewTimeSec[slot] > max_dead_sec) {
               mfe->Msg(MERROR, "CheckSkew", "Slot %d (%s) is now dead, no packets in %.3f sec", slot, slotNames[slot].c_str(), fSkewTimeSec[slot]);
               sync->fModules[slot].fDead = true;
            }
         }
      }
   }

   void ResetPerSecond()
   {
      double now = TMFE::GetTime();
      fPrevTime = now;
      for (int i=0; i<fNumSlots; i++) {
         fPacketsPerSec[i] = 0;
         fBytesPerSec[i] = 0;
         fPrevCountPackets[i] = fCountPackets[i];
         fPrevCountBytes[i] = fCountBytes[i];
         fPrevCountSent0[i] = fCountSent0[i];
         fPrevCountSent1[i] = fCountSent1[i];
         fPrevCountThr0[i] = fCountThr0[i];
         fPrevCountThr1[i] = fCountThr1[i];
      }
   }

   void ComputePerSecond()
   {
      double now = TMFE::GetTime();
      double elapsed = now - fPrevTime;
      fPrevTime = now;

      double adcAveSent = 0;
      double adcAveThr = 0;
      
      double pwbAveSent = 0;
      double pwbAveThr = 0;
      
      double tdcAveSent = 0;
      double tdcAveThr = 0;
      
      for (int i=0; i<fNumSlots; i++) {
         double dp = fCountPackets[i] - fPrevCountPackets[i];
         double db = fCountBytes[i] - fPrevCountBytes[i];
         
         fPacketsPerSec[i] = dp/elapsed;
         fBytesPerSec[i] = db/elapsed;
         
         fPrevCountPackets[i] = fCountPackets[i];
         fPrevCountBytes[i] = fCountBytes[i];
         
         double ds0 = fCountSent0[i] - fPrevCountSent0[i];
         double ds1 = fCountSent1[i] - fPrevCountSent1[i];
         
         double aves = 0;
         if (ds0 >= 1)
            aves = ds1/ds0;
         
         fSentAve[i] = aves;

         if (aves > fSentAveMax[i])
            fSentAveMax[i] = aves;
         
         fPrevCountSent0[i] = fCountSent0[i];
         fPrevCountSent1[i] = fCountSent1[i];

         double dt0 = fCountThr0[i] - fPrevCountThr0[i];
         double dt1 = fCountThr1[i] - fPrevCountThr1[i];
         
         double avet = 0;
         if (dt0 >= 1)
            avet = dt1/dt0;
         
         fThrAve[i] = avet;

         if (avet > fThrAveMax[i])
            fThrAveMax[i] = avet;
         
         fPrevCountThr0[i] = fCountThr0[i];
         fPrevCountThr1[i] = fCountThr1[i];

         if (fPwbSlot[i]) {
            pwbAveSent += aves;
            pwbAveThr += avet;
         } else if (fAdcSlot[i]) {
            adcAveSent += aves;
            adcAveThr += avet;
         } else if (fTdcSlot[i]) {
            tdcAveSent += aves;
            tdcAveThr += avet;
         }
      }

      fAdcSentAvePerEvent = adcAveSent;
      fAdcThrAvePerEvent = adcAveThr;

      if (adcAveSent > fAdcSentAvePerEventMax) {
         fAdcSentAvePerEventMax = adcAveSent;
      }

      if (adcAveThr > fAdcThrAvePerEventMax) {
         fAdcThrAvePerEventMax = adcAveThr;
      }

      fPwbSentAvePerEvent = pwbAveSent;
      fPwbThrAvePerEvent = pwbAveThr;

      if (pwbAveSent > fPwbSentAvePerEventMax) {
         fPwbSentAvePerEventMax = pwbAveSent;
      }

      if (pwbAveThr > fPwbThrAvePerEventMax) {
         fPwbThrAvePerEventMax = pwbAveThr;
      }

      fTdcSentAvePerEvent = tdcAveSent;
      fTdcThrAvePerEvent = tdcAveThr;

      if (tdcAveSent > fTdcSentAvePerEventMax) {
         fTdcSentAvePerEventMax = tdcAveSent;
      }

      if (tdcAveThr > fTdcThrAvePerEventMax) {
         fTdcThrAvePerEventMax = tdcAveThr;
      }
   }

   void WriteStatus(MVOdb* odb)
   {
      odb->WI("per_slot_errors",  fSumSlotErrors);

      odb->WDA("skew_time", fSkewTimeSec);
      odb->WDA("packets_count", fCountPackets);
      odb->WDA("bytes_count", fCountBytes);
      odb->WDA("packets_per_second", fPacketsPerSec);
      odb->WDA("bytes_per_second", fBytesPerSec);
      odb->WIA("sent_min", fSentMin);
      odb->WIA("sent_max", fSentMax);
      odb->WDA("sent_ave", fSentAve);
      odb->WDA("sent_ave_max", fSentAveMax);
      odb->WIA("thr_min", fThrMin);
      odb->WIA("thr_max", fThrMax);
      odb->WDA("thr_ave", fThrAve);
      odb->WDA("thr_ave_max", fThrAveMax);
      odb->WIA("errors", fCountErrors);
      odb->WIA("pwb_sca_fifo_max_used", fPwbScaFifoMaxUsed);
      odb->WIA("pwb_event_fifo_wr_max_used", fPwbEventFifoWrMaxUsed);
      odb->WIA("pwb_event_fifo_rd_max_used", fPwbEventFifoRdMaxUsed);
      odb->WIA("pwb_event_fifo_wr_used", fPwbEventFifoWrUsed);
      odb->WIA("pwb_event_fifo_rd_used", fPwbEventFifoRdUsed);
      odb->WIA("pwb_event_fifo_overflows", fPwbEventFifoOverflows);

      odb->WD("adc_samples_per_event_thr_max", fAdcThrAvePerEventMax);
      odb->WD("adc_samples_per_event_sent_max", fAdcSentAvePerEventMax);

      odb->WD("pwb_pads_per_event_thr_max", fPwbThrAvePerEventMax);
      odb->WD("pwb_pads_per_event_sent_max", fPwbSentAvePerEventMax);

      odb->WD("tdc_words_per_event_thr_max", fTdcThrAvePerEventMax);
      odb->WD("tdc_words_per_event_sent_max", fTdcSentAvePerEventMax);
   }

   void WriteVariables(MVOdb* odb)
   {
      odb->WD("skew_sec", fSkewMax);
      fSkewMax = 0;
      odb->WD("skew_max_sec", fSkewMaxEver);
      
      odb->WD("pwb_sca_fifo_max_used", fPwbScaFifoMaxUsedEver);
      odb->WD("pwb_event_fifo_used", fPwbEventFifoWrMaxUsedAll);
      fPwbEventFifoWrMaxUsedAll = 0;
      odb->WD("pwb_event_fifo_max_used", fPwbEventFifoWrMaxUsedEver);
      
      odb->WD("pwb_event_fifo_overflows", fPwbEventFifoOverflowsAll);
      fPwbEventFifoOverflowsAll = 0;
      odb->WD("pwb_event_fifo_overflows_sum", fPwbEventFifoOverflowsEver);

      odb->WD("adc_samples_per_event_thr", fAdcThrAvePerEvent);
      odb->WD("adc_samples_per_event_sent", fAdcSentAvePerEvent);

      odb->WD("pwb_pads_per_event_thr", fPwbThrAvePerEvent);
      odb->WD("pwb_pads_per_event_sent", fPwbSentAvePerEvent);

      odb->WD("tdc_words_per_event_thr", fTdcThrAvePerEvent);
      odb->WD("tdc_words_per_event_sent", fTdcSentAvePerEvent);
   }

   void LogPwbCounters(TMFE* mfe, const std::vector<std::string>& slotNames) const
   {
      for (unsigned i=0; i<fPwbData.size(); i++) {
         const PwbData* d = &fPwbData[i];
         if (d->count_error > 0) {
            mfe->Msg(MINFO, "LogPwbCounters", "slot %d: %s: PWB counters: errors: %d: bad_pkt_seq %d, bad_channel_id %d, bad_format_revision %d, bad_chunk_id %d, no_header %d, lost_header %d, lost_footer %d",
                   i,
                   slotNames[i].c_str(),
                   d->count_error,
                   d->count_bad_pkt_seq,
                   d->count_bad_channel_id,
                   d->count_bad_format_revision,
                   d->count_bad_chunk_id,
                   d->count_no_header,
                   d->count_lost_header,
                   d->count_lost_footer);
         }
      }
   }
};

#define EVB_STATE_UNCONFIGURED        0
#define EVB_STATE_WAITING_FOR_SYNC    1
#define EVB_STATE_RUNNING             2
#define EVB_STATE_SYNC_FAIL         101

class Evb
{
public:
   TMFE* fMfe = NULL;
   std::mutex fLock;

public:
   MVOdb* fOdbSettings  = NULL;
   MVOdb* fOdbConfig    = NULL;
   MVOdb* fOdbStatus    = NULL;
   MVOdb* fOdbVariables = NULL;

public:
   SendQueue* fSendQueue = NULL;

public: // settings
   double   fConfMaxAgeSec  = 3.0;
   double   fConfMaxDeadSec = 15.0;
   double   fConfEpsSec     = 0.000100;
   bool     fConfAllowClockDrift = false;
   double   fConfSyncTimeoutSec = 15.0;
   bool     fTrace = false;
   bool     fPrintStatus = false; // print evb status when updating equipment status
   bool     fPrintIncomplete = false; // print incomplete events
   bool     fPrintAll = false; // print all built events

   //int      fConfMaxReaderQueue = 100000;
   int      fConfMaxSendQueue   = 300;
   bool     fConfDropSendQueue  = false;

public: // state machine
   int      fEvbState = EVB_STATE_UNCONFIGURED;

public: // configuration maps, etc
   unsigned fNumSlots = 0;
   std::vector<int> fTrgSlot;   // slot of each module
   std::vector<int> fAdcSlot;  // slot of each module
   std::vector<int> fPwbSlot; // slot of each module
   std::vector<int> fTdcSlot; // slot of each module
   std::vector<int> fNumBanks; // number of banks for each slot
   std::vector<int> fSlotType; // module type for each slot
   std::vector<std::string> fSlotName; // module name for each slot

 public: // event builder state
   bool   fKludgeReady = false;
   TsSync fSync;
   int    fCounter = 0;
   std::deque<Bank32aBuf*> fBanks;
   std::deque<EvbEvent*> fEvents;
   std::vector<int> fCountSlotIncomplete;
   std::vector<int> fDeadSlots;
   int fCountDeadSlots = 0;
   double fLastBuiltTime = -9999;
   int fCountEvbErrorsTooOld = 0;
   int fCountEvbErrorsFindEvent = 0;
   double fSyncStartTime = 0;

public: // kludges for TDC behind-by-one-event problem
   bool fKludgeTdcKillFirstEvent = false;
   bool fKludgeTdcFirstEvent = false;
   bool fKludgeTdcLastEvent = false;

 public: // diagnostics
   double fMaxDt = 0;
   double fMinDt = 0;
   size_t fEventsSizeMax = 0;
   size_t fEventsSizeMaxEver = 0;

 public: // counters
   int fCountUnknown    = 0; // count unknown banks
   int fCountOut        = 0; // count all built events
   int fCount = 0;
   int fCountComplete   = 0; // count complete events e->complete
   int fCountError      = 0; // count events with errors e->error
   int fCountIncomplete = 0; // count incomplete events !e->complete
   int fCountPopAge     = 0; // count of incomplete events poped by age
   int fCountPopFollowingComplete = 0; // count of incomplete events popped by complete following events
   int fCountPopLast    = 0; // count of events popped at the end of run

 public: // public functions
   Evb(TMFE* mfe, SendQueue* send_queue, MVOdb* settings, MVOdb* config, MVOdb* status, MVOdb* variables); // ctor
   ~Evb(); // dtor
   void InitEvbLocked();
   void CheckSyncLocked();
   bool BuildLocked(bool check_too_old);
   EvbEvent* GetLocked();
   EvbEvent* GetLastEventLocked();
   bool EvbTickLocked(bool build_last);

 public: // threads
   bool fRunThread = false;
   std::thread* fThread = NULL;
   void EvbThread();
   void StartThread();
   void StopThread();

 public: // internal functions
   EvbEvent* FindEventLocked(double t, Bank32aBuf *b, bool check_too_old);
   void IncrBank(EvbEvent*e, Bank32aBuf* b);
   void MergeChain(EvbEvent*e, Bank32aBuf* b);
   void CheckEventLocked(EvbEvent *e, bool last_event);
   void BuildBankLocked(Bank32aBuf *b, bool check_too_old);
   void UpdateCountersLocked(const EvbEvent* e);
   EvbEvent* GetNextLocked();
   EvbEvent* GetCompleteLocked();
   void EvbSendEvent(EvbEvent* e);

 public: // reporting
   void PrintEvbLocked() const;
   void PrintEventsLocked() const;
   void LogPwbCounters() const;
   void WriteSyncStatus() const;
   void WriteEvbStatus();
   void WriteVariables();
};

void Evb::EvbThread()
{
   printf("EvbThread: Thread for event builder started\n");
   while (fRunThread) {
      fLock.lock();
      bool done_something = EvbTickLocked(false);
      fLock.unlock();
      if (done_something) {
         //::usleep(100);
      } else {
         ::usleep(10000);
      }
   }
   printf("EvbThread: Thread for event builder stopped\n");
}

void Evb::StartThread()
{
   assert(fThread==NULL);
   fRunThread = true;
   fThread = new std::thread(&Evb::EvbThread, this);
}

void Evb::StopThread()
{
   assert(fThread);
   fRunThread = false;
   fThread->join();
   delete fThread;
   fThread = NULL;
}

static void set_vector_element(std::vector<int>* v, unsigned i, int value, bool overwrite = true)
{
   assert(i<1000); // protect against crazy value
   while (i>=v->size()) {
      v->push_back(-1);
   }
   assert(i<v->size());
   if (overwrite || (*v)[i] < 0) {
      (*v)[i] = value;
   }
}

static int get_vector_element(const std::vector<int>& v, unsigned i)
{
   if (i>=v.size())
      return -1;
   else
      return v[i];
}

Evb::Evb(TMFE* mfe, SendQueue* send_queue, MVOdb* settings, MVOdb* config, MVOdb* status, MVOdb* variables) // ctor
{
   printf("Evb: constructor!\n");
   fMfe = mfe;
   fSendQueue = send_queue;
   fOdbSettings = settings;
   fOdbConfig = config;
   fOdbStatus = status;
   fOdbVariables = variables;
}

void Evb::InitEvbLocked()
{
   if (fEvbState != EVB_STATE_UNCONFIGURED) {
      //cm_msg(MERROR, "Evb::InitEvbLocked", "Event builder is already configured!");
      return;
   }

   cm_msg(MINFO, "Evb::InitEvbLocked", "Initializing the event builder!");

   fOdbSettings->RD("eps_sec", &fConfEpsSec, true);
   fOdbSettings->RD("max_age_sec", &fConfMaxAgeSec, true);
   fOdbSettings->RD("max_dead_sec", &fConfMaxDeadSec, true);
   fOdbSettings->RB("clock_drift", &fConfAllowClockDrift, true);

   fOdbSettings->RD("sync_eps_sec", &fSync.fConfEpsSec, true);
   fOdbSettings->RD("sync_rel_eps", &fSync.fConfRelEps, true);
   fOdbSettings->RU32("sync_min_events", &fSync.fConfSyncMinEvents, true);
   fOdbSettings->RU32("sync_max_events", &fSync.fConfSyncMaxEvents, true);
   fOdbSettings->RD("sync_timeout_sec", &fConfSyncTimeoutSec, true);

   fOdbSettings->RB("print_status",     &fPrintStatus,     true);
   fOdbSettings->RB("print_incomplete", &fPrintIncomplete, true);
   fOdbSettings->RB("print_all",        &fPrintAll,        true);

   //fOdbSettings->RI("max_reader_queue", &fConfMaxReaderQueue, true);
   fOdbSettings->RI("max_send_queue", &fConfMaxSendQueue, true);
   fOdbSettings->RB("drop_send_queue", &fConfDropSendQueue, true);

   fCounter = 0;
   
   fOdbSettings->RB("trace_sync", &fSync.fTrace, true);
   
   // Load configuration from ODB

   std::vector<std::string> name;
   std::vector<int> type;
   std::vector<int> module;
   std::vector<int> nbanks;
   std::vector<double> tsfreq;

   fOdbConfig->RSA("name", &name, false, 0, 0);
   fOdbConfig->RIA("type", &type, false, 0);
   fOdbConfig->RIA("module", &module, false, 0);
   fOdbConfig->RIA("nbanks", &nbanks, false, 0);
   fOdbConfig->RDA("tsfreq", &tsfreq, false, 0);

   assert(name.size() == type.size());
   assert(name.size() == module.size());
   assert(name.size() == nbanks.size());
   assert(name.size() == tsfreq.size());

   // Loop over evb slots

   //int count = 0;
   int count_trg = 0;
   int count_adc = 0;
   int count_pwb = 0;
   int count_tdc = 0;

   fNumSlots = name.size();

   fSlotName.resize(fNumSlots);

   for (unsigned i=0; i<name.size(); i++) {
      printf("Slot %2d: [%s] type %d, module %d, nbanks %d, tsfreq %f\n", i, name[i].c_str(), type[i], module[i], nbanks[i], tsfreq[i]);

      switch (type[i]) {
      default:
         break;
      case 1: { // TRG
         fSync.Configure(i, 2.0*0x80000000, tsfreq[i]);
         set_vector_element(&fTrgSlot, module[i], i);
         set_vector_element(&fNumBanks, i, nbanks[i]);
         set_vector_element(&fSlotType, i, type[i]);
         fSlotName[i] = name[i];
         count_trg++;
         break;
      }
      case 2: { // ADC
         fSync.Configure(i, 2.0*0x80000000, tsfreq[i]);
         set_vector_element(&fAdcSlot, module[i], i);
         set_vector_element(&fNumBanks, i, nbanks[i]);
         set_vector_element(&fSlotType, i, type[i]);
         fSlotName[i] = name[i];
         count_adc++;
         break;
      }
      case 3: { // FEAMrev0
         //fSync.Configure(i, 2.0*0x80000000, tsfreq[i]);
         //set_vector_element(&fPwbSlot, module[i], i);
         //set_vector_element(&fNumBanks, i, nbanks[i]);
         //set_vector_element(&fSlotType, i, type[i]);
         //fSlotName[i] = name[i];
         //count_pwb++;
         break;
      }
      case 4: { // PWB rev1
         //fSync.Configure(i, 2.0*0x80000000, tsfreq[i]);
         //set_vector_element(&fPwbSlot, module[i], i);
         //set_vector_element(&fNumBanks, i, nbanks[i]);
         //set_vector_element(&fSlotType, i, type[i]);
         //fSlotName[i] = name[i];
         //count_pwb++;
         break;
      }
      case 5: { // PWB rev1 with HW UDP
         fSync.Configure(i, 2.0*0x80000000, tsfreq[i]);
         set_vector_element(&fPwbSlot, module[i], i, false);
         set_vector_element(&fNumBanks, i, nbanks[i]);
         set_vector_element(&fSlotType, i, type[i]);
         fSlotName[i] = name[i];
         count_pwb++;
         break;
      }
      case 6: { // TDC
         fSync.Configure(i, 0x10000000, tsfreq[i]);
         set_vector_element(&fTdcSlot, module[i], i, false);
         set_vector_element(&fNumBanks, i, nbanks[i]);
         set_vector_element(&fSlotType, i, type[i]);
         fSlotName[i] = name[i];
         count_tdc++;
         fKludgeTdcKillFirstEvent = true;
         fKludgeTdcFirstEvent = true;
         fKludgeTdcLastEvent = true;
         break;
      }
      }
   }

   printf("For each module:\n");

   printf("TRG map:   ");
   for (unsigned i=0; i<fTrgSlot.size(); i++)
      printf(" %2d", fTrgSlot[i]);
   printf("\n");

   printf("ADC map:  ");
   for (unsigned i=0; i<fAdcSlot.size(); i++)
      printf(" %2d", fAdcSlot[i]);
   printf("\n");

   printf("PWB map: ");
   for (unsigned i=0; i<fPwbSlot.size(); i++)
      printf(" %2d", fPwbSlot[i]);
   printf("\n");

   printf("TDC map: ");
   for (unsigned i=0; i<fTdcSlot.size(); i++)
      printf(" %2d", fTdcSlot[i]);
   printf("\n");

   printf("For each evb slot:\n");

   printf("SlotName: ");
   for (unsigned i=0; i<fSlotName.size(); i++)
      printf(" %s", fSlotName[i].c_str());
   printf("\n");

   printf("SlotType: ");
   for (unsigned i=0; i<fSlotType.size(); i++)
      printf(" %d", fSlotType[i]);
   printf("\n");

   printf("NumBanks: ");
   for (unsigned i=0; i<fNumBanks.size(); i++)
      printf(" %d", fNumBanks[i]);
   printf("\n");

   fCountSlotIncomplete.resize(fNumSlots);

   fDeadSlots.resize(fNumSlots);

   cm_msg(MINFO, "Evb::InitEvbLocked", "Event builder configured with %d slots: %d TRG, %d ADC, %d TDC, %d PWB", fNumSlots, count_trg, count_adc, count_tdc, count_pwb);

   WriteSyncStatus();
   WriteEvbStatus();

   fMaxDt = 0;
   fMinDt = 0;

   fEvbState = EVB_STATE_WAITING_FOR_SYNC;
   fSyncStartTime = TMFE::GetTime();
}

Evb::~Evb()
{
   if (fThread)
      StopThread();

   printf("Evb: max dt: %.0f ns, min dt: %.0f ns\n", fMaxDt*1e9, fMinDt*1e9);
   printf("Evb: dtor!\n");
}

//static double xxx = 0;
//static double yyy = 0;
//static double zzz = 0;

void Evb::PrintEvbLocked() const
{
#if 0
   printf("Per module buffer:\n");

   for (unsigned i=0; i<fBuf.size(); i++) {
      printf("module %d: %d buffered, %d epoch\n", i, (int) fBuf[i].size(), fSync.fModules[i].fEpoch);
   }

   printf("\n");
#endif

   printf("Event buffer:\n");

   printf("Last built:  %.6f\n", fLastBuiltTime);
   bool sorted = true;
   double skew = -1;
   if (!fEvents.empty()) {
      skew = fEvents.back()->time - fEvents.front()->time;
      fEvents[0]->Print();
      printf("\n");
      double now = TMFE::GetTime();
      printf("slot %d, time %.6f, update %f\n", 0, fEvents[0]->time, now - fEvents[0]->update_time);
      for (unsigned i=1; i<fEvents.size(); i++) {
         if (i < 5 || i >= fEvents.size() - 5)
            printf("slot %d, time %.6f, update %.6f, skew %.6f\n", i, fEvents[i]->time, now - fEvents[i]->update_time, fEvents[i]->update_time - fEvents[i]->create_time);
         if (fEvents[i-1]->time >= fEvents[i]->time) {
            //printf("XXX\n");
            sorted = false;
         }
      }
      fEvents.back()->Print();
      printf("\n");
   }

   printf("\n");

   printf("Evb status:\n");
   printf("  Sync: "); fSync.Print(); printf("\n");
   //printf("  FindEvent time %.6f, %.6f and %.6f sec\n", yyy, zzz, xxx);
   //zzz = 0;
   printf("  Errors: TooOld: %d, FindEvent: %d\n", fCountEvbErrorsTooOld, fCountEvbErrorsFindEvent);
   printf("  Buffered output: %d, skew %.6f sec, sorted %d\n", (int)fEvents.size(), skew, sorted);
   printf("  Output %d events: %d complete, %d with errors, %d incomplete\n", fCount, fCountComplete, fCountError, fCountIncomplete);
   //printf("  Slot errors: %d\n", fCountSlotErrors);
#if 0
   printf("  Incomplete count for each slot:\n");
   for (unsigned i=0; i<fCountSlotIncomplete.size(); i++) {
      if (fCountSlotIncomplete[i] > 0) {
         printf("    slot %d, module %s: incomplete count: %d\n", i, fSlotName[i].c_str(), fCountSlotIncomplete[i]);
      }
   }
#endif
#if 0
   for (unsigned i=0; i<fPwbData.size(); i++) {
      const PwbData* d = &fPwbData[i];
      if (d->count_error > 0) {
         printf("slot %d: PWB counters: bad_pkt_seq %d, bad_channel_id %d, bad_format_revision %d, bad_chunk_id %d, no_header %d, lost_header %d, lost_footer %d\n",
                i,
                d->count_bad_pkt_seq,
                d->count_bad_channel_id,
                d->count_bad_format_revision,
                d->count_bad_chunk_id,
                d->count_bo_header,
                d->count_lost_header,
                d->count_lost_footer);
         //uint16_t chunk_id[MAX_PWB_CHAN];
      }
   }
#endif
#if 0
   LogPwbCounters();
#endif
   printf("  Max dt: %.0f ns\n", fMaxDt*1e9);
   printf("  Min dt: %.0f ns\n", fMinDt*1e9);

   //assert(sorted);
   //assert(skew < 10.0);
}

void Evb::PrintEventsLocked() const
{
   printf("Evb dump of buffered events, fEvents size is %d:\n", (int)fEvents.size());
   for (unsigned i=0; i<fEvents.size(); i++) {
      printf("slot %d: ", i);
      fEvents[i]->Print();
      printf("\n");
   }
}

void Evb::WriteSyncStatus() const
{
   fOdbStatus->WB("sync_active",  fSync.fSyncActive);
   fOdbStatus->WB("sync_ok",  fSync.fSyncOk);
   fOdbStatus->WB("sync_failed",   fSync.fSyncFailed);
}

void Evb::WriteEvbStatus()
{
   if (fEvbState == EVB_STATE_UNCONFIGURED) return;

   fOdbStatus->WI("unknown",          fCountUnknown);
   fOdbStatus->WI("complete",         fCountComplete);
   fOdbStatus->WI("incomplete",       fCountIncomplete);
   fOdbStatus->WI("pop_age",          fCountPopAge);
   fOdbStatus->WI("pop_following",    fCountPopFollowingComplete);
   fOdbStatus->WI("pop_last",         fCountPopLast);
   fOdbStatus->WI("error",            fCountError);
   fOdbStatus->WI("count_dead_slots", fCountDeadSlots);
   fOdbStatus->WI("evb_queue_size",   fEventsSizeMax);
   fEventsSizeMax = 0;
   fOdbStatus->WI("evb_queue_size_max", fEventsSizeMaxEver);
   fOdbStatus->WI("evb_errors_too_old", fCountEvbErrorsTooOld);
   fOdbStatus->WI("evb_errors_find_event", fCountEvbErrorsFindEvent);

   fOdbStatus->WSA("names", fSlotName, 32);
   fOdbStatus->WIA("dead", fDeadSlots);
   fOdbStatus->WIA("incomplete_count", fCountSlotIncomplete);
}

void Evb::WriteVariables()
{
}

EvbEvent* Evb::FindEventLocked(double t, Bank32aBuf* b, bool check_too_old)
{
   double amin = 0;

   bool push_front = false;
   bool push_back  = false;
   auto it = fEvents.begin();

   int cmpcount = 0;

   //if (!fEvents.empty()) {
   //   printf("FindEventLocked: timestamp %.6f sec: event buffer has %d events, time range %.6f..%.6f, cutoff time %.6f\n", t, (int)fEvents.size(), fEvents.front()->time, fEvents.back()->time, fEvents.back()->time - fConfMaxAgeSec);
   //}

   if (fEvents.empty()) {
      push_back = true;
      //printf("first %.6f!\n", t);
   } else if (t > fEvents.back()->time + 10.0) {
      // event from far future
      if (fCountEvbErrorsFindEvent < 10) {
         cm_msg(MERROR, "Evb::FindEventLocked", "Rejecting event from the future, timestamp %.6f sec: event buffer has %d events, time range %.6f..%.6f, cutoff time %.6f", t, (int)fEvents.size(), fEvents.front()->time, fEvents.back()->time, fEvents.back()->time - fConfMaxAgeSec);
      }
      fCountEvbErrorsFindEvent++;
      return NULL;
   } else if (check_too_old && (t < fEvents.back()->time - fConfMaxAgeSec)) {
      // this check is to prevent the Evb from bogging down
      // when fEvents for some reason gets too long and start taking
      // a lot of time to walk it to find the right time. So basically
      // we refuse to build events that are too old. K.O.
      if (fCountEvbErrorsFindEvent < 10) {
         cm_msg(MERROR, "Evb::FindEventLocked", "Rejecting event with invalid timestamp %.6f sec: event buffer has %d events, time range %.6f..%.6f, cutoff time %.6f", t, (int)fEvents.size(), fEvents.front()->time, fEvents.back()->time, fEvents.back()->time - fConfMaxAgeSec);
      }
      fCountEvbErrorsFindEvent++;
      return NULL;
   } else if (t < fEvents.front()->time - fConfEpsSec) {
      push_front = true;
      //printf("find event for time %.6f: %.6f, diff %.6f, push front\n", t, (*it)->time, (*it)->time - t);
   } else if (t > fEvents.back()->time + fConfEpsSec) {
      push_back = true;
      //printf("find event for time %.6f: %.6f, diff %.6f, push back\n", t, (*it)->time, (*it)->time - t);
   } else {
      for (it=fEvents.end()-1; 1; it--) {
         //printf("find event for time %.6f: %.6f, diff %.6f\n", t, (*it)->time, (*it)->time - t);
         cmpcount++;
         
         double tt = (*it)->time;
         double dt = tt - t;
         double adt = fabs(dt);

         if (dt < -fConfEpsSec) { // dt is negative
            // fEvents is sorted by time
            //printf("find event for time %.6f: %.6f, diff %.6f, insert here\n", t, (*it)->time, (*it)->time - t);
            //PrintEvbLocked();
            //assert(!"here!");
            it++;
            break;
         } else if (dt > fConfEpsSec) { // dt is positive
         } else { // dt is zero
            if (adt > fMaxDt) {
               //printf("AgEVB: for time %f found event at time %f, new max dt %.0f ns, old max dt %.0f ns\n", t, fEvents[i]->time, adt*1e9, fMaxDt*1e9);
               fMaxDt = adt;
            }
            //printf("Found event for time %f\n", t);
            //printf("Found event for time %f: event %d of %d, %f, diff %f %.0f ns\n", t, i, fEvents.size(), fEvents[i]->time, dt, dt*1e9);
            //printf("found cmpcount %d, size %d\n", cmpcount, (int)fEvents.size());
            //if (cmpcount > 0.5*fEvents.size()) {
            //   printf("CCC %d size %d, times %.6f %.6f %.6f\n", cmpcount, (int)fEvents.size(), fEvents.front()->time, t, fEvents.back()->time);
            //}
            return (*it);
         }
         
         if (amin == 0)
            amin = adt;
         if (adt < amin)
            amin = adt;
         
         //if (dt < 0) {
         //   // fEvents is sorted by time
         //   //printf("find event for time %.6f: %.6f, diff %.6f, insert here\n", t, (*it)->time, (*it)->time - t);
         //   //PrintEvbLocked();
         //   //assert(!"here!");
         //   it++;
         //   break;
         //}
      }
   }

   //if (cmpcount > 0.5*fEvents.size()) {
   //   printf("QQQ %d size %d\n", cmpcount, (int)fEvents.size());
   //}

   if (0) {
      printf("Creating new event for time %f, already buffered events do not match this time:\n", t);
      b->Print();
      printf("\n");
   
      for (unsigned i=0; i<fEvents.size(); i++) {
         printf("Slot %d: ", i);
         //printf("find event for time %f: event %d, %f, diff %f\n", t, i, fEvents[i]->time, fEvents[i]->time - t);
         fEvents[i]->Print();
         printf("\n");
      }
   }
   
   if (fMinDt == 0)
      fMinDt = amin;

   if (amin < fMinDt)
      fMinDt = amin;
   
   EvbEvent* e = new EvbEvent();
   e->create_time = TMFE::GetTime();
   e->update_time = e->create_time;
   e->complete = false;
   e->error = false;
   e->counter = fCounter++;
   e->time = t;

   assert(e->banks_count.size() == 0);
   assert(e->banks_waiting.size() == 0);
   for (unsigned i=0; i<fNumBanks.size(); i++) {
      e->banks_count.push_back(0);
      e->banks_waiting.push_back(fNumBanks[i]);
   }
   
   if (push_front) {
      fEvents.push_front(e);
   } else if (push_back) {
      fEvents.push_back(e);
   } else {
      //printf("insert %.6f %.6f %.6f\n", (*(it-1))->time, e->time, (*it)->time);
      fEvents.insert(it, e);
      //PrintEvbLocked();
      //assert(!"here!");
   }

   if (fEvents.size() > fEventsSizeMax) {
      fEventsSizeMax = fEvents.size();
   }
   if (fEvents.size() > fEventsSizeMaxEver) {
      fEventsSizeMaxEver = fEvents.size();
   }

   if (0) {
      printf("New event for time %.6f: ", t);
      b->Print();
      printf("\n");
   }
   
   return e;
}

void Evb::IncrBank(EvbEvent* e, Bank32aBuf* b)
{
   e->error |= b->fError;

   int slot = b->fSlot;

   int bw = e->banks_waiting[slot];

   if (0) {
      printf("adding slot %d waiting_incr %d to bw %d: ", slot, b->fWaitingIncr, bw);
      e->Print();
      printf("\n");
   }

   e->banks_count[slot]++;

   if (b->fWaitingIncr == 999999) {
      // excessive data from PWBs
   } else if (bw > 0) {
      bw -= b->fWaitingIncr;
   } else {
      fMfe->Msg(MERROR, "EvbEvent::MergeSlot", "Error: slot %d: too many banks, banks_waiting %d, waiting_incr %d", slot, bw, b->fWaitingIncr);
   }

   e->banks_waiting[slot] = bw;

   if (bw == 0)
      e->maybe_complete = true;
}

void Evb::MergeChain(EvbEvent* e, Bank32aBuf* b)
{
   e->update_time = TMFE::GetTime();

   if (e->banks) {
      e->banks->AddChain(b);
   } else {
      e->banks = b;
   }

   while (b) {
      IncrBank(e, b);
      b = b->fNext;
   }
}

void Evb::CheckEventLocked(EvbEvent *e, bool last_event)
{
   assert(e);

   // shortcut for events we know are incomplete
   if (!e->maybe_complete && !last_event)
      return;

   assert(e->banks);
   assert(e->banks_count.size() == fNumBanks.size());
   assert(e->banks_waiting.size() == fNumBanks.size());

   bool is_first_event = (fabs(e->time - 0.0) < fConfEpsSec); 

   if (0) {
      printf("check event: ");
      e->Print();
      printf("\n");
   }

   bool complete = true;

   int num_slots = fNumBanks.size();
   for (int i=0; i<num_slots; i++) {
      if (fSync.fModules[i].fDead) {
         continue;
      }

      if (e->banks_waiting[i] > 0) {
         //printf("event %d, slot %d, last_event %d, kludge %d, slot type %d\n", e->counter, i, last_event, gKludgeTdcLastEvent, fSlotType[i]);
         if (fKludgeTdcFirstEvent && fSlotType[i] == 6 && is_first_event) {
            fMfe->Msg(MINFO, "Evb::CheckEventLocked", "Kludge: ignoring lack of TDC data in the first event");
            fKludgeTdcFirstEvent = false;
         } else if (last_event && fKludgeTdcLastEvent && fSlotType[i] == 6) {
            fMfe->Msg(MINFO, "Evb::CheckEventLocked", "Kludge: ignoring lack of TDC data in the last event");
            fKludgeTdcLastEvent = false;
         } else {
            complete = false;
         }
      }

      if (0) {
         printf("slot %d: type %d, should have %d, have %d, waiting %d, complete %d\n", i, fSlotType[i], fNumBanks[i], e->banks_count[i], e->banks_waiting[i], complete);
      }

      if (!complete)
         break;
   }

   e->complete = complete;
   e->maybe_complete = false;

   //e->Print();
}

void Evb::BuildBankLocked(Bank32aBuf* b, bool check_too_old)
{
   b->fTime = fSync.fModules[b->fSlot].GetTime(b->fTs, b->fEpoch);

   if (b->fTime <= fLastBuiltTime - fConfEpsSec) {
      // this event is in the past
      if (fCountEvbErrorsTooOld < 10) {
         fMfe->Msg(MERROR, "Evb::BuildSlotLocked", "Dropping event from slot %d with event timestamp %.6f sec in the past: older than %.6f sec", b->fSlot, b->fTime, fLastBuiltTime);
      }
      fCountEvbErrorsTooOld++;
      delete b;
      return;
   }

   //double t0 = TMFE::GetTime();

   EvbEvent* e = FindEventLocked(b->fTime, b, check_too_old);

   //double t1 = TMFE::GetTime();

   //double dt = t1 - t0;
   //yyy = dt;
   //if (dt > zzz) {
   //   printf("ZZZ FindEvent time: %f -> %f ZZZ\n", zzz, dt);
   //   zzz = dt;
   //}
   //if (dt > xxx) {
   //   printf("XXX FindEvent time: %f -> %f XXX\n", xxx, dt);
   //   xxx = dt;
   //}

   if (!e) {
      // rejected by FindEvent()
      //cm_msg(MERROR, "Evb::BuildSlotLocked", "Dropping event from slot %d with timestamp %.6f rejected by FindEventLocked()", slot, m->time);
      //fCountEvbErrors++;

      if (0) {
         PrintEvbLocked();
         printf("dropped bank: "); b->Print(); printf("\n");
      }

      delete b;
      b = NULL;
      return;
   }

   assert(e);

#if 0
   if (0 && index == 1) {
      printf("offset: %f %f, index %d, ts 0x%08x, epoch %d, feam time %f\n", e->time, m->time, index, m->ts, m->epoch, m->feam->time);
   }
#endif

   if (fConfAllowClockDrift) { // adjust offset for clock drift
      double off = e->time - b->fTime;
      //printf("offset: %f %f, diff %f, index %d\n", e->time, m->time, off, slot);
      fSync.fModules[b->fSlot].fOffsetSec += off/2.0;
   }

   MergeChain(e, b);

   CheckEventLocked(e, false);
}

bool Evb::BuildLocked(bool check_too_old)
{
   if (fEvbState == EVB_STATE_WAITING_FOR_SYNC) {
      double dt = TMFE::GetTime() - fSyncStartTime;
      printf("sync timeout %.6f sec\n", dt);
      if (dt > fConfSyncTimeoutSec) {
         CheckSyncLocked();
      }
   }

   if (fEvbState != EVB_STATE_RUNNING)
      return false;

   bool done_something = false;

   double t1 = TMFE::GetTime();
   int loops = 0;
   while (1) {
      //printf("B");
      bool done_nothing = true;
      if (!fBanks.empty()) {
         Bank32aBuf* b = fBanks.front();
         fBanks.pop_front();
         //delete b;
         BuildBankLocked(b, check_too_old);
         done_something = true;
         done_nothing = false;
         loops++;
         
         EvbEvent* e = GetCompleteLocked();
         if (e) {
            //printf("E");
            EvbSendEvent(e);
         }
      }
      if (done_nothing)
         break;

      double t2 = TMFE::GetTime();
      double dt = t2-t1;
      if (dt > 0.20) {
         fMfe->Msg(MERROR, "Evb::BuildLocked", "EvbBuildLocked() took %f sec, %d loops", dt, loops);
         //fStall = true;
         break;
      }
      //fStall = false;
   }

   return done_something;
}

void Evb::UpdateCountersLocked(const EvbEvent* e)
{
   fCount++;
   if (e->error) {
      if (fCountError == 0) {
         cm_msg(MERROR, "Evb::UpdateCountersLocked", "First event event with errors");
      }
      fCountError++;
   }

   if (e->complete) {
      fCountComplete++;
   } else {
      if (fCountIncomplete == 0) {
         cm_msg(MERROR, "Evb::UpdateCountersLocked", "First incomplete event");
      }

      fCountIncomplete++;

      for (unsigned i=0; i<fNumSlots; i++) {
         if (fSync.fModules[i].fDead) {
            fDeadSlots[i] = true;
            continue;
         }

         if (e->banks_waiting[i] > 0) {
            fCountSlotIncomplete[i]++;
         }

         if (0) {
            printf("slot %d: type %d, should have %d, have %d\n", i, fSlotType[i], fNumBanks[i], e->banks_count[i]);
         }
      }
   }
}

EvbEvent* Evb::GetCompleteLocked()
{
   if (fEvents.empty()) {
      return NULL;
   }

   EvbEvent* e = fEvents.front();

   if (!e->complete)
      return NULL;
   
   fEvents.pop_front();

   if (fPrintAll) {
      e->Print();
      printf("\n");
   }
   
   UpdateCountersLocked(e);
   return e;
}

EvbEvent* Evb::GetLastEventLocked()
{
   if (fEvents.empty())
      return NULL;
   
   EvbEvent* e = fEvents.front();
   fEvents.pop_front();

   fCountPopLast += 1;

   if (fPrintAll) {
      e->Print();
      printf(" (last event)\n");
   }

   bool is_last_event = fEvents.empty();

   CheckEventLocked(e, is_last_event);
   UpdateCountersLocked(e);
   return e;
}

EvbEvent* Evb::GetNextLocked()
{
   EvbEvent* e = fEvents.front();

   // top of queue has a complete event, return it!

   if (e->complete) {
      fEvents.pop_front();
      fLastBuiltTime = e->time;
      return e;
   }

   double now = TMFE::GetTime();
   double age = now - e->update_time;
   
   // There is a problem:
   // at the end of run, we have a race condition.
   // fectrl stops the trigger then does other stuff for a long time.
   // after it finishes, femudp, fetdc end of run handlers run.
   // at the same time, this timeout here is running and after
   // fConfMaxAgeSec seconds, it expires the last incomplete event(s)
   // that are missing the TDC data and are normally processed by
   // the feevb end of run handler. So the time from fectrl stopping
   // the trigger and the running of feevb end of run handler
   // has to be shorter than fConfMaxAgeSec. Value 3 sec is too short.
   // Value 5 sec seems to be ok. K.O. Sept 2021.

   if (age > fConfMaxAgeSec) {
      // this event has not been updated for too long,
      // it will never become complete, pop it out!
      fEvents.pop_front();

      if (fPrintIncomplete) {
         printf("Evb::GetNextLocked: popping old incomplete event with timestamp %.6f sec, age %.3f sec\n", e->time, age);
         e->Print(1);
         printf("\n");
      }

      fCountPopAge += 1;
      
      fLastBuiltTime = e->time;
      return e;
   }

   // top of queue has an incomplete event,
   // check if there is a complete event down the line

   for (unsigned i=0; i<fEvents.size(); i++) {
      if (fEvents[i]->complete) {
         // a later event has been completed,
         // this event will never become complete.
         fEvents.pop_front();

         if (fPrintIncomplete) {
            printf("Evb::GetNextLocked: found complete event at %d, popping this incomplete event!\n", i);
            e->Print(1);
            printf("\n");
         }

         fCountPopFollowingComplete += 1;

         fLastBuiltTime = e->time;
         return e;
      }
   }

   // marinade this event for some more
   e = NULL;
   return NULL;
}

EvbEvent* Evb::GetLocked()
{
   if (fEvents.empty()) {
      return NULL;
   }

   if (fTrace) {
      printf("Evb::Get: ");
      PrintEvbLocked();
      printf("\n");
   }
   
   EvbEvent* e = GetNextLocked();

   if (!e) {
      return NULL;
   }

   if (fPrintAll) {
      e->Print();
      printf("\n");
   }
   
   //DWORD t3 = ss_millitime();
   //fEvents.pop_front();
   UpdateCountersLocked(e);
   //DWORD t4 = ss_millitime();
   //DWORD dt = t4-t1;
   //if (dt > 1)
   //   printf("Get() t1t2 %d, t2t3 %d, t3t4 %d, dt %d\n", t2-t1, t3-t2, t4-t3, dt);
   return e;
}

void Evb::EvbSendEvent(EvbEvent* e)
{
   //printf("Have EvbEvent: ");
   //e->Print();
   //printf("\n");
   
   Bank32aBuf* b = e->banks;
   e->banks = NULL;
   delete e;
   e = NULL;
   
   if (!b) {
      return;
   }
   
   if (fCountOut == 0) {
      fMfe->Msg(MINFO, "build_thread", "Event builder built the first event");
   }
   
   fCountOut++;
   fSendQueue->PushEvent(b);
   b = NULL;
}

bool Evb::EvbTickLocked(bool build_last)
{
   //double t1 = TMFE::GetTime();

   bool done_something = false;

   done_something |= BuildLocked(true);

   //for (int i=0; i<1000; i++) {
   int count = 0;
   while (1) {
      EvbEvent* e = GetLocked();
      
      if (!e && build_last) {
         e = GetLastEventLocked();
      }
   
      //DWORD t2 = ss_millitime();
      //DWORD dt = t2 - t1;
      //if (dt > 0) {
      //   printf("Get() took %d ms\n", dt);
      //}
      
      if (!e)
         return done_something;

      count++;
      EvbSendEvent(e);
   }

   //printf("%d.", count);

   return true;
}

void Evb::CheckSyncLocked()
{
   if (fSync.fSyncActive) {
      fMfe->Msg(MERROR, "Evb::CheckSyncLocked", "Event builder timestamp sync timeout after %.6f sec", TMFE::GetTime() - fSyncStartTime);
      fSync.fSyncFailed = true;
      fSync.PostSync();
      assert(fSync.fSyncActive == false);
   }

   if (fSync.fSyncOk) {

      fEvbState = EVB_STATE_RUNNING;

      int count_dead = 0;
      std::string deaders = "";
      for (unsigned i=0; i<fSync.fModules.size(); i++) {
         if (fSync.fModules[i].fDead) {
            if (count_dead > 0)
               deaders += ", ";
            deaders += fSlotName[i];
            count_dead++;
         }
      }

      if (count_dead > 0) {
         fMfe->Msg(MERROR, "Evb::CheckSyncLocked", "Event builder timestamp sync successful after %.6f sec. %d dead modules: %s", TMFE::GetTime() - fSyncStartTime, count_dead, deaders.c_str());
      } else {
         fMfe->Msg(MINFO, "Evb::CheckSyncLocked", "Event builder timestamp sync successful after %.6f sec", TMFE::GetTime() - fSyncStartTime);
      }
      
      if (0) {
         fSync.Dump();
         fSync.Print();
         printf("\n");
         //exit(123);
      }
      
      // build accumulated sync events
      for (int i=0; ; i++) {
         // disable check for too-old-events
         // because typical time span of sync
         // events is longer than typical setting
         // of fConfMaxAgeSec - 10 events with about 1 sec
         // spacing makes first event about 10 seconds
         // old vs typical fConfMaxAgeSec set to 3 sec. K.O.
         
         bool done_something = BuildLocked(false);
         if (!done_something) {
            fMfe->Msg(MINFO, "Evb::CheckSyncLocked", "Built sync events, %d loops", i);
            break;
         }
      }
      
      if (fPrintStatus)
         PrintEvbLocked();
      
   } else if (fSync.fSyncFailed) {
      fEvbState = EVB_STATE_SYNC_FAIL;
      fMfe->Msg(MERROR, "Evb::CheckSyncLocked", "Event builder timestamp sync FAILED after %.6f sec", TMFE::GetTime() - fSyncStartTime);
   } else {
      assert(!"we should never come here!");
   }
}

static int CountBits(uint32_t bitmap)
{
   int count = 0;
   while (bitmap != 0) {
      if (bitmap & 1)
         count++;
      bitmap>>=1;
   }
   return count;
}

class BufferReader
{
public:
   TMFE* fMfe = NULL;

public:
   std::mutex  fLock;
   std::string fBufName;
   int fBh = -1; // MIDAS event buffer handle
   int fReqId = -1; // MIDAS event request ID

public:
   Evb* fEvb = NULL;
   PerSlotInfo* fInfo = NULL;

public:
   int fEventsIn = 0;

public:
   std::thread* fThread = NULL;
   bool fRunThread = false;

public:
   std::deque<Bank32aBuf*> fBankBuf;
   size_t fSize = 0;
   size_t fMaxSize = 0;
   size_t fMaxSizeEver = 0;
   size_t fFlushThreshold = 2000;

public:
   //bool fKludgeFlush = false;
   //int  fKludgeFlushCount = 0;

public:
   BufferReader(TMFE* mfe, const char* bufname) // ctor
   {
      fMfe = mfe;
      fBufName = bufname;
   }

   ~BufferReader() // dtor
   {
      if (fThread)
         StopThread();

      CloseBuffer();

      if (!fBankBuf.empty()) {
         int count = 0;
         for (unsigned i=0; i<fBankBuf.size(); i++) {
            Bank32aBuf* b = fBankBuf[i];
            if (b) {
               delete b;
               fBankBuf[i] = NULL;
               count++;
            }
         }
         if (count>0) {
            fMfe->Msg(MERROR, "BufferReader::dtor", "Deleted %d buffered banks", count);
         }
      }

      fEvb  = NULL;
      fInfo = NULL;
   }

public:
   void SetEvb(Evb* evb, PerSlotInfo* info)
   {
      std::lock_guard<std::mutex> lock(fLock);
      fEvb = evb;
      fInfo = info;
      // implicit unlock
   }

public:
   void WriteStatus(MVOdb* odb)
   {
      std::string n1 = "reader_" + fBufName + "_queue_size"; 
      std::string n2 = "reader_" + fBufName + "_queue_size_max";
      std::string n3 = "reader_" + fBufName + "_queue_full";

      odb->WI(n1.c_str(), fMaxSize);
      fMaxSize = 0;
      odb->WI(n2.c_str(), fMaxSizeEver);
      odb->WI(n3.c_str(), 0);
   }

private:
   bool OpenBuffer()
   {
      if (fBh >= 0)
         return true;
      
      int status;
      int evid = -1;
      int trigmask = 0xFFFF;

      fBh = -1;
      
      status = bm_open_buffer(fBufName.c_str(), 0, &fBh);
      if (status != BM_SUCCESS && status != BM_CREATED) {
         cm_msg(MERROR, "frontend_init", "Error: bm_open_buffer(\"%s\") status %d", fBufName.c_str(), status);
         fBh = -1;
         return false;
      }
      
      fReqId = -1;
      status = bm_request_event(fBh, evid, trigmask, GET_ALL, &fReqId, NULL);
      if (status != BM_SUCCESS) {
         cm_msg(MERROR, "frontend_init", "Error: bm_request_event() status %d", status);
         fReqId = -1;
         return false;
      }

      fMfe->Msg(MINFO, "frontend_init", "Event builder reading from buffer \"%s\", evid %d, trigmask 0x%x", fBufName.c_str(), evid, trigmask);

      //fKludgeFlush = true;
      //fKludgeFlushCount = 0;
      
      return true;
   }

   bool CloseBuffer()
   {
      int status;
      bool ok = true;
      
      if (fBh >= 0) {
         if (fReqId >= 0) {
            status = bm_remove_event_request(fBh, fReqId);
            
            if (status != BM_SUCCESS) {
               fMfe->Msg(MERROR, "Reader::CloseBuffer", "bm_remove_event_request(%d,%d) status %d", fBh, fReqId, status);
               ok = false;
            }
            
            fReqId = -1;
         }

         status = bm_close_buffer(fBh);

         if (status != BM_SUCCESS) {
            fMfe->Msg(MERROR, "Reader::CloseBuffer", "bm_close_buffer(%d) status %d", fBh, status);
            ok = false;
         }
         
         fBh = -1;
      }

      return ok;
   }

public:
   void XAddBank(Bank32aBuf* b)
   {
      int imodule = b->fSlot;
      //uint32_t ts = b->fTs;
      
      assert(imodule >= 0);
      assert(imodule < (int)fEvb->fSync.fModules.size());
      
      // N.B. this requires that each thread always handles the same modules,
      // i.e. thread 1 does modules 12 and 13, thread 2 does modules 24 and 15.
      // not permitted for thread 1 to handle module 11 and thread 2 to also handle module 11.
      // K.O.
      
      bool dupe = false;
      
      if (fEvb->fSync.fSyncActive) {
         dupe = fEvb->fSync.fModules[imodule].IsDupe(b->fTs);
      }
      
      b->fEpoch = fEvb->fSync.fModules[imodule].NextEpoch(b->fTs);

      //if (imodule == 0) {
      //   printf("slot %d, ts 0x%08x, epoch %d\n", imodule, b->fTs, b->fEpoch);
      //}
      
      if (fEvb->fSync.fSyncActive && !dupe) {
         std::lock_guard<std::mutex> lock(fEvb->fLock);
         if (fEvb->fSync.fSyncActive) {
            fEvb->fSync.Sync(imodule, b->fTs, b->fEpoch);
         }
      }

      if (fEvb->fSync.fSyncOk) {
         b->fTime = fEvb->fSync.fModules[imodule].GetTime(b->fTs, b->fEpoch);

         if (!fBankBuf.empty()) {
            double tback = fBankBuf.back()->fTime;
            if (tback != 0) {
               //printf("push slot %d, ts %08x+%d %.6f to slot %d, 0x%08x+%d %.6f diff %.6f\n", b->fSlot, b->fTs, b->fEpoch, b->fTime, fBankBuf.back()->fSlot, fBankBuf.back()->fTs, fBankBuf.back()->fEpoch, fBankBuf.back()->fTime, b->fTime - fBankBuf.back()->fTime);

               if (fabs(tback - b->fTime) < fEvb->fConfEpsSec) {
                  fBankBuf.back()->AddChain(b);
                  b = NULL;
               }
            }
         }
      } else {
         b->fTime = 0;
      }

      if (b) {
         fBankBuf.push_back(b);
         
         size_t size = fBankBuf.size();
         
         fSize = size;
         if (size > fMaxSize)
            fMaxSize = size;
         if (size > fMaxSizeEver)
            fMaxSizeEver = size;
      }
   }

   double fLastFlushTime = 0;

   bool XFlushBank()
   {
      if (fEvb->fSync.fSyncFailed) {
         while (!fBankBuf.empty()) {
            Bank32aBuf* b = fBankBuf.front();
            fBankBuf.pop_front();
            delete b;
         }
         return true;
      }
      
      if (fEvb->fSync.fSyncActive) {
         return false;
      }

      //for (auto b: fBankBuf) {
      //   b->fTime = fEvb->fModules[b->fSlot].GetTime(b->fTs, b->fEpoch);
      //}
      
      if (!fEvb) {
         if (fBankBuf.empty())
            return false;
         fMfe->Msg(MERROR, "Handler::XFlushBank", "XFlushBank: Have %d buffered banks but no EVB", (int)fBankBuf.size());
         return false;
      }

      int flush_count = 0;

      fEvb->fLock.lock();

      while (!fBankBuf.empty()) {
         Bank32aBuf* b = fBankBuf.front();
         fBankBuf.pop_front();
         fEvb->fBanks.push_back(b);
         flush_count++;
      }

      fEvb->fLock.unlock();

      fLastFlushTime = TMFE::GetTime();

      //printf("XFlushBank: flushed %d\n", flush_count);
      return flush_count > 0;
   }

   bool XMaybeFlushBank(const char* bufname)
   {
      if (fBankBuf.empty()) {
         return false;
      }

      if (fBankBuf.size() >= fFlushThreshold) {
         return XFlushBank();
      }

      if (fLastFlushTime == 0) {
         return XFlushBank();
      }

      double elapsed = TMFE::GetTime() - fLastFlushTime;

      if (elapsed > 0.010) {
         //if (strstr(bufname, "BUFTRG") || strstr(bufname, "BUFTDC")) {
         //   printf("[%s,%.6f,%d]", bufname, elapsed, (int)fBankBuf.size());
         //}
         return XFlushBank();
      }

      return false;
   }

public:
   bool AddTrgBank(const char* bkname, const char* pbank, int bklen, int bktype, PerSlotInfo* info)
   {
      //printf("AddTrgBank: name [%s] len %d type %d, tid_size %d\n", bkname, bklen, bktype, rpc_tid_size(bktype));
      
      TrgPacket p;
      p.Unpack(pbank, bklen);
      
      //p.Print();
      //printf("\n");
      
      int imodule = 1;
      
      int islot = get_vector_element(fEvb->fTrgSlot, imodule);
      
      if (islot < 0) {
         return false;
      }
      
      info->fLastTimeSec[islot] = TMFE::GetTime();
      info->fCountPackets[islot] += 1;
      info->fCountBytes[islot] += bklen;
      
      uint32_t ts = p.ts_625;
      
      Bank32aBuf *b = new Bank32aBuf(bkname, TID_DWORD, pbank, bklen, 1, islot, ts);
      
      XAddBank(b);
      
      return true;
   }

   bool AddAdcBank(int imodule, const char* pbank, int bklen, PerSlotInfo* info)
   {
      Alpha16info p;
      int status = p.UnpackAdc(pbank, bklen);

      //if (imodule == 4) {
      //   printf("adc module %d size %d unpack status %d\n", imodule, bklen, status);
      //   p.PrintAdc();
      //}

      //if ((imodule == 1) && (p.channelType == 128) && (p.channelId == 0)) {
      //   static int trigno = 0;
      //   if (p.acceptedTrigger != trigno) {
      //      printf("adc imodule %2d: ", imodule); p.PrintAdcHeader(); printf(" expected %d got %d, diff %d\n", trigno, p.acceptedTrigger, p.acceptedTrigger - trigno);
      //      trigno = p.acceptedTrigger;
      //   }
      //   trigno++;
      //}
      
      //if ((imodule == 1)) {
      //   static uint32_t ts = 0;
      //   if (p.eventTimestamp < ts) {
      //      printf("adc imodule %2d: ", imodule); p.PrintAdcHeader(); printf(" misordered ts 0x%08x vs 0x%08x\n", p.eventTimestamp, ts);
      //   }
      //   ts = p.eventTimestamp;
      //}
      
      if (status != 0) {
         // FIXME: unpacking error
         //printf("unpacking error!\n");
         return false;
      }
      
      int xmodule = imodule;
      
      if (p.channelType == 128) {
         xmodule += 100;
      }
      
      int islot = get_vector_element(fEvb->fAdcSlot, xmodule);
      
      //printf("adc module %d slot %d\n", imodule, islot);
      
      if (islot < 0) {
         return false;
      }

      info->fCountSent0[islot] += 1;
      info->fCountSent1[islot] += p.nsamples_supp;

      info->fCountThr0[islot] += 1;
      info->fCountThr1[islot] += p.keep_bit;
      
      char cname = 0;
      if (p.channelId <= 9) {
         cname = '0' + p.channelId;
      } else {
         cname = 'A' + p.channelId - 10;
      }
      
      info->fLastTimeSec[islot] = TMFE::GetTime();
      info->fCountPackets[islot] += 1;
      info->fCountBytes[islot] += bklen;
      
      char newname[5];
      
      if (p.channelType == 0) {
         sprintf(newname, "%c%02d%c", 'B', imodule, cname);
         //printf("bank name [%s]\n", newname);
      } else if (p.channelType == 128) {
         sprintf(newname, "%c%02d%c", 'C', imodule, cname);
      } else {
         sprintf(newname, "XX%02d", imodule);
      }
      
#if 0
      if (info.channelType == 128) {
         printf("bank %s islot %d imodule %d xmodule %d channel %d timestamp 0x%08x\n", newname, islot, imodule, xmodule, info.channelId, info.eventTimestamp);
      }
#endif
      
      Bank32aBuf *b = new Bank32aBuf(newname, TID_BYTE, pbank, bklen, 1, islot, p.eventTimestamp);
      
      XAddBank(b);
      
      return true;
   };
   
   bool AddPwbBank(int imodule, const char* bkname, const char* pbank, int bklen, int bktype, PerSlotInfo* info)
   {
      int jslot = get_vector_element(fEvb->fPwbSlot, imodule);
      
      if (jslot < 0) {
         return false;
      }

      const uint32_t *p32 = (const uint32_t*)pbank;
      const int n32 = bklen/4;

      bool error = false;

      if (0) {
         unsigned nprint = n32;
         nprint=10;
         for (unsigned i=0; i<nprint; i++) {
            printf("PB05[%d]: 0x%08x (%d)\n", i, p32[i], p32[i]);
            //e->udpData.push_back(p32[i]);
         }
      }

      char nbkname[5];
      nbkname[0] = 'P';
      nbkname[1] = 'C';
      nbkname[2] = bkname[2];
      nbkname[3] = bkname[3];
      nbkname[4] = 0;

      //uint32_t fake_ts_clk = 16 + evb->fConfEpsSec * evb->fSync.fModules[jslot].fFreqHz;
      uint32_t fake_ts_clk = 0;

      //printf("fake_ts_clk %d 0x%x, %f %f\n", fake_ts_clk, fake_ts_clk, evb->fConfEpsSec, evb->fSync.fModules[jslot].fFreqHz);

      uint32_t DEVICE_ID   = p32[0];
      uint32_t PKT_SEQ     = p32[1];
      uint32_t CHANNEL_SEQ = (p32[2] >>  0) & 0xFFFF;
      uint32_t CHANNEL_ID  = (p32[2] >> 16) & 0xFF;
      uint32_t FLAGS       = (p32[2] >> 24) & 0xFF;
      uint16_t CHUNK_ID    = (p32[3] >>  0) & 0xFFFF;
      uint32_t CHUNK_LEN   = (p32[3] >> 16) & 0xFFFF;
      uint32_t HEADER_CRC  = p32[4];
      uint32_t end_of_payload = 5*4 + CHUNK_LEN;
      uint32_t payload_crc = p32[end_of_payload/4];
   
      if (0) {
         printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x, LEN 0x%04x, CRC 0x%08x, bank bytes %d, end of payload %d, CRC 0x%08x\n",
                DEVICE_ID,
                PKT_SEQ,
                CHANNEL_SEQ,
                CHANNEL_ID,
                FLAGS,
                CHUNK_ID,
                CHUNK_LEN,
                HEADER_CRC,
                bklen,
                end_of_payload,
                payload_crc);
      }

      PwbData* dj = &info->fPwbData[jslot];

      bool bad_pkt_seq = false;

      if ((PKT_SEQ == 0) && (dj->pkt_seq == 0xFFFFFFFF)) {
         fMfe->Msg(MLOG, "AddPwbBank", "%s: UDP packet counter wraparound: PKT_SEQ 0x%08x -> 0x%08x", fEvb->fSlotName[jslot].c_str(), dj->pkt_seq, PKT_SEQ);
      } else if (dj->pkt_seq == 0) {
         fMfe->Msg(MLOG, "AddPwbBank", "%s: received first UDP packet counter: PKT_SEQ 0x%08x", fEvb->fSlotName[jslot].c_str(), PKT_SEQ);
      } else if (dj->pkt_seq+1 != PKT_SEQ) {
         printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x -- Error: PKT_SEQ jump 0x%08x to 0x%08x\n",
                DEVICE_ID,
                PKT_SEQ,
                CHANNEL_SEQ,
                CHANNEL_ID,
                FLAGS,
                CHUNK_ID,
                dj->pkt_seq,
                PKT_SEQ);
         dj->count_bad_pkt_seq++;
         dj->count_error++;
         info->fCountErrors[jslot]++;
         bad_pkt_seq = true;
         error = true;
         if (dj->count_bad_pkt_seq < 10) {
            //cm_msg(MERROR, "AddPwbBank", "%s: UDP packet sequence jump: PKT_SEQ 0x%08x -> 0x%08x (%d missing packets), ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x. count %d", evb->fSlotName[jslot].c_str(), dj->pkt_seq, PKT_SEQ, PKT_SEQ-dj->pkt_seq-1, CHANNEL_ID, FLAGS, CHUNK_ID, dj->count_bad_pkt_seq);         }
            fMfe->Msg(MERROR, "AddPwbBank", "%s: UDP packet sequence jump: PKT_SEQ 0x%08x -> 0x%08x (%d missing packets)", fEvb->fSlotName[jslot].c_str(), dj->pkt_seq, PKT_SEQ, PKT_SEQ-dj->pkt_seq-1);
         }
      }

      dj->pkt_seq = PKT_SEQ;

      if (bad_pkt_seq) {
         // packets in front of this packet have been lost,
         // close out pending events in all 4 channels
         // send all data into "unknown" until new event header
         // is received.
         for (int i=0; i<4; i++) {
            int islot = jslot + i;
            PwbData* d = &info->fPwbData[islot];

            if (d->cnt > 0) {
               //cm_msg(MERROR, "AddPwbBank", "%s: lost UDP packet synchronization, closing pending event with %d chunks, timestamp 0x%08x", evb->fSlotName[islot].c_str(), d->cnt, d->ts);

               // send fake event footer to close out the previous event
               Bank32aBuf *b = new Bank32aBuf(nbkname, TID_BYTE, NULL, 0, 1, islot, d->ts);
               b->fError = true;
               XAddBank(b);

               d->chunk_id = 0;
               d->cnt = 0;
               d->ts  = d->ts + fake_ts_clk;
            }
         }
      }

      if (CHANNEL_ID > 3) {
         printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x -- Error: invalid CHANNEL_ID\n",
                DEVICE_ID,
                PKT_SEQ,
                CHANNEL_SEQ,
                CHANNEL_ID,
                FLAGS,
                CHUNK_ID);
         fMfe->Msg(MERROR, "AddPwbBank", "%s: invalid CHANNEL_ID 0x%x", fEvb->fSlotName[jslot].c_str(), CHANNEL_ID);
         dj->count_bad_channel_id++;
         dj->count_error++;
         info->fCountErrors[jslot]++;
         return false;
      }
   
      int islot = jslot + CHANNEL_ID;
   
      info->fLastTimeSec[islot] = TMFE::GetTime();
      info->fCountPackets[islot] += 1;
      info->fCountBytes[islot] += bklen;

      //printf("pwb module %d slot %d/%d\n", imodule, jslot, islot);

      PwbData* d = &info->fPwbData[islot];

      if (0) {
         printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x\n",
                DEVICE_ID,
                PKT_SEQ,
                CHANNEL_SEQ,
                CHANNEL_ID,
                FLAGS,
                CHUNK_ID);
      }
   
      bool trace = false;

      uint32_t ts = 0;

      int waiting_incr = 0;

      if (CHUNK_ID == 0) { // event header
         // error flag may be set if there are lost packets
         // before this event header. clear the error flag,
         // as the error belongs with the previous event.
         error = false;
      
         int FormatRevision  = (p32[5]>> 0) & 0xFF;
         //int ScaId           = (p32[5]>> 8) & 0xFF;
         //int CompressionType = (p32[5]>>16) & 0xFF;
         //int TriggerSource   = (p32[5]>>24) & 0xFF;

         uint32_t TriggerTimestamp1 = 0;
         uint32_t ScaChannelsSent1 = 0;
         uint32_t ScaChannelsSent2 = 0;
         uint32_t ScaChannelsSent3 = 0;
         uint32_t ScaChannelsThreshold1 = 0;
         uint32_t ScaChannelsThreshold2 = 0;
         uint32_t ScaChannelsThreshold3 = 0;
      
         if ((FormatRevision == 0) || (FormatRevision == 1)) {
            //uint32_t HardwareId1 = p32[6];
            //
            //uint32_t HardwareId2 = (p32[7]>> 0) & 0xFFFF;
            //int TriggerDelay     = (p32[7]>>16) & 0xFFFF;
         
            // NB timestamp clock is 125 MHz
         
            TriggerTimestamp1 = p32[8];
         
            //uint32_t TriggerTimestamp2 = (p32[9]>> 0) & 0xFFFF;
            //uint32_t Reserved1         = (p32[9]>>16) & 0xFFFF;
         
            //int ScaLastCell = (p32[10]>> 0) & 0xFFFF;
            //int ScaSamples  = (p32[10]>>16) & 0xFFFF;
         
            ScaChannelsSent1 = p32[11];
            ScaChannelsSent2 = p32[12];
         
            ScaChannelsSent3 = (p32[13]>> 0) & 0xFFFF;
            ScaChannelsThreshold1 = (p32[13]>>16) & 0xFFFF;
         
            ScaChannelsThreshold1 |= ((p32[14] & 0xFFFF) << 16) & 0xFFFF0000;
            ScaChannelsThreshold2 = (p32[14]>>16) & 0xFFFF;
         
            ScaChannelsThreshold2 |= ((p32[15] & 0xFFFF) << 16) & 0xFFFF0000;
            ScaChannelsThreshold3 = (p32[15]>>16) & 0xFFFF;
         } else if (FormatRevision == 2) {
            const uint32_t *w32 = p32+4;
            TriggerTimestamp1 = w32[4];

            ScaChannelsSent1 = w32[7];
            ScaChannelsSent2 = w32[8];
            ScaChannelsSent3 = (w32[9]>> 0) & 0xFFFF;

            ScaChannelsThreshold1 = (w32[9]>>16) & 0xFFFF;
            ScaChannelsThreshold1 |= ((w32[10] & 0xFFFF) << 16) & 0xFFFF0000;
            ScaChannelsThreshold2 = (w32[10]>>16) & 0xFFFF;
            ScaChannelsThreshold2 |= ((w32[11] & 0xFFFF) << 16) & 0xFFFF0000;
            ScaChannelsThreshold3 = (w32[11]>>16) & 0xFFFF;

            uint32_t w13 = w32[13];
            int ScaFifoMaxUsed  = (w13 & 0x0000FFFF);
            int EventFifoWrUsed = (w13 & 0xFF000000) >> 24;
            int EventFifoRdUsed = (w13 & 0x00FF0000) >> 16;

            info->fPwbScaFifoMaxUsed[islot] = ScaFifoMaxUsed;
            if (ScaFifoMaxUsed > info->fPwbScaFifoMaxUsedEver)
               info->fPwbScaFifoMaxUsedEver = ScaFifoMaxUsed;

            info->fPwbEventFifoWrUsed[islot] = EventFifoWrUsed;
            if (EventFifoWrUsed > info->fPwbEventFifoWrMaxUsed[islot])
               info->fPwbEventFifoWrMaxUsed[islot] = EventFifoWrUsed;
            if (EventFifoWrUsed > info->fPwbEventFifoWrMaxUsedAll)
               info->fPwbEventFifoWrMaxUsedAll = EventFifoWrUsed;
            if (EventFifoWrUsed > info->fPwbEventFifoWrMaxUsedEver)
               info->fPwbEventFifoWrMaxUsedEver = EventFifoWrUsed;

            info->fPwbEventFifoRdUsed[islot] = EventFifoRdUsed;
            if (EventFifoRdUsed > info->fPwbEventFifoRdMaxUsed[islot])
               info->fPwbEventFifoRdMaxUsed[islot] = EventFifoRdUsed;
            //if (EventFifoRdUsed > info->fPwbEventFifoRdMaxUsedAll)
            //   info->fPwbEventFifoRdMaxUsedAll = EventFifoRdUsed;

            if (EventFifoWrUsed == 31) { // overflow
               info->fPwbEventFifoOverflows[islot] += 1;
               info->fPwbEventFifoOverflowsAll += 1;
               info->fPwbEventFifoOverflowsEver += 1;
            }

            //printf("module %d, slot %d, word 13: 0x%08x, sca fifo max used: %d, event fifo used: %d, %d, max used: %d, %d\n", imodule, islot, w13, ScaFifoMaxUsed, EventFifoWrUsed, EventFifoRdUsed, info->fPwbEventFifoWrMaxUsed[islot], info->fPwbEventFifoRdMaxUsed[islot]);
         } else {
            printf("Error: invalid format revision %d\n", FormatRevision);
            d->count_bad_format_revision++;
            d->count_error++;
            info->fCountErrors[islot]++;
            cm_msg(MERROR, "AddPwbBank", "%s: invalid FormatRevision 0x%x", fEvb->fSlotName[islot].c_str(), FormatRevision);
            return false;
         }

         ts = TriggerTimestamp1;
      
         if (d->chunk_id != 0) {
            printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x -- Error: last chunk_id 0x%04x, lost event footer\n",
                   DEVICE_ID,
                   PKT_SEQ,
                   CHANNEL_SEQ,
                   CHANNEL_ID,
                   FLAGS,
                   CHUNK_ID,
                   d->chunk_id);
            d->count_lost_footer++;
            d->count_error++;
            info->fCountErrors[islot]++;
            cm_msg(MERROR, "AddPwbBank", "%s: missing event footer: CHUNK_ID 0x%04x -> 0x%04x, PKT_SEQ 0x%08x, FLAGS 0x%02x", fEvb->fSlotName[islot].c_str(), d->chunk_id, CHUNK_ID, PKT_SEQ, FLAGS);

            // send fake event footer to close out the previous event
            //BankBuf *b = new BankBuf(nbkname, TID_BYTE, NULL, 0, 1);
            //b->error = true;
            //XAddBank(islot, d->ts, b);
         }

         d->chunk_id = 0;
         d->cnt = 1;
         d->ts  = ts;

         int sent_bits = CountBits(ScaChannelsSent1) + CountBits(ScaChannelsSent2) + CountBits(ScaChannelsSent3);
         int threshold_bits = CountBits(ScaChannelsThreshold1) + CountBits(ScaChannelsThreshold2) + CountBits(ScaChannelsThreshold3);

         d->sent_bits = sent_bits;
         d->threshold_bits = threshold_bits;

         //printf("sent_bits: 0x%08x 0x%08x 0x%08x -> %d bits, threshold bits %d\n", ScaChannelsSent1, ScaChannelsSent2, ScaChannelsSent3, sent_bits, threshold_bits);
      
         if (sent_bits > info->fSentMax[islot])
            info->fSentMax[islot] = sent_bits;
         if ((info->fSentMin[islot] == 0) || (sent_bits < info->fSentMin[islot]))
            info->fSentMin[islot] = sent_bits;
         info->fCountSent0[islot] += 1;
         info->fCountSent1[islot] += sent_bits;

         if (threshold_bits > info->fThrMax[islot])
            info->fThrMax[islot] = threshold_bits;
         if ((info->fThrMin[islot] == 0) || (threshold_bits < info->fThrMin[islot]))
            info->fThrMin[islot] = threshold_bits;
         info->fCountThr0[islot] += 1;
         info->fCountThr1[islot] += threshold_bits;


         if (trace) {
            printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x, TS 0x%08x\n",
                   DEVICE_ID,
                   PKT_SEQ,
                   CHANNEL_SEQ,
                   CHANNEL_ID,
                   FLAGS,
                   CHUNK_ID,
                   TriggerTimestamp1
                   );
         }

#if 0      
         if (0) {
            printf("H F 0x%02x, Sca 0x%02x, C 0x%02x, T 0x%02x, H 0x%08x, 0x%04x, Delay 0x%04x, TS 0x%08x, 0x%04x, R1 0x%04x, SCA LastCell 0x%04x, Samples 0x%04x, Sent 0x%08x 0x%08x 0x%08x, Thr 0x%08x 0x%08x 0x%08x, R2 0x%04x\n",
                   FormatRevision,
                   ScaId,
                   CompressionType,
                   TriggerSource,
                   HardwareId1, HardwareId2,
                   TriggerDelay,
                   TriggerTimestamp1, TriggerTimestamp2,
                   Reserved1,
                   ScaLastCell,
                   ScaSamples,
                   ScaChannelsSent1,
                   ScaChannelsSent2,
                   ScaChannelsSent3,
                   ScaChannelsThreshold1,
                   ScaChannelsThreshold2,
                   ScaChannelsThreshold3,
                   Reserved2);
         }
#endif
      } else if (d->cnt == 0) { // no event header
         d->count_no_header++;
         d->count_error++;
         info->fCountErrors[islot]++;
         error = true;

         //cm_msg(MERROR, "AddPwbBank", "%s: data packet without event header: PKT_SEQ 0x%08x, ID 0x%02x, FLAGS 0x%02x, CHUNK_ID 0x%04x, fake timestamp 0x%08x", fEvb->fSlotName[islot].c_str(), PKT_SEQ, CHANNEL_ID, FLAGS, CHUNK_ID, d->ts);

         Bank32aBuf *b = new Bank32aBuf(nbkname, TID_BYTE, pbank, bklen, 999999, islot, d->ts);
         b->fError = error;

         XAddBank(b);

         return true;
      } else {
         ts = d->ts;
         d->cnt++;
         if (CHUNK_ID <= d->chunk_id) {
            printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x -- Error: last chunk_id 0x%04x, lost event header\n",
                   DEVICE_ID,
                   PKT_SEQ,
                   CHANNEL_SEQ,
                   CHANNEL_ID,
                   FLAGS,
                   CHUNK_ID,
                   d->chunk_id);
            d->count_lost_header++;
            d->count_error++;
            info->fCountErrors[islot]++;
            error = true;
            cm_msg(MERROR, "AddPwbBank", "%s: lost event header: CHUNK_ID 0x%04x -> 0x%04x", fEvb->fSlotName[islot].c_str(), d->chunk_id, CHUNK_ID);
         } else if (CHUNK_ID != d->chunk_id+1) {
            int missing = CHUNK_ID - d->chunk_id - 1;
            printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x -- Error: CHUNK_ID jump 0x%04x to 0x%04x, %d missing\n",
                   DEVICE_ID,
                   PKT_SEQ,
                   CHANNEL_SEQ,
                   CHANNEL_ID,
                   FLAGS,
                   CHUNK_ID,
                   d->chunk_id,
                   CHUNK_ID,
                   missing);
            d->count_bad_chunk_id++;
            d->count_error++;
            info->fCountErrors[islot]++;
            error = true;
            cm_msg(MERROR, "AddPwbBank", "%s: CHUNK_ID jump 0x%04x -> 0x%04x, %d missing", fEvb->fSlotName[islot].c_str(), d->chunk_id, CHUNK_ID, missing);
         }

         d->chunk_id = CHUNK_ID;

         if (0) {
            printf("slot %2d, bank %s, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x, TS 0x%08x\n",
                   islot,
                   bkname,
                   PKT_SEQ,
                   CHANNEL_SEQ,
                   CHANNEL_ID,
                   FLAGS,
                   CHUNK_ID,
                   ts
                   );
         }
      }

      if (FLAGS & 1) { // event footer
         if (trace) {
            printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x, TS 0x%08x, LAST of %d packets\n",
                   DEVICE_ID,
                   PKT_SEQ,
                   CHANNEL_SEQ,
                   CHANNEL_ID,
                   FLAGS,
                   CHUNK_ID,
                   d->ts,
                   d->cnt);
         }
         d->cnt = 0;
         d->ts = d->ts + fake_ts_clk;
         d->chunk_id = 0;
         waiting_incr = 1;
      } else {
         if (0 && trace) {
            printf("ID 0x%08x, PKT_SEQ 0x%08x, CHAN SEQ 0x%04x, ID 0x%02x, FLAGS 0x%02x, CHUNK ID 0x%04x, TS 0x%08x, count %d\n",
                   DEVICE_ID,
                   PKT_SEQ,
                   CHANNEL_SEQ,
                   CHANNEL_ID,
                   FLAGS,
                   CHUNK_ID,
                   d->ts,
                   d->cnt);
         }
      }
      
      //if (ts == 0) { // no event header for these packets
      //   return false;
      //}

      Bank32aBuf *b = new Bank32aBuf(nbkname, TID_BYTE, pbank, bklen, waiting_incr, islot, ts);
      b->fError = error;

      XAddBank(b);
   
      return true;
   }

   bool AddTdcBank(const char* bkname, const char* pbank, int bklen, int bktype, PerSlotInfo* info)
   {
      //printf("AddTdcBank: name [%s] len %d type %d, tid_size %d\n", bkname, bklen, bktype, rpc_tid_size(bktype));
      
      if (fEvb->fKludgeTdcKillFirstEvent) {
         fEvb->fKludgeTdcKillFirstEvent = false;
         fMfe->Msg(MINFO, "AddTdcBank", "Kludge: killing first TDC event");
         return true;
      }
      
      if (0) {
         const uint32_t* p32 = (const uint32_t*)pbank;
         
         for (int i=0; i<20; i++) {
            printf("tdc[%d]: 0x%08x 0x%08x\n", i, p32[i], getUint32(pbank, i*4));
         }
      }
      
      uint32_t fpga0_mark = getUint32(pbank, 6*4);
      uint32_t fpga0_header = getUint32(pbank, 7*4);
      uint32_t fpga0_epoch  = getUint32(pbank, 8*4);
      uint32_t fpga0_hit    = getUint32(pbank, 9*4);
      
      uint32_t ts = fpga0_epoch & 0x0FFFFFFF;
      
      if (0) {
         printf("fpga0: mark 0x%08x, h 0x%08x, e 0x%08x, h 0x%08x, ts 0x%08x\n",
                fpga0_mark,
                fpga0_header,
                fpga0_epoch,
                fpga0_hit,
                ts);
      }
      
      int imodule = 0;
      
      int islot = get_vector_element(fEvb->fTdcSlot, imodule);
      
      if (islot < 0) {
         return false;
      }
      
      info->fLastTimeSec[islot] = TMFE::GetTime();
      info->fCountPackets[islot] += 1;
      info->fCountBytes[islot] += bklen;
      info->fCountSent0[islot] += 1;
      info->fCountSent1[islot] += bklen/4;
      
      Bank32aBuf *b = new Bank32aBuf(bkname, TID_DWORD, pbank, bklen, 1, islot, ts);
      
      XAddBank(b);
      
      return true;
   }

public:
   void HandleEvent(EVENT_HEADER *pheader, void *pevent, PerSlotInfo* info)
   {
      if (fEvb->fEvbState == EVB_STATE_WAITING_FOR_SYNC) {
         if (!fEvb->fSync.fSyncActive) {
            std::lock_guard<std::mutex> lock(fEvb->fLock);
            if (fEvb->fEvbState == EVB_STATE_WAITING_FOR_SYNC) {
               if (!fEvb->fSync.fSyncActive) {
                  fEvb->CheckSyncLocked();
               }
            }
         }
      }

      if (verbose) {
         //printf("event_handler: Evid: 0x%x, Mask: 0x%x, Serial: %d, Size: %d, Banks: %d (%s)\n", pheader->event_id, pheader->trigger_mask, pheader->serial_number, pheader->data_size, nbanks, banklist);
         printf("event_handler: Evid: 0x%x, Mask: 0x%x, Serial: %d, Size: %d\n", pheader->event_id, pheader->trigger_mask, pheader->serial_number, pheader->data_size);
      }
      
      Bank32aBuf* no_evb_banks = NULL;
      
      BANK32A* bhptr = NULL;
      int is32a = bk_is32a(pevent);
      for (int ibank=0; ; ibank++) {
         char *pdata;
         if (is32a)
            bk_iterate32a(pevent, &bhptr, &pdata);
         else
            bk_iterate32(pevent, (BANK32**)&bhptr, &pdata);
         if (bhptr == NULL) {
            break;
         }
         
         char name[5];
         name[0] = bhptr->name[0];
         name[1] = bhptr->name[1];
         name[2] = bhptr->name[2];
         name[3] = bhptr->name[3];
         name[4] = 0;
         
         //printf("bk_iterate32 bhptr %p, pdata %p, name %s [%s], type %d, size %d\n", bhptr, pdata, bhptr->name, name, bhptr->type, bhptr->data_size);
         
         const char* pbank = pdata;
         int bktype = bhptr->type;
         int bklen = bhptr->data_size;
         
         if (bklen <= 0 || bklen > 1000000) {
            fMfe->Msg(MERROR, "Handler::HandleEvent", "Bank [%s] type %d invalid length %d", name, bktype, bklen);
            return;
         }
         
         bool handled = false;
         
         if (name[0]=='A' && name[1]=='A') {
            int imodule = (name[2]-'0')*10 + (name[3]-'0')*1;
            handled = AddAdcBank(imodule, pbank, bklen, info);
         } else if (name[0]=='P' && name[1]=='B') {
            int imodule = (name[2]-'0')*10 + (name[3]-'0')*1;
            handled = AddPwbBank(imodule, name, (const char*)pbank, bklen, bktype, info);
         } else if (name[0]=='A' && name[1]=='T') {
            handled = AddTrgBank(name, (const char*)pbank, bklen, bktype, info);
         } else if (name[0]=='T' && name[1]=='R' && name[2]=='B' && name[3]=='A') {
            handled = AddTdcBank(name, (const char*)pbank, bklen, bktype, info);
         }
         
         if (!handled) {
            Bank32aBuf *bank = new Bank32aBuf(name, bktype, (char*)pbank, bklen, 1, 0, 0);
            if (no_evb_banks) {
               no_evb_banks->AddChain(bank);
            } else {
               no_evb_banks = bank;
            }
         }

         if (fBankBuf.size() > fFlushThreshold) {
            XFlushBank();
         }
      }
      
      if (0) {
         fEvb->fSync.Dump();
         fEvb->fSync.Print();
         printf("\n");
      }
      
      if (0) {
         static bool ok = false;
         static bool failed = false;
         
         if (ok != fEvb->fSync.fSyncOk) {
            if (fEvb->fSync.fSyncOk) {
               int count_dead = 0;
               std::string deaders = "";
               for (unsigned i=0; i<fEvb->fSync.fModules.size(); i++) {
                  if (fEvb->fSync.fModules[i].fDead) {
                     if (count_dead > 0)
                        deaders += ", ";
                     deaders += fEvb->fSlotName[i];
                     count_dead++;
                  }
               }
               if (count_dead > 0) {
                  cm_msg(MERROR, "event_handler", "Event builder timestamp sync successful. %d dead modules: %s", count_dead, deaders.c_str());
               } else {
                  cm_msg(MINFO, "event_handler", "Event builder timestamp sync successful");
               }
            }
            ok = fEvb->fSync.fSyncOk;
         }
         
         if (failed != fEvb->fSync.fSyncFailed) {
            if (fEvb->fSync.fSyncFailed) {
               cm_msg(MERROR, "event_handler", "Event builder timestamp sync FAILED");
            }
            failed = fEvb->fSync.fSyncFailed;
         }
      }
      
      if (no_evb_banks) {

         if (fEvb->fCountUnknown == 0) {
            fMfe->Msg(MERROR, "read_event", "Event builder received first unknown event");
         }
         
         fEvb->fCountUnknown++;
         
         fEvb->fSendQueue->PushEvent(no_evb_banks);
         no_evb_banks = NULL;
      }
   }

   EVENT_HEADER* ReadEvent()
   {
      if (fBh < 0)
         return NULL;

      //if (fEvb->fEvbState == EVB_STATE_UNCONFIGURED) {
      //   fMfe->Msg(MINFO, "Reader::ReadEvent", "Event builder is not ready yet!");
      //   return NULL;
      //}

      EVENT_HEADER* pheader = NULL;

      int status = bm_receive_event_alloc(fBh, &pheader, BM_NO_WAIT);
      //printf("bh %d, size %d, status %d\n", fBh, pheader->data_size, status);
      if (status == BM_ASYNC_RETURN) {
         //if (fKludgeFlush) {
         //   fMfe->Msg(MINFO, "BufferReader::ReadEvent", "Flush buffer \"%s\" done, %d events flushed!", fBufName.c_str(), fKludgeFlushCount);
         //   fKludgeFlush = false;
         //}
         return NULL;
      }
      if (status != BM_SUCCESS) {
         fMfe->Msg(MERROR, "BufferReader::ReadEvent", "bm_receive_event() returned %d", status);
         fBh = -1;
         return NULL;
      }

      //if (fKludgeFlush) {
      //   fKludgeFlushCount++;
      //   fMfe->Msg(MERROR, "BufferReader::ReadEvent", "Flushing buffer \"%s\", %d events discarded!", fBufName.c_str(), fKludgeFlushCount);
      //   delete pheader;
      //   
      //   return NULL;
      //}

      if (!fEvb || (fEvb && !fEvb->fKludgeReady)) {
         fMfe->Msg(MERROR, "BufferReader::ReadEvent", "Discarding event from \"%s\", event builder is not ready yet!", fBufName.c_str());
         delete pheader;
         return NULL;
      }

      if (fEventsIn == 0) {
         fMfe->Msg(MINFO, "BufferReader::ReadEvent", "Event builder received the first event from \"%s\"", fBufName.c_str());
      }

      //if (strstr(fBufName.c_str(), "BUFTDC")) {
      //   printf("T");
      //}

      fEventsIn++;

      return pheader;
   }

public:
   bool TickLocked()
   {
      EVENT_HEADER* pevent = ReadEvent();
      if (pevent == NULL)
         return false;

      if (fEvb->fEvbState == EVB_STATE_UNCONFIGURED) {
         std::lock_guard<std::mutex> lock(fEvb->fLock);
         if (fEvb->fEvbState == EVB_STATE_UNCONFIGURED) {
            fEvb->InitEvbLocked();
            fInfo->Init(fEvb->fNumSlots, fEvb->fSlotType);
            assert(fEvb->fEvbState == EVB_STATE_WAITING_FOR_SYNC);
            fEvb->fSendQueue->fMaxQueueSize = fEvb->fConfMaxSendQueue;
            fEvb->fSendQueue->fDropQueue = fEvb->fConfDropSendQueue;
         }
         // implicit unlock
      }

      HandleEvent(pevent, pevent+1, fInfo);

      free(pevent);
      pevent = NULL;

      return true;
   }

public:
   void BufferReaderThread()
   {
      printf("BufferReaderThread: Thread for event buffer \"%s\" started\n", fBufName.c_str());
      while (fRunThread) {
         bool done_something = false;
         {
            std::lock_guard<std::mutex> lock(fLock);
            for (int i=0; i<1000; i++) {
               done_something = TickLocked();
               //if (strstr(fBufName.c_str(), "BUFTDC")) {
               //   printf("(%d,%d)", i, done_something);
               //}
               if (!done_something)
                  break;
            }
            XMaybeFlushBank(fBufName.c_str());
            // implicit unlock
         }
         if (done_something) {
            //::usleep(100);
         } else {
            ::usleep(10000);
         }
      }
      printf("BufferReaderThread: Thread for event buffer \"%s\" stopped\n", fBufName.c_str());
   }

   void StartThread()
   {
      assert(fThread==NULL);
      fRunThread = true;
      fThread = new std::thread(&BufferReader::BufferReaderThread, this);
   }

   void StopThread()
   {
      assert(fThread);
      fRunThread = false;
      fThread->join();
      delete fThread;
      fThread = NULL;
   }

public:
   bool BeginRun()
   {
      std::lock_guard<std::mutex> lock(fLock);
      bool ok = true;
      fEventsIn = 0;
      fMaxSize = 0;
      fMaxSizeEver = 0;
      ok &= OpenBuffer();
      return true;
      // implicit unlock
   }

public:
   bool EndRun()
   {
      std::lock_guard<std::mutex> lock(fLock);

      int count = 0;
      while (1) {
         bool done_something = TickLocked();
         if (!done_something)
            break;
         count++;
      }
      if (count) {
         fMfe->Msg(MINFO, "Reader::EndRunLocked", "At end of run read %d additional events from \"%s\"", count, fBufName.c_str());
      }

      CloseBuffer();

      XFlushBank();

      return true;
      // implicit unlock
   }
};

class EqEvb :
   public TMFeEquipment
{
public: // methods
   EqEvb(const char* eqname, const char* eqfilename); // ctor
   virtual ~EqEvb(); // dtor
   void ReportEvb();
   void Report();

public: // handlers for MIDAS callbacks
   TMFeResult HandleInit(const std::vector<std::string>& args);
   TMFeResult HandleBeginRun(int run_number);
   TMFeResult HandleEndRun(int run_number);
   void HandlePeriodic();

public:
   BufferReader* fReaderTRG = NULL;
   BufferReader* fReaderTDC = NULL;
   BufferReader* fReaderADC = NULL;
   BufferReader* fReaderUDP = NULL;
   BufferReader* fReaderPWBA = NULL;
   BufferReader* fReaderPWBB = NULL;
   BufferReader* fReaderPWBC = NULL;
   BufferReader* fReaderPWBD = NULL;
   Evb* fEvb = NULL;
   PerSlotInfo* fInfo = NULL;
   SendQueue* fSendQueue = NULL;

public:
   bool fDoCheckCompleteEvents = false;
   int fPrevCountComplete = 0;
   double fPrevCountCompleteTime = 0;

public: // ODB
   MVOdb* fOdbConfig = NULL;
   MVOdb* fOdbStatus = NULL;
};

EqEvb::EqEvb(const char* eqname, const char* eqfilename) // ctor
   : TMFeEquipment(eqname, eqfilename)
{
   printf("EqEvb::ctor!\n");
   
   // configure the equipment here:
   
   fEqConfEventID = 1;
   fEqConfLogHistory = 1;
   fEqConfPeriodMilliSec = 1000;
   fEqConfBuffer = "SYSTEM";
   fEqConfWriteCacheSize = 0;

   fEqConfReadConfigFromOdb = false;
   //fEqConfWriteEventsToOdb = true;
   fEqConfReadOnlyWhenRunning = false;
}

TMFeResult EqEvb::HandleInit(const std::vector<std::string>& args)
{
   printf("EqEvb::HandleInit!\n");

   fReaderTRG = new BufferReader(fMfe, "BUFTRG");
   fReaderTDC = new BufferReader(fMfe, "BUFTDC");
   fReaderADC = new BufferReader(fMfe, "BUFADC");
   fReaderUDP = new BufferReader(fMfe, "BUFUDP");
   fReaderPWBA = new BufferReader(fMfe, "BUFPWBA");
   fReaderPWBB = new BufferReader(fMfe, "BUFPWBB");
   fReaderPWBC = new BufferReader(fMfe, "BUFPWBC");
   fReaderPWBD = new BufferReader(fMfe, "BUFPWBD");

   fReaderTRG->StartThread();
   fReaderTDC->StartThread();
   fReaderADC->StartThread();
   fReaderUDP->StartThread();
   fReaderPWBA->StartThread();
   fReaderPWBB->StartThread();
   fReaderPWBC->StartThread();
   fReaderPWBD->StartThread();

   fSendQueue = new SendQueue(fMfe, this);

   fSendQueue->StartThread();

   fOdbConfig = fMfe->fOdbRoot->Chdir("Equipment/Ctrl/EvbConfig", false);
   fOdbStatus = fOdbEq->Chdir("EvbStatus", true);

   fMfe->ResetAlarm("EVB");

   EqSetStatus("Started!", "#00FF00");

   return TMFeOk();
}

EqEvb::~EqEvb()
{
   if (fReaderTRG) {
      delete fReaderTRG;
      fReaderTRG = NULL;
   }

   if (fReaderTDC) {
      delete fReaderTDC;
      fReaderTDC = NULL;
   }

   if (fReaderADC) {
      delete fReaderADC;
      fReaderADC = NULL;
   }

   if (fReaderUDP) {
      delete fReaderUDP;
      fReaderUDP = NULL;
   }

   if (fReaderPWBA) {
      delete fReaderPWBA;
      fReaderPWBA = NULL;
   }

   if (fReaderPWBB) {
      delete fReaderPWBB;
      fReaderPWBB = NULL;
   }

   if (fReaderPWBC) {
      delete fReaderPWBC;
      fReaderPWBC = NULL;
   }

   if (fReaderPWBD) {
      delete fReaderPWBD;
      fReaderPWBD = NULL;
   }

   if (fEvb) {
      fEvb->fLock.lock();
      delete fEvb;
      fEvb = NULL;
   }

   if (fInfo) {
      delete fInfo;
      fInfo = NULL;
   }

   if (fSendQueue) {
      delete fSendQueue;
      fSendQueue = NULL;
   }
}

void EqEvb::ReportEvb()
{
   assert(fEvb);

   fInfo->SumSlotErrors();
   fInfo->ComputePerSecond();

   fEvb->fLock.lock();

   int count_dead_slots = 0;
   int count_dead_tdc = 0;
   for (unsigned i=0; i<fEvb->fNumSlots; i++) {
      if (fEvb->fSync.fModules[i].fDead) {
         fEvb->fDeadSlots[i] = true;
         count_dead_slots++;
         if (fInfo->fTdcSlot[i]) {
            count_dead_tdc ++;
         }
      } else {
         fEvb->fDeadSlots[i] = false;
      }
   }
   if (count_dead_slots != fEvb->fCountDeadSlots) {
      fMfe->Msg(MERROR, "ReportEvb", "Number of dead slots changed from %d to %d", fEvb->fCountDeadSlots, count_dead_slots);
      fEvb->fCountDeadSlots = count_dead_slots;

      if (count_dead_tdc > 0) {
         fMfe->TriggerAlarm("EVB", "TDC is dead", "Alarm");
      } else if (count_dead_slots > 0) {
         fMfe->TriggerAlarm("EVB", "Something died in the evb", "Alarm");
      }
   }

   bool yellow = false;

   std::string st = "";

   if (fEvb->fEvbState != EVB_STATE_RUNNING) {
      st += msprintf("state %d", fEvb->fEvbState);
   } else {
      st += "Running";
   }

   if (fEvb->fCountDeadSlots) {
      yellow = true;
      st += msprintf(", dead %d", fEvb->fCountDeadSlots);
   }
   if (fEvb->fCountEvbErrorsTooOld) {
      yellow = true;
      st += msprintf(", dropped too old %d", fEvb->fCountEvbErrorsTooOld);
   }
   if (fEvb->fCountEvbErrorsFindEvent) {
      yellow = true;
      st += msprintf(", dropped by FindEvent() %d", fEvb->fCountEvbErrorsFindEvent);
   }
   if (fEvb->fSendQueue->fCountQueueFull) {
      yellow = true;
      st += msprintf(", send queue full %d", (int)fEvb->fSendQueue->fCountQueueFull);
      if (fEvb->fSendQueue->fCountDropFull) {
         st += msprintf(", dropped %d", (int)fEvb->fSendQueue->fCountDropFull);
      }
   }
   if (fEvb->fCountUnknown) {
      yellow = true;
      st += msprintf(", unknown %d", fEvb->fCountUnknown);
   }
   st += msprintf(", built %d (complete %d", fEvb->fCountOut, fEvb->fCountComplete);

   if (fEvb->fCountIncomplete) {
      yellow = true;
      st += msprintf(", incomplete %d", fEvb->fCountIncomplete);
   }
   if (fEvb->fCountError) {
      yellow = true;
      st += msprintf(", with errors %d", fEvb->fCountError);
   }

   st += ")";
   if (fInfo->fSumSlotErrors) {
      yellow = true;
      st += msprintf(", per-slot errors %d", fInfo->fSumSlotErrors);
   }
   
   st += msprintf(", per-event adcs %.1f, pads %.1f, tdcs %.1f",
                  fInfo->fAdcSentAvePerEvent,
                  fInfo->fPwbSentAvePerEvent,
                  fInfo->fTdcSentAvePerEvent);

   std::string st_col = "#00FF00";

   if (yellow) {
      st_col = "yellow";
   }

   if (fEvb->fPrintStatus) {
      fEvb->PrintEvbLocked();
   }

   fEvb->fLock.unlock();
   
   EqSetStatus(st.c_str(), st_col.c_str());

   fEvb->WriteSyncStatus();
   fEvb->WriteEvbStatus();
   fEvb->WriteVariables();

   if (fEvb->fEvbState != EVB_STATE_UNCONFIGURED) {
      fInfo->WriteStatus(fOdbStatus);
      fInfo->WriteVariables(fOdbEqVariables);
   }

   if (fDoCheckCompleteEvents) {
      if (fEvb->fCountComplete != fPrevCountComplete) {
         // counter of complete events is increasing
         fPrevCountComplete = fEvb->fCountComplete;
         fPrevCountCompleteTime = TMFE::GetTime();
      } else {
         // counter of complete events stopped
         if (fPrevCountCompleteTime > 0) {
            double now = TMFE::GetTime();
            if (now - fPrevCountCompleteTime > 15) {
               fMfe->TriggerAlarm("EVB", "No complete events for 15 seconds", "Alarm");
               fPrevCountCompleteTime = 0;
            }
         }
      }
   }
}

void EqEvb::Report()
{
#if 0
   if (0) {
      printf("Reader buffers: size/max size/max size ever\n");
      printf("TRG: %d/%d/%d\n", fReaderTRG->fHandler->fSize, fReaderTRG->fHandler->fMaxSize, fReaderTRG->fHandler->fMaxSizeEver);
      printf("TDC: %d/%d/%d\n", fReaderTDC->fHandler->fSize, fReaderTDC->fHandler->fMaxSize, fReaderTDC->fHandler->fMaxSizeEver);
      printf("ADC: %d/%d/%d\n", fReaderADC->fHandler->fSize, fReaderADC->fHandler->fMaxSize, fReaderADC->fHandler->fMaxSizeEver);
      printf("UDP: %d/%d/%d\n", fReaderUDP->fHandler->fSize, fReaderUDP->fHandler->fMaxSize, fReaderUDP->fHandler->fMaxSizeEver);
   }
#endif

   if (fEvb)
      ReportEvb();

   fReaderTRG->WriteStatus(fOdbStatus);
   fReaderTDC->WriteStatus(fOdbStatus);
   fReaderADC->WriteStatus(fOdbStatus);
   fReaderUDP->WriteStatus(fOdbStatus);
   fReaderPWBA->WriteStatus(fOdbStatus);
   fReaderPWBB->WriteStatus(fOdbStatus);
   fReaderPWBC->WriteStatus(fOdbStatus);
   fReaderPWBD->WriteStatus(fOdbStatus);

   fSendQueue->WriteStatus(fOdbStatus);
   fSendQueue->WriteVariables(fOdbEqVariables);   
}

void EqEvb::HandlePeriodic()
{
   //printf("EqEvb::HandlePeriodic!\n");

   if (fEvb) {
      if (fEvb->fEvbState == EVB_STATE_RUNNING) {
         fInfo->CheckSkew(fMfe, fEvb->fConfMaxDeadSec, fEvb->fSlotName, &fEvb->fSync);
      }
   }

   Report();

   EqWriteStatistics();
}

TMFeResult EqEvb::HandleBeginRun(int run_number)
{
   printf("EqEvb::HandleBeginRun!\n");

   EqSetStatus("Begin run...", "#00FF00");

   fMfe->ResetAlarm("EVB");

   fPrevCountComplete = 0;
   fPrevCountCompleteTime = 0;
   fDoCheckCompleteEvents = true;

   bool ok = true;

   if (fEvb) {
      fReaderTRG->SetEvb(NULL, NULL);
      fReaderTDC->SetEvb(NULL, NULL);
      fReaderADC->SetEvb(NULL, NULL);
      fReaderUDP->SetEvb(NULL, NULL);
      fReaderPWBA->SetEvb(NULL, NULL);
      fReaderPWBB->SetEvb(NULL, NULL);
      fReaderPWBC->SetEvb(NULL, NULL);
      fReaderPWBD->SetEvb(NULL, NULL);
      fEvb->fLock.lock();
      delete fEvb;
      fEvb = NULL;
      delete fInfo;
      fInfo = NULL;
   }

   if (fSendQueue) {
      fSendQueue->fCountDropFull  = 0;
      fSendQueue->fCountQueueFull = 0;
      fSendQueue->fMaxSize        = 0;
      fSendQueue->fMaxSizeEver    = 0;
   }

   fEvb = new Evb(fMfe, fSendQueue, fOdbEqSettings, fOdbConfig, fOdbStatus, fOdbEqVariables);
   fInfo = new PerSlotInfo();

   fEvb->StartThread();

   Report();

   EqZeroStatistics();
   EqWriteStatistics();

   fReaderTRG->SetEvb(fEvb, fInfo);
   fReaderTDC->SetEvb(fEvb, fInfo);
   fReaderADC->SetEvb(fEvb, fInfo);
   fReaderUDP->SetEvb(fEvb, fInfo);
   fReaderPWBA->SetEvb(fEvb, fInfo);
   fReaderPWBB->SetEvb(fEvb, fInfo);
   fReaderPWBC->SetEvb(fEvb, fInfo);
   fReaderPWBD->SetEvb(fEvb, fInfo);

   ok &= fReaderTRG->BeginRun();
   ok &= fReaderTDC->BeginRun();
   ok &= fReaderADC->BeginRun();
   ok &= fReaderUDP->BeginRun();
   ok &= fReaderPWBA->BeginRun();
   ok &= fReaderPWBB->BeginRun();
   ok &= fReaderPWBC->BeginRun();
   ok &= fReaderPWBD->BeginRun();

   fEvb->fKludgeReady = true;

   return TMFeOk();
}

TMFeResult EqEvb::HandleEndRun(int run_number)
{
   printf("EqEvb::HandleEndRun!\n");

   fMfe->Msg(MINFO, "HandleEndRun", "HandleEndRun entered!");

   fDoCheckCompleteEvents = false;

   bool ok = true;

   ok &= fReaderTRG->EndRun();
   ok &= fReaderTDC->EndRun();
   ok &= fReaderADC->EndRun();
   ok &= fReaderUDP->EndRun();
   ok &= fReaderPWBA->EndRun();
   ok &= fReaderPWBB->EndRun();
   ok &= fReaderPWBC->EndRun();
   ok &= fReaderPWBD->EndRun();

   if (fEvb) {
      std::lock_guard<std::mutex> lock(fEvb->fLock);

      fEvb->BuildLocked(true);

      int count = 0;
      while (1) {
         bool done_something = fEvb->EvbTickLocked(true);
         if (!done_something)
            break;
         count++;
      }

      if (count) {
         fMfe->Msg(MLOG, "HandleEndRun", "HandleEndRun: %d loops of Evb->TickLocked(true)", count);
      }

      count = 0;
      while (1) {
         int num = fSendQueue->GetQueueSize();
         if (num == 0)
            break;
         count++;
         ::usleep(100000);
      }

      if (count) {
         fMfe->Msg(MLOG, "HandleEndRun", "HandleEndRun: %d loops of waiting for the send queue to drain", count);
      }

      if (fEvb->fPrintStatus) {
         printf("HandleEndRun: Evb state before building the last events:\n");
         fEvb->PrintEvbLocked();
      }

      int count_lost = 0;

      while (1) {
         EvbEvent *e = fEvb->GetLastEventLocked();
         if (!e)
            break;

         count_lost += 1;
         
         delete e;
      }

      if (count_lost) {
         fMfe->Msg(MERROR, "HandleEndRun", "HandleEndRun: %d events lost at end of run", count_lost);
      }

      if (fEvb->fPrintStatus) {
         printf("HandleEndRun: Evb state after building the last events:\n");
         fEvb->PrintEvbLocked();
      }

      fInfo->LogPwbCounters(fMfe, fEvb->fSlotName);
      
      cm_msg(MLOG, "end_of_run", "end_of_run: complete %d, incomplete %d, with errors %d, unknown %d, out %d, lost at end of run %d", fEvb->fCountComplete, fEvb->fCountIncomplete, fEvb->fCountError, fEvb->fCountUnknown, fEvb->fCountOut, count_lost);
   }

   Report();

   EqWriteStatistics();

   fMfe->Msg(MINFO, "HandleEndRun", "HandleEndRun done, run stopped");

   return TMFeOk();
}

#if 0
bool EqEvb::Build(bool build_last)
{
   bool done_something = false;
   if (fEvb) {
      //done_something |= fReaderTRG->TickLocked(true);
      //done_something |= fReaderTDC->TickLocked(true);
      //done_something |= fReaderUDP->TickLocked(true);
      //done_something |= fEvb->TickLocked(build_last);
      //done_something |= fEvb->fSendQueue->Tick();
   }
   return done_something;
}
#endif

// example frontend

class FeEvb: public TMFrontend
{
public:
   FeEvb() // ctor
   {
      printf("FeEvb::ctor!\n");
      FeSetName("feevb");
      FeAddEquipment(new EqEvb("EVB", __FILE__));
   }

   void HandleUsage()
   {
      printf("FeEvb::HandleUsage!\n");
   };
   
   TMFeResult HandleArguments(const std::vector<std::string>& args)
   {
      printf("FeEvb::HandleArguments!\n");
      return TMFeOk();
   };
   
   TMFeResult HandleFrontendInit(const std::vector<std::string>& args)
   {
      printf("FeEvb::HandleFrontendInit!\n");

      fMfe->SetTransitionSequenceStart(905);
      fMfe->SetTransitionSequenceStop(99);
      fMfe->DeregisterTransitionPause();
      fMfe->DeregisterTransitionResume();

      return TMFeOk();
   };
   
   TMFeResult HandleFrontendReady(const std::vector<std::string>& args)
   {
      printf("FeEvb::HandleFrontendReady!\n");
      FeStartPeriodicThread();
      fMfe->StartRpcThread();
      return TMFeOk();
   };
   
   void HandleFrontendExit()
   {
      printf("FeEvb::HandleFrontendExit!\n");
   };
};

// boilerplate main function

int main(int argc, char* argv[])
{
   FeEvb fe_evb;
   return fe_evb.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
