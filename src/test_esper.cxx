//
// Name: test_esper.cxx
//

//#include "KOtcp.h"
#include "EsperComm.h"

#include <stdio.h>

int main(int argc, char* argv[])
{
  //const char* host = argv[1];
  //const char* service = argv[2];
  const char* hostname = argv[1];
  const char* mid = argv[2];
  const char* vid = argv[3];

  //KOtcpConnection *conn = new KOtcpConnection(host, service);

  Esper::EsperComm *esper = new Esper::EsperComm(hostname);

  esper->s->fHttpKeepOpen = true;

  while (1) {
    std::string last_errmsg;
    std::string v = esper->Read(mid, vid, &last_errmsg);
    
    if (v.empty()) {
      printf("%s: read %s/%s: error %s\n", hostname, mid, vid, last_errmsg.c_str());
    } else {
      printf("%s: read %s/%s: %s\n", hostname, mid, vid, v.c_str());
    }

    sleep(1);
  }
    
  delete esper;

  return 0;
}

// end file
