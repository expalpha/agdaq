//
// TsSync.h
// Timestamp synchronization
// K.Olchanski
//

#ifndef TsSyncH
#define TsSyncH

#include <stdio.h>
#include <stdint.h>
#include <vector>
#include <deque>

struct TsSyncEntry
{
   uint32_t ts;
   int epoch;
   double time;

   TsSyncEntry(uint32_t xts, int xepoch, double xtime); // ctor
};

class TsSyncModule
{
 public: // identity
   int fModule = -1;

 public: // configuration
   double   fEpochTs = 0; // epoch size, in timestamp units (1<<32, 1<<24, etc)
   double   fFreqHz  = 0; // timestamp clock frequency, Hz
   
 public: // running data
   int      fEpoch   = 0;
   uint32_t fFirstTs = 0;
   uint32_t fPrevTs  = 0;
   uint32_t fLastTs  = 0;
   double   fOffsetSec   = 0;
   double   fPrevTimeSec = 0;
   double   fLastTimeSec = 0;

 public: // final status
   int      fSyncedWith = -1;    // =(-1) not synchronized, >= 0 synchronized with module
   bool     fDead       = false; // module has no events, declared dead

 public:
   TsSyncModule(); // ctor
   bool IsDupe(uint32_t ts) const;
   int NextEpoch(uint32_t ts);
   double GetTime(uint32_t ts, int epoch) const;
};

class TsSync
{
public: // configureation
   double   fConfEpsSec = 0.000100; // time comparison threshold
   double   fConfRelEps = 0; // relative time comparison threshold, sec/sec
   uint32_t fConfSyncMinEvents = 5; // minimum number of events to consider sync attempt
   uint32_t fConfSyncMaxEvents = 100; // maximum number of sync events

public: // per module data
   std::vector<TsSyncModule> fModules;
   std::vector<std::vector<TsSyncEntry>> fBuf;
   std::vector<double> fMaxDtSec;
   std::vector<double> fMaxRelDt;

public: // status
   bool fSyncActive = true;
   bool fSyncOk = false;
   bool fSyncFailed = false;
   bool fTrace = false;

   unsigned fCountNoData = 0;
   unsigned fCountNoSync = 0;

public:
   TsSync(); // ctor
   ~TsSync(); // dtor
   void Configure(unsigned imodule, double epoch_ts, double freq_hz);
   void Sync(unsigned imodule, uint32_t ts, int epoch);
   void PostSync();
   void Dump() const;
   void Print() const;
   void Print(unsigned imodule) const;
   void DumpBuf(unsigned imodule) const;

public: // internal functions
   bool TrySync(unsigned ii, unsigned i);
   bool TrySync(unsigned inew);
   double GetDt(unsigned imodule, unsigned j);
   unsigned FindDt(unsigned imodule, double dt);
   void Retime(unsigned imodule);
   void Count();
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
