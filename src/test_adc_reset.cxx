//
// Name: test_esper.cxx
//

//#include "KOtcp.h"
#include "EsperComm.h"

#include <stdio.h>

int main(int argc, char* argv[])
{
  //const char* host = argv[1];
  //const char* service = argv[2];
  const char* hostname = argv[1];
  //const char* mid = argv[2];
  //const char* vid = argv[3];

  //KOtcpConnection *conn = new KOtcpConnection(host, service);

  Esper::EsperComm *esper = new Esper::EsperComm(hostname);

  esper->s->fHttpKeepOpen = true;

  while (1) {
    std::string v;
    uint32_t vv;

    // reset ADC

    esper->Write("adc16", "adc_reset", "true");

    v = esper->Read("ag", "stat_c");
    vv = strtoul(v.c_str()+1, NULL, 0);
    printf("stat_c: 0x%08x (adc in reset)\n", vv);

    esper->Write("adc16", "adc_reset", "false");

    v = esper->Read("ag", "stat_c");
    vv = strtoul(v.c_str()+1, NULL, 0);
    printf("stat_c: 0x%08x\n", vv);
    
    // reset serdes

    esper->Write("ag", "ctrl_a", "0x40000000");

    v = esper->Read("ag", "stat_c");
    vv = strtoul(v.c_str()+1, NULL, 0);
    printf("stat_c: 0x%08x (serdes in reset)\n", vv);

    esper->Write("ag", "ctrl_a", "0x00000000");

    v = esper->Read("ag", "stat_c");
    vv = strtoul(v.c_str()+1, NULL, 0);
    printf("stat_c: 0x%08x\n", vv);

    if ((vv & 0xFFFF) == 0xFFFF) {
      break;
    }
    
    sleep(1);
  }
    
  delete esper;

  return 0;
}

// end file
