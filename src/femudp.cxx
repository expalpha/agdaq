//
// femudp.cxx
//
// Frontend for receiving and storing UDP packets as MIDAS data banks.
//

#include <stdio.h>
#include <netdb.h> // getnameinfo()
#include <string.h> // memcpy()
#include <errno.h> // errno
#include <signal.h> // SIGPIPE
#include <assert.h> // assert()
#include <unistd.h> // usleep()

#include <string>
#include <vector>
#include <deque>
#include <mutex>
#include <thread>

//#include "midas.h"
#include "tmfe.h"

struct Source
{
  struct sockaddr addr;
  char bank_name[5];
  std::string host_name;
};

#define XALIGN8(x)  (((x)+7) & ~7)

class SgEvent
{
public:
   uint32_t fHeader[6];
   std::vector<char*>  fSgPtr;
   std::vector<size_t> fSgLen;

public:
   SgEvent() // ctor
   {
   }

   ~SgEvent() // dtor
   {
      Clear();
   }

   void Clear()
   {
      //Print();
      for (int i=0; i<6; i++) {
         fHeader[i] = 0;
      }
      assert(fSgPtr.size() == fSgLen.size());
      // count from 1: slot[0] is pointer the header
      for (size_t i=1; i<fSgPtr.size(); i++) {
         if (fSgPtr[i]) {
            free(fSgPtr[i]);
            fSgPtr[i] = NULL;
            fSgLen[i] = 0;
         }
      }
      fSgPtr.clear();
      fSgLen.clear();
   }

   void Init(uint16_t event_id, uint16_t trigger_mask, uint32_t serial, uint32_t timestamp)
   {
      Clear();
      fHeader[0] = trigger_mask<<16 | event_id; // 0x00010001; // event id and trigger mask
      fHeader[1] = serial; // 1; // serial
      fHeader[2] = timestamp; // TMFE::GetTime(); // timestamp in seconds
      fHeader[3] = 2*4;  // data size
      fHeader[4] = 0;    // bank header: data size
      fHeader[5] = 0x31; // bank header: flags: bank32a format
      fSgPtr.push_back(NULL); // header
      fSgLen.push_back(0);
   }

   void AddBank(char* ptr, size_t len)
   {
      fHeader[3] += len;
      fHeader[4] += len;
      fSgPtr.push_back(ptr);
      fSgLen.push_back(len);
   }

   void AddBank32a(char* ptr)
   {
      uint32_t* wptr = (uint32_t*)ptr;
      size_t len = 4*4 + XALIGN8(wptr[2]);
      AddBank(ptr, len);
   }

   void Print() const
   {
      printf("Header:\n");
      for (int i=0; i<6; i++) {
         printf("  header[%d] 0x%08x (%d)\n", i, fHeader[i], fHeader[i]);
      }
      printf("Segments: %d\n", (int)fSgPtr.size());
      for (size_t i=0; i<fSgPtr.size(); i++) {
         printf("  segment[%d] %p %6d", (int)i, fSgPtr[i], (int)fSgLen[i]);
         if (fSgPtr[i]) {
            uint32_t* bptr32a = (uint32_t*)fSgPtr[i];
            printf(", bank 0x%08x 0x%08x 0x%08x 0x%08x", bptr32a[0], bptr32a[1], bptr32a[2], bptr32a[3]);
            printf(", [%s] %d", (char*)bptr32a, bptr32a[2]);
         }
         printf("\n");
      }
   }
};

TMFeResult SendSgEvent(TMFeEquipment* eq, SgEvent* e)
{
   //e->Print();
   assert(e->fSgPtr.size() == e->fSgLen.size());
   e->fSgPtr[0] = (char*)e->fHeader;
   e->fSgLen[0] = 6*4;
   return eq->EqSendEvent(e->fSgPtr.size(), (const char**)e->fSgPtr.data(), e->fSgLen.data(), false);
}

class EqMudp : public TMFeEquipment
{
public:
   int fDataSocket = 0;
   std::thread* fUdpReadThread = NULL;
   std::thread* fSendThread = NULL;

   std::deque<char*> fSendQueue;
   std::mutex fSendQueueMutex;

   int fConfRcvBufSize = 2*100*1024*1024; // gige is 100 Mbytes/sec, 2 seconds of data
   int fConfPacketSize = 1500; // UDP packet size
   int fConfMaxBufferedPackets = 10000; // drop packets if sender cannot keep up
   int fConfMaxPacketsPerEvent =  8000; // do not package too many packets per midas event
   int fConfMaxPacketsPerRecv = 100000; // udp packets to read in one recvmmsg() syscall

   std::vector<Source> fSrc;

   int fCountDroppedPackets = 0;
   int fCountUnknownPackets = 0;
   bool fSkipUnknownPackets = false;
   int fCountUnhappy = 0;

   // socket buffer
   int fMaxBcount = 0;
   int fMaxBcountEver = 0;

   // recvmmsg() syscall
   int fMaxRecvPackets = 0;
   int fMaxRecvPacketsEver = 0;
   
   // thread buffer
   int fThreadBufferSize = 0;
   int fMaxThreadBuffer = 0;
   int fMaxThreadBufferEver = 0;

   // send queue
   int fSendQueueSize = 0;
   int fMaxSendQueue = 0;
   int fMaxSendQueueEver = 0;

   EqMudp(const char* eqname, const char* eqfilename) // ctor
      : TMFeEquipment(eqname, eqfilename)
   {
      fEqConfEnablePoll = false;

      fEqConfReadConfigFromOdb = false;

      fEqConfEventID = 1;
      fEqConfTriggerMask = 0;
      if (strcmp(eqname, "ADC_UDP")==0)
         fEqConfBuffer = "BUFADC";
      else if (strcmp(eqname, "PWB_A_UDP")==0)
         fEqConfBuffer = "BUFPWBA";
      else if (strcmp(eqname, "PWB_B_UDP")==0)
         fEqConfBuffer = "BUFPWBB";
      else if (strcmp(eqname, "PWB_C_UDP")==0)
         fEqConfBuffer = "BUFPWBC";
      else if (strcmp(eqname, "PWB_D_UDP")==0)
         fEqConfBuffer = "BUFPWBD";
      else
         fEqConfBuffer = "BUFUDP";
      fEqConfPeriodMilliSec = 1000;
      fEqConfLogHistory = 0;
      fEqConfWriteCacheSize = 0;

      fEqConfReadOnlyWhenRunning = true;
      fEqConfWriteEventsToOdb = false;
      fEqConfMaxEventSize = 10*1024*1024;
      fEqConfBufferSize = 335544320;
   }

   ~EqMudp() // dtor
   {
      JoinUdpReadThread();
      JoinSendThread();
   }

   TMFeResult HandleInit(const std::vector<std::string>& args)
   {
      int udp_port = 50006;

      fOdbEqSettings->RI("udp_port", &udp_port, true);
      fOdbEqSettings->RI("rcvbuf_size", &fConfRcvBufSize, true);
      fOdbEqSettings->RI("packet_size", &fConfPacketSize, true);
      fOdbEqSettings->RI("max_packets_per_recv", &fConfMaxPacketsPerRecv, true);
      fOdbEqSettings->RI("max_buffered_packets", &fConfMaxBufferedPackets, true);
      fOdbEqSettings->RI("max_packets_per_event",  &fConfMaxPacketsPerEvent, true);

      int status;
   
      int fd = socket(AF_INET, SOCK_DGRAM, 0);
   
      if (fd < 0) {
         fMfe->Msg(MERROR, "Mudp::Init", "socket(AF_INET,SOCK_DGRAM) returned %d, errno %d (%s)", fd, errno, strerror(errno));
         exit(1);
      }

      int opt = 1;
      status = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

      if (status == -1) {
         fMfe->Msg(MERROR, "Mudp::Init", "setsockopt(SOL_SOCKET,SO_REUSEADDR) returned %d, errno %d (%s)", status, errno, strerror(errno));
         exit(1);
      }

      status = setsockopt(fd, SOL_SOCKET, SO_RCVBUF, &fConfRcvBufSize, sizeof(fConfRcvBufSize));

      if (status == -1) {
         fMfe->Msg(MERROR, "Mudp::Init", "setsockopt(SOL_SOCKET,SO_RCVBUF,%d) returned %d, errno %d (%s)", fConfRcvBufSize, status, errno, strerror(errno));
         exit(1);
      }

      struct sockaddr_in local_addr;
      memset(&local_addr, 0, sizeof(local_addr));
      
      local_addr.sin_family = AF_INET;
      local_addr.sin_port = htons(udp_port);
      local_addr.sin_addr.s_addr = INADDR_ANY;
      
      status = bind(fd, (struct sockaddr *)&local_addr, sizeof(local_addr));
      
      if (status == -1) {
         fMfe->Msg(MERROR, "Mudp::Init", "bind(port=%d) returned %d, errno %d (%s)", udp_port, status, errno, strerror(errno));
         exit(1);
      }

      int xbufsize = 0;
      unsigned size = sizeof(xbufsize);
      
      status = getsockopt(fd, SOL_SOCKET, SO_RCVBUF, &xbufsize, &size);

      if (status == -1) {
         fMfe->Msg(MERROR, "Mudp::Init", "getsockopt(SOL_SOCKET,SO_RCVBUF) returned %d, errno %d (%s)", status, errno, strerror(errno));
         exit(1);
      }

      if (xbufsize < fConfRcvBufSize) {
         fMfe->Msg(MERROR, "Mudp::Init", "Cannot allocate %d bytes for the UDP socket buffer, got only %d bytes, check the value of \"sysctl net.core.rmem_max\", maybe it is too small.", fConfRcvBufSize, xbufsize);
         exit(1);
      }
      
      fMfe->Msg(MLOG, "Mudp::Init", "Listening on UDP port: %d, socket receive buffer size: %d", udp_port, xbufsize);

      fDataSocket = fd;

      StartUdpReadThread();
      StartSendThread();

      EqSetStatus("Init ok", "#00FF00");

      return TMFeOk();
   }

   void StartUdpReadThread()
   {
      assert(fUdpReadThread == NULL);
      fUdpReadThread = new std::thread(&EqMudp::UdpReadThread, this);
   }

   int find_source(Source* src, const void* addr, int addr_len)
   {
      char host[NI_MAXHOST], service[NI_MAXSERV];
      
      int status = getnameinfo((const struct sockaddr*) addr, addr_len, host, NI_MAXHOST, service, NI_MAXSERV, NI_NUMERICSERV);
      
      if (status != 0) {
         fMfe->Msg(MERROR, "read_udp", "getnameinfo() returned %d (%s), errno %d (%s)", status, gai_strerror(status), errno, strerror(errno));
         return -1;
      }
      
      char bankname[5];
      //int size = sizeof(bankname);
      
      if (host[0] == 'a' && host[1] == 'd' && host[2] == 'c') {
         int module = atoi(host+3);
         bankname[0] = 'A';
         bankname[1] = 'A';
         bankname[2] = '0' + module/10;
         bankname[3] = '0' + module%10;
         bankname[4] = 0;
      } else if (host[0] == 'p' && host[1] == 'w' && host[2] == 'b') {
         int module = atoi(host+3);
         bankname[0] = 'P';
         bankname[1] = 'B';
         bankname[2] = '0' + module/10;
         bankname[3] = '0' + module%10;
         bankname[4] = 0;
      } else {
         fMfe->Msg(MERROR, "read_udp", "UDP packet from unknown host \"%s\"", host);
         return -1;
      }
      
#if 0      
      status = db_get_value(hDB, hKeySet, host, bankname, &size, TID_STRING, FALSE);
      
      if (status == DB_NO_KEY) {
         fMfe->Msg(MERROR, "read_udp", "UDP packet from unknown host \"%s\"", host);
         fMfe->Msg(MINFO, "read_udp", "Register this host by running following commands:");
         fMfe->Msg(MINFO, "read_udp", "odbedit -c \"create STRING /Equipment/%s/Settings/%s\"", EQ_NAME, host);
         fMfe->Msg(MINFO, "read_udp", "odbedit -c \"set /Equipment/%s/Settings/%s AAAA\", where AAAA is the MIDAS bank name for this host", EQ_NAME, host);
         return -1;
      } else if (status != DB_SUCCESS) {
         fMfe->Msg(MERROR, "read_udp", "db_get_value(\"/Equipment/%s/Settings/%s\") status %d", EQ_NAME, host, status);
         return -1;
      }
      
      if (strlen(bankname) != 4) {
         fMfe->Msg(MERROR, "read_udp", "ODB \"/Equipment/%s/Settings/%s\" should be set to a 4 character MIDAS bank name", EQ_NAME, host);
         fMfe->Msg(MINFO, "read_udp", "Use this command:");
         fMfe->Msg(MINFO, "read_udp", "odbedit -c \"set /Equipment/%s/Settings/%s AAAA\", where AAAA is the MIDAS bank name for this host", EQ_NAME, host);
         return -1;
      }
#endif
      
      fMfe->Msg(MLOG, "read_udp", "UDP packets from host \"%s\" will be stored in bank \"%s\"", host, bankname);
      
      src->host_name = host;
      memcpy(src->bank_name, bankname, 5);
      memcpy(&src->addr, addr, sizeof(src->addr));
      
      return 0;
   }

   bool MatchAddr(const Source* s, const void *addr, int addr_len)
   {
      int v = memcmp(&s->addr, addr, addr_len);
#if 0
      for (int i=0; i<addr_len; i++)
         printf("%3d - 0x%02x 0x%02x\n", i, ((char*)&s->addr)[i], ((char*)addr)[i]);
      printf("match %d, hostname [%s] bank [%s], status %d\n", addr_len, s->host_name.c_str(), s->bank_name, v);
#endif
      return v==0;
   }

   bool WaitForData(int msec)
   {
      int status;
      fd_set fdset;
      struct timeval timeout;
      
      FD_ZERO(&fdset);
      FD_SET(fDataSocket, &fdset);
      
      timeout.tv_sec = msec/1000;
      timeout.tv_usec = (msec%1000)*1000;
      
      status = select(fDataSocket+1, &fdset, NULL, NULL, &timeout);
      
#ifdef EINTR
      if (status < 0 && errno == EINTR) {
         return false; // watchdog interrupt, try again
      }
#endif
      
      if (status < 0) {
         fMfe->Msg(MERROR, "WaitForData", "Error: select() returned %d, errno %d (%s)", status, errno, strerror(errno));
         fMfe->fShutdownRequested = true;
         return false;
      }
      
      if (status == 0) {
         return false; // timeout
      }
      
      if (FD_ISSET(fDataSocket, &fdset)) {
         return true; // have data
      }
      
      // timeout
      return false;
   }

   const char* GetUdpBankName(const void* addr, socklen_t addr_len)
   {
      for (unsigned i=0; i<fSrc.size(); i++) {
         if (MatchAddr(&fSrc[i], addr, addr_len)) {
            //printf("rd %d, bank [%s]\n", rd, pbankname);
            return fSrc[i].bank_name;
         }
      }
      
      if (fSkipUnknownPackets) {
         fCountUnknownPackets++;
         return NULL;
      }
      
      Source sss;
      
      int status = find_source(&sss, addr, addr_len);
      
      if (status < 0) {
         
         fCountUnknownPackets++;
         
         if (fCountUnknownPackets > 10) {
            fSkipUnknownPackets = true;
            fMfe->Msg(MERROR, "GetUdpBankName", "further messages about unknown packets are now suppressed...");
         }
         
         return 0;
      }
      
      fSrc.push_back(sss);
      
      return fSrc.back().bank_name;
   }

   //double fLastFlush = 0;

   void FlushThreadBuffer(std::vector<char*>* buf)
   {
      int size = buf->size();

      //double now = TMFE::GetTime();
      //printf("FlushThreadBuffer: %d, %.6f sec\n", size, now - fLastFlush);
      //fLastFlush = now;

      bool drop_everything = false;

      {
         std::lock_guard<std::mutex> lock(fSendQueueMutex);

         if ((int)fSendQueue.size() >= fConfMaxBufferedPackets) {
            drop_everything = true;
         } else {
            for (int i=0; i<size; i++) {
               char* pp = (*buf)[i];
               (*buf)[i] = NULL;
               fSendQueue.push_back(pp);
            }

            buf->clear();

            fSendQueueSize = fSendQueue.size();
            if (fSendQueueSize > fMaxSendQueue)
               fMaxSendQueue = fSendQueueSize;
            if (fSendQueueSize > fMaxSendQueueEver)
               fMaxSendQueueEver = fSendQueueSize;
         }
      }

      if (drop_everything) {
         if (fCountUnhappy == 0) {
            fMfe->Msg(MERROR, "FlushThreadBuffer", "Send queue is full, dropping %d packets, please increase Settings/max_buffered_packets", (int)buf->size());
         }
         fCountUnhappy++;

         int size = buf->size();
         for (int i=0; i<size; i++) {
            char* pp = (*buf)[i];
            if (pp) {
               (*buf)[i] = NULL;
               //delete pp;
               free(pp);
               fCountDroppedPackets++;
            }
         }
         buf->clear();
      }

      fThreadBufferSize = buf->size();
   }
   
   void UdpReadThread()
   {
      printf("UdpReader thread started\n");

      std::vector<char*> buf;

      std::vector<struct mmsghdr> msgvec;

      for (int i=0; i<fConfMaxPacketsPerRecv; i++) {
         struct mmsghdr mhdr;
         mhdr.msg_hdr.msg_name = malloc(sizeof(struct sockaddr));
         mhdr.msg_hdr.msg_namelen = sizeof(struct sockaddr);
         mhdr.msg_hdr.msg_iov = (struct iovec*)malloc(sizeof(struct iovec));
         char* pbank = (char*)malloc(4*4 + fConfPacketSize);
         char* pdata = pbank + 4*4;
         mhdr.msg_hdr.msg_iov->iov_base = pdata;
         mhdr.msg_hdr.msg_iov->iov_len  = fConfPacketSize;
         mhdr.msg_hdr.msg_iovlen = 1;
         mhdr.msg_hdr.msg_control = NULL;
         mhdr.msg_hdr.msg_controllen = 0;
         mhdr.msg_hdr.msg_flags = 0;
         mhdr.msg_len = 0;
         msgvec.push_back(mhdr);
      }

      double last_flush_time = TMFE::GetTime();

      bool do_sleep = false;

      while (!fMfe->fShutdownRequested) {
         if (do_sleep) {
            //printf("sleep!\n");
            bool have_data = WaitForData(10);
            if (!have_data) {
               do_sleep = true;
               
               if (buf.size() > 0) {
                  FlushThreadBuffer(&buf);
                  last_flush_time = TMFE::GetTime();
               }
               
               continue;
            }
         }
         
         int rd = recvmmsg(fDataSocket, msgvec.data(), msgvec.size(), MSG_DONTWAIT, NULL);
         
         if (rd == -1) {
            if (errno == EAGAIN) {
               do_sleep = true;
               continue;
            }
            
            fMfe->Msg(MERROR, "UdpReadThread", "Error reading UDP socket, recvmmsg() returned %d, errno %d (%s)", rd, errno, strerror(errno));
            fMfe->fShutdownRequested = true;
            break;
         }
         
         if (rd == 0) {
            //printf("recvmmsg(%d) returned %d\n", (int)msgvec.size(), rd);
            do_sleep = true;
            continue;
         }

         if (rd > fMaxRecvPackets)
            fMaxRecvPackets = rd;
         
         if (rd > fMaxRecvPacketsEver)
            fMaxRecvPacketsEver = rd;

         int bcount = 0;
         
         //printf("recvmmsg(%d) returned %d\n", (int)msgvec.size(), rd);
         
         do_sleep = false;
         
         for (int i=0; i<rd; i++) {
            const char* bank_name = GetUdpBankName(msgvec[i].msg_hdr.msg_name, msgvec[i].msg_hdr.msg_namelen);
            //printf("packet %d: msg_hdr, msg_len %d, bank_name [%s]\n", i, (int)msgvec[i].msg_len, bank_name);

            bcount += msgvec[i].msg_len;
            
            if ((int)msgvec[i].msg_len >= fConfPacketSize) { // truncated UDP packet
               // stop the thread
               fCountUnhappy++;
               fMfe->Msg(MERROR, "UdpReadThread", "UDP packet was truncated to %d bytes, please restart with larger value of Settings/packet_size", fConfPacketSize);
               fMfe->fShutdownRequested = true;
               continue;
            }
            
            if (bank_name == NULL) { // unknown packet
               fCountUnhappy++;
               // read again
               continue;
            }
            
            char* pdata = (char*)msgvec[i].msg_hdr.msg_iov->iov_base;
            char* pbank = pdata - 4*4;
            uint32_t* pbank32a = (uint32_t*)pbank;

            //typedef struct {
            //   char name[4];                       /**< - */
            //   DWORD type;                         /**< - */
            //   DWORD data_size;                    /**< - */
            //   DWORD reserved;                     /**< - */
            //} BANK32A;

            memcpy(pbank, bank_name, 4); // pbank32a[0]
            pbank32a[1] = TID_BYTE;
            pbank32a[2] = msgvec[i].msg_len;
            pbank32a[3] = 0;
            
            char* pnew = (char*)malloc(4*4 + fConfPacketSize);

            msgvec[i].msg_hdr.msg_iov->iov_base = pnew + 4*4;
            
            bcount += msgvec[i].msg_len;
            
            //printf("%d.", msgvec[i].msg_len);
            
            buf.push_back(pbank);
         }

         if (bcount > 0) {
            //printf("buffered packets: %d (%d bytes)\n", wcount, bcount);
            if (bcount > fConfRcvBufSize) {
               fCountUnhappy++;
               fMfe->Msg(MERROR, "UdpReadThread", "UDP buffer overflow: received %d bytes while buffer size is only %d bytes, please increase Settings/rcvbuf_size", bcount, fConfRcvBufSize);
            }
            if (bcount > fMaxBcount) {
               fMaxBcount = bcount;
            }
            if (bcount > fMaxBcountEver) {
               fMaxBcountEver = bcount;
            }
         }

         fThreadBufferSize = buf.size();

         if (fThreadBufferSize > fMaxThreadBuffer)
            fMaxThreadBuffer = fThreadBufferSize;
         if (fThreadBufferSize > fMaxThreadBufferEver)
            fMaxThreadBufferEver = fThreadBufferSize;
         
         double now = TMFE::GetTime();

         if ((now > last_flush_time + 0.1) ||
             ((int)buf.size() >= fConfMaxBufferedPackets/4)) {
            FlushThreadBuffer(&buf);
            last_flush_time = now;
         }
      }

      printf("UdpReader thread shutdown\n");
   }

   void JoinUdpReadThread()
   {
      if (fUdpReadThread) {
         fUdpReadThread->join();
         delete fUdpReadThread;
         fUdpReadThread = NULL;
      }
   }

   void StartSendThread()
   {
      assert(fSendThread == NULL);
      fSendThread = new std::thread(&EqMudp::SendThread, this);
   }

   void SendThread()
   {
      printf("Send thread started\n");

      SgEvent e;

      while (!fMfe->fShutdownRequested) {

         e.Init(fEqConfEventID, fEqConfTriggerMask, fEqSerial, 0);

         int num_banks = 0;
      
         {
            std::lock_guard<std::mutex> lock(fSendQueueMutex);
            int size = fSendQueue.size();
            //printf("Have events: %d\n", size);
            if (size > fConfMaxPacketsPerEvent)
               size = fConfMaxPacketsPerEvent;
            for (int i=0; i<size; i++) {
               //buf.push_back(fSendQueue.front());
               e.AddBank32a(fSendQueue.front());
               fSendQueue.pop_front();
               //printf("move %d\n", i);
               num_banks++;
            }
            //printf("remained %d\n", gUdpPacketBuf.size());
            fSendQueueSize = fSendQueue.size();
         }

         if (num_banks == 0) {
            //double t1 = TMFE::GetTime();
            TMFE::Sleep(0.001);
            //double t2 = TMFE::GetTime();
            //printf("sleep time %f\n", t2-t1);
            continue;
         }
         
         //printf("event banks %d, size %d/%d.\n", count, fEq->BkSize(event), max_event_size);

         SendSgEvent(this, &e);
      }

      printf("Send thread shutdown\n");
   }

   void JoinSendThread()
   {
      if (fSendThread) {
         fSendThread->join();
         delete fSendThread;
         fSendThread = NULL;
      }
   }

   //std::string HandleRpc(const char* cmd, const char* args)
   //{
   //   fMfe->Msg(MLOG, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);
   //   return "OK";
   //}

   TMFeResult HandleBeginRun(int run_number)
   {
      fMfe->Msg(MLOG, "HandleBeginRun", "Begin run!");
      fCountDroppedPackets = 0;
      fCountUnknownPackets = 0;
      fSkipUnknownPackets = false;
      fMaxBcount = 0;
      fMaxBcountEver = 0;
      fMaxRecvPackets = 0;
      fMaxRecvPacketsEver = 0;
      fMaxThreadBuffer = 0;
      fMaxThreadBufferEver = 0;
      fMaxSendQueue = 0;
      fMaxSendQueueEver = 0;
      fCountUnhappy = 0;
      EqZeroStatistics();
      EqWriteStatistics();
      return TMFeOk();
   }

   TMFeResult HandleEndRun(int run_number)
   {
      fMfe->Msg(MLOG, "HandleEndRun", "End run!");
      for (int i=0; i<10; i++) {
         if (fThreadBufferSize == 0)
            break;
         fMfe->Msg(MLOG, "HandleEndRun", "Thread buffer has %d entries, waiting %d to drain empty!", fThreadBufferSize, i);
         ::usleep(100000);
      }
      for (int i=0; i<10; i++) {
         if (fSendQueueSize == 0)
            break;
         fMfe->Msg(MLOG, "HandleEndRun", "Send queue has %d entries, waiting %d to drain empty!", fSendQueueSize, i);
         ::usleep(100000);
      }
      EqWriteStatistics();
      return TMFeOk();
   }

   void HandlePeriodic()
   {
      //printf("periodic!\n");
      char buf[256];
      double MiB = 1024*1024;
      sprintf(buf, "Buffers: socket %.1f/%.1f MiB, recv %d/%d, thread %d/%d, send %d/%d, Packets: dropped %d, unknown %d, State: unhappy %d, running: %d", fMaxBcount/MiB, fMaxBcountEver/MiB, fMaxRecvPackets, fMaxRecvPacketsEver, fMaxThreadBuffer, fMaxThreadBufferEver, fMaxSendQueue, fMaxSendQueueEver, fCountDroppedPackets, fCountUnknownPackets, fCountUnhappy, fMfe->fStateRunning);
      fMaxBcount = 0;
      fMaxRecvPackets = 0;
      fMaxThreadBuffer = 0;
      fMaxSendQueue = 0;
      if (fCountUnhappy) {
         EqSetStatus(buf, "#FFFF00");
      } else {
         //EqSetStatus(buf, "#00FF00");
         EqSetStatus("Ok", "#00FF00");
      }
      EqWriteStatistics();
   }
};

class FeMudp: public TMFrontend
{
public:
   FeMudp() // ctor
   {
   }

   void HandleUsage()
   {
      printf("FeEverything::HandleUsage!\n");
   };
   
   TMFeResult HandleArguments(const std::vector<std::string>& args)
   {
      std::string eqname = args[0];
      FeSetName((std::string("femudp_") + eqname).c_str());
      FeAddEquipment(new EqMudp(eqname.c_str(), __FILE__));
      return TMFeOk();
   };
   
   TMFeResult HandleFrontendInit(const std::vector<std::string>& args)
   {
      fMfe->DeregisterTransitionPause();
      fMfe->DeregisterTransitionResume();
      fMfe->DeregisterTransitionStartAbort();
      fMfe->SetTransitionSequenceStop(95);
      return TMFeOk();
   };
   
   TMFeResult HandleFrontendReady(const std::vector<std::string>& args)
   {
      //fMfe->StartPeriodicThread();
      //fMfe->StartRpcThread();
      return TMFeOk();
   };
   
   void HandleFrontendExit()
   {
   };
};

// boilerplate main function

int main(int argc, char* argv[])
{
   FeMudp fe_mudp;
   return fe_mudp.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
