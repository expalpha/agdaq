// fecaenet14xxet.cxx
//
// MIDAS frontend for CAEN HV PS R1419ET/R1470ET

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <signal.h>

#include <vector>

#include "tmfe.h"

#include "KOtcp.h"

#include "midas.h"

#define C(x) ((x).c_str())

class R14xxet:
   public TMFeEquipment
{
public: // configuration
   std::string fHostname;
   std::string fPort = "1470";

   int fInitialConnectDelayMilliSec = 200;
   int fPollPeriodMilliSec = 5000;

   bool fTraceCmdMon = false;

public: // state
   KOtcpConnection* fSocket = NULL;

   std::string fBdnch;
   int fNumChan = 0;

   double fFastUpdate = 0;

   std::vector<double> fMaxImon;

public:
   R14xxet(const char* eqname, const char* eqfilename, const std::string& hostname) // ctor
      : TMFeEquipment(eqname, eqfilename)
   {
      printf("R14xxet::ctor!\n");

      fHostname = hostname;

      // configure the equipment here:

      fEqConfBuffer = "";
      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 3;
      fEqConfPeriodMilliSec = 1000;
      fEqConfLogHistory = 1;
      fEqConfEnablePoll = true;
      fEqConfPollSleepSec = 0.100;

      fEqConfReadOnlyWhenRunning = false; // overwrite ODB Common RO_RUNNING to false
      fEqConfWriteEventsToOdb = false; // overwrite ODB Common RO_ODB to true
   }

   ~R14xxet() // dtor
   {
      printf("R14xxet::dtor!\n");
      if (fSocket) {
         fSocket->Close();
         delete fSocket;
         fSocket = NULL;
      }
   }

   TMFeResult HandleInit(const std::vector<std::string>& args)
   {
      printf("R14xxet::HandleInit!\n");

      EqSetStatus("Started...", "white");

      fSocket = new KOtcpConnection(fHostname.c_str(), fPort.c_str());

      return TMFeOk();
   }

#if 0
   bool Wait(int wait_sec, const char* explain)
   {
      time_t to = time(NULL) + wait_sec;
      while (1) {
         int a = 0;

         KOtcpError e = s->BytesAvailable(&a);
         if (e.error) {
            fMfe->Msg(MERROR, "Wait", "Socket error %s", e.message.c_str());
            s->Close();
            EqSetStatus("Lost connection", "red");
            return false;
         }
         
         //printf("Wait %d sec, available %d\n", wait_sec, a);
         if (a > 0)
            return true;
         
         if (time(NULL) > to) {
            fMfe->Msg(MERROR, "Wait", "Timeout waiting for repy to command: %s", explain);
            s->Close();
            EqSetStatus("Lost connection", "red");
            return false;
         }
         
         fMfe->SleepMSec(1);
         
         if (fMfe->fShutdownRequested) {
            fMfe->Msg(MERROR, "Wait", "Shutdown command while waiting for reply to command: %s", explain);
            return false;
         }
      }
      // not reached
   }
#endif
         
   bool Wait(int wait_sec, const char* explain)
   {
      int a = 0;

      KOtcpError e = fSocket->WaitBytesAvailable(wait_sec*1000, &a);
      if (e.error) {
         fMfe->Msg(MERROR, "Wait", "Error waiting for reply to command \"%s\": %s", explain, e.message.c_str());
         fSocket->Close();
         EqSetStatus("Lost connection", "red");
         return false;
      }
         
      //printf("Wait %d sec, available %d\n", wait_sec, a);
      if (a > 0)
         return true;
         
      fMfe->Msg(MERROR, "Wait", "Timeout waiting for reply to command \"%s\"", explain);
      fSocket->Close();
      EqSetStatus("Lost connection", "red");
      return false;
   }
         
   std::string Exch(const char* cmd)
   {
      if (fMfe->fShutdownRequested)
         return "";
      
      std::string ss = cmd;
      ss += '\r';
      ss += '\n';
      
      KOtcpError err = fSocket->WriteString(ss);
      
      if (err.error) {
         fMfe->Msg(MERROR, "Exch", "Communication error: Command [%s], WriteString error [%s]", cmd, err.message.c_str());
         fSocket->Close();
         EqSetStatus("Lost connection", "red");
         return "";
      }
      
      if (!Wait(5, cmd))
         return "";
      
      std::string reply;
      
      err = fSocket->ReadString(&reply, 64*1024);
      
      if (err.error) {
         fMfe->Msg(MERROR, "Exch", "Communication error: Command [%s], ReadString error [%s]", cmd, err.message.c_str());
         fSocket->Close();
         EqSetStatus("Lost connection", "red");
         return "";
      }

      bool trace = true;
      
      if (strstr(cmd, "CMD:MON") != NULL) {
         trace = fTraceCmdMon;
      }

      if (trace) {
         printf("command %s, reply [%s]\n", cmd, reply.c_str());
      }

      if (strstr(reply.c_str(), "VAL:ERR") != NULL) {
         fMfe->Msg(MERROR, "Exch", "Error: Command command \"%s\" failed with reply \"%s\"", cmd, reply.c_str());
      }

      return reply;
   }

   static std::string V(const std::string& s)
   {
      std::string::size_type p = s.find("VAL:");
      if (p == std::string::npos)
         return "";
      return s.substr(p+4);
   }

   static std::vector<std::string> split(const std::string& s)
   {
      std::vector<std::string> v;
      
      std::string::size_type p = 0;
      while (1) {
         std::string::size_type pp = s.find(";", p);
         //printf("p %d, pp %d\n", p, pp);
         if (pp == std::string::npos) {
            v.push_back(s.substr(p));
            return v;
         }
         v.push_back(s.substr(p, pp-p));
         p = pp + 1;
      }
      // not reached
   }

   static std::vector<double> D(std::vector<std::string>& v)
   {
      std::vector<double> vv;
      for (unsigned i=0; i<v.size(); i++) {
         //printf("v[%d] is [%s]\n", i, C(v[i]));
         vv.push_back(atof(C(v[i])));
      }
      return vv;
   }

   void WR(const char* name, const char* v)
   {
      if (fMfe->fShutdownRequested)
         return;
      
      std::string path;
      path += "/Equipment/";
      path += fEqName;
      path += "/Readback/";
      path += name;
      //printf("Write ODB %s Readback %s: %s\n", C(path), name, v);
      int status = db_set_value(fMfe->fDB, 0, C(path), v, strlen(v)+1, 1, TID_STRING);
      if (status != DB_SUCCESS) {
         printf("WR: db_set_value status %d\n", status);
      }
   }
         
   void WVD(const char* name, const std::vector<double> &v)
   {
      if (fMfe->fShutdownRequested)
         return;
      
      std::string path;
      path += "/Equipment/";
      path += fEqName;
      path += "/Variables/";
      path += name;
      //printf("Write ODB %s Readback %s: %s\n", C(path), name, v);
      int status = db_set_value(fMfe->fDB, 0, C(path), &v[0], sizeof(double)*v.size(), v.size(), TID_DOUBLE);
      if (status != DB_SUCCESS) {
         printf("WVD: db_set_value status %d\n", status);
      }
   }
         
   void WRStat(const std::vector<double> &stat)
   {
      if (fMfe->fShutdownRequested)
         return;
      
      std::string path;
      path += "/Equipment/";
      path += fEqName;
      path += "/Readback/";
      path += "STAT_BITS";
      
      std::string v;
      
      for (unsigned i=0; i<stat.size(); i++) {
         if (i>0)
            v += ";";
         
         int b = stat[i];
         char buf[256];
         sprintf(buf, "0x%04x", b);
         v += buf;
         
         if (b & (1<<0)) v += " ON";
         if (b & (1<<1)) { v += " RUP"; fFastUpdate = TMFE::GetTime() + 10; }
         if (b & (1<<2)) { v += " RDW"; fFastUpdate = TMFE::GetTime() + 10; }
         if (b & (1<<3)) v += " OVC";
         if (b & (1<<4)) v += " OVV";
         if (b & (1<<5)) v += " UNV";
         if (b & (1<<6)) v += " MAXV";
         if (b & (1<<7)) v += " TRIP";
         if (b & (1<<8)) v += " OVP";
         if (b & (1<<9)) v += " OVT";
         if (b & (1<<10)) v += " DIS";
         if (b & (1<<11)) v += " KILL";
         if (b & (1<<12)) v += " ILK";
         if (b & (1<<13)) v += " NOCAL";
         if (b & (1<<14)) v += " bit14";
         if (b & (1<<15)) v += " bit15";
      }
      
      //printf("Write ODB %s value %s\n", C(path), C(v));

      int status = db_set_value(fMfe->fDB, 0, C(path), C(v), v.length()+1, 1, TID_STRING);
      if (status != DB_SUCCESS) {
         printf("WR: db_set_value status %d\n", status);
      }
   }
         
   void WRAlarm(const std::string &alarm)
   {
      if (fMfe->fShutdownRequested)
         return;
      
      std::string path;
      path += "/Equipment/";
      path += fEqName;
      path += "/Readback/";
      path += "BDALARM_BITS";
      
      std::string v;
      
      int b = atoi(C(alarm));
      
      char buf[256];
      sprintf(buf, "0x%04x", b);
      v += buf;
      
      if (b & (1<<0)) v += " CH0";
      if (b & (1<<1)) v += " CH1";
      if (b & (1<<2)) v += " CH2";
      if (b & (1<<3)) v += " CH3";
      if (b & (1<<4)) v += " PWFAIL";
      if (b & (1<<5)) v += " OVP";
      if (b & (1<<6)) v += " HVCKFAIL";
      
      //printf("Write ODB %s value %s\n", C(path), C(v));
      int status = db_set_value(fMfe->fDB, 0, C(path), C(v), v.length()+1, 1, TID_STRING);
      if (status != DB_SUCCESS) {
         printf("WR: db_set_value status %d\n", status);
      }
      
      if (b) {
         std::string vv = "Alarm: " + v;
         EqSetStatus(C(vv), "#FF0000");
         
         std::string aa = fEqName + " alarm " + v;
         fMfe->TriggerAlarm(C(fEqName), C(aa), "Alarm");
      } else {
         path.clear();
         path += "/Equipment/";
         path += fEqName;
         path += "/Variables/";
         path += "VMON[2]";
         double awv;
         int size = sizeof(awv);
         db_get_value(fMfe->fDB, 0, C(path), &awv, &size, TID_DOUBLE, FALSE);
         path.clear();
         path += "/Equipment/";
         path += fEqName;
         path += "/Variables/";
         path += "IMON[2]";
         double awi;
         size = sizeof(awi);
         db_get_value(fMfe->fDB, 0, C(path), &awi, &size, TID_DOUBLE, FALSE);
         char str[64];
         sprintf(str, "Anode Wires %4.0lf[V]@%3.0lf[nA]", awv, 1000.*awi);
         if (awi*1000. > 100.0) {
            EqSetStatus(str, "#F1C40F");
         } else {
            EqSetStatus(str, "#00FF00");
         }
         fMfe->ResetAlarm(C(fEqName));
      }
   }

   std::string RE1(const char* name)
   {
      if (fMfe->fShutdownRequested)
         return "";
      std::string cmd;
      cmd += "$BD:00:CMD:MON,PAR:";
      cmd += name;
      std::string r = Exch(C(cmd));
      if (r.length() < 1)
         return "";
      std::string v = V(r);
      WR(name, C(v));
      return v;
   }

   std::string RE(const char* name)
   {
      if (fMfe->fShutdownRequested)
         return "";
      std::string cmd;
      //Exch(s, "$BD:00:CMD:MON,CH:4,PAR:VSET");
      cmd += "$BD:00:CMD:MON,CH:";
      cmd += fBdnch;
      cmd += ",PAR:";
      cmd += name;
      std::string r = Exch(C(cmd));
      if (r.length() < 1)
         return "";
      std::string v = V(r);
      WR(name, C(v));
      return v;
   }

   std::vector<double> VE(const char* name, bool write_to_odb = true)
   {
      std::vector<double> vd;
      if (fMfe->fShutdownRequested)
         return vd;
      std::string cmd;
      //Exch(s, "$BD:00:CMD:MON,CH:4,PAR:VSET");
      cmd += "$BD:00:CMD:MON,CH:";
      cmd += fBdnch;
      cmd += ",PAR:";
      cmd += name;
      std::string r = Exch(C(cmd));
      if (r.length() < 1)
         return vd;
      std::string v = V(r);
      std::vector<std::string> vs = split(v);
      vd = D(vs);
      //WVD(name, vd);
      if (write_to_odb)
         fOdbEqVariables->WDA(name, vd);
      return vd;
   }

   // write parameter, no value

   void WE(const char* name, int ch)
   {
      char cmd[256];
      sprintf(cmd, "$BD:00:CMD:SET,CH:%d,PAR:%s", ch, name);
      Exch(cmd);
   }

   // write parameter, floating point value

   void WED(const char* name, int ch, double v)
   {
      char cmd[256];
      sprintf(cmd, "$BD:00:CMD:SET,CH:%d,PAR:%s,VAL:%f", ch, name, v);
      Exch(cmd);
   }

   // write parameter, string value

   void WES(const char* name, int ch, const char* v)
   {
      char cmd[256];
      sprintf(cmd, "$BD:00:CMD:SET,CH:%d,PAR:%s,VAL:%s", ch, name, v);
      Exch(cmd);
   }

   void WES(const char* name, const char* v)
   {
      char cmd[256];
      sprintf(cmd, "$BD:00:CMD:SET,PAR:%s,VAL:%s", name, v);
      Exch(cmd);
   }

   // read important parameters

   void ReadImportant()
   {
      RE1("BDILK"); // interlock status

      std::string bdalarm = RE1("BDALARM"); // alarm status
      if (bdalarm.length() > 0) {
         WRAlarm(bdalarm);
      }

      VE("VSET"); // voltage setpoint VSET
      VE("VMON"); // voltage actual value VMON

      VE("ISET"); // current setpoint ISET, uA
      std::vector<double> imon = VE("IMON"); // current actual value, uA

      if (fMaxImon.size() < imon.size()) {
         fMaxImon.resize(imon.size());
      }

      for (unsigned i=0; i<imon.size(); i++) {
         if (imon[i] > fMaxImon[i])
            fMaxImon[i] = imon[i];
      }

      fOdbEqVariables->WDA("IMON_max", fMaxImon);

      for (unsigned i=0; i<imon.size(); i++) {
         fMaxImon[i] = imon[i];
      }

      std::vector<double> stat = VE("STAT"); // channel status
      WRStat(stat);
   }

   void ReadSettings()
   {
      RE1("BDILKM"); // interlock mode
      RE1("BDCTR"); // control mode
      RE1("BDTERM"); // local bus termination
      
      fMfe->Sleep(0.001);

      VE("VMIN"); // VSET minimum value
      VE("VMAX"); // VSET maximum value
      VE("VDEC"); // VSET number of decimal digits
         
      fMfe->Sleep(0.001);

      VE("IMIN");    // ISET minimum value
      VE("IMAX");    // ISET maximum value
      VE("ISDEC");   // ISET number of decimal digits
      RE("IMRANGE"); // current monitor range HIGH/LOW
      VE("IMDEC");   // IMON number of decimal digits
         
      fMfe->Sleep(0.001);

      VE("MAXV");  // MAXVSET max VSET value
      VE("MVMIN"); // MAXVSET minimum value
      VE("MVMAX"); // MAXVSET maximum value
      VE("MVDEC"); // MAXVSET number of decimal digits

      fMfe->Sleep(0.001);

      VE("RUP"); // ramp up V/s
      VE("RUPMIN");
      VE("RUPMAX");
      VE("RUPDEC");
         
      fMfe->Sleep(0.001);

      VE("RDW"); // ramp down V/s
      VE("RDWMIN");
      VE("RDWMAX");
      VE("RDWDEC");
         
      fMfe->Sleep(0.001);

      VE("TRIP"); // trip time, sec
      VE("TRIPMIN");
      VE("TRIPMAX");
      VE("TRIPDEC");

      fMfe->Sleep(0.001);

      RE("PDWN"); // power down RAMP/KILL
      RE("POL"); // polarity
   }

   void TurnOn(int chan)
   {
      fMfe->Msg(MINFO, "TurnOn", "Turning on channel %d", chan);
      WE("ON", chan);
   }
         
   void TurnOff(int chan)
   {
      fMfe->Msg(MINFO, "TurnOff", "Turning off channel %d", chan);
      WE("OFF", chan);
   }

   std::vector<double> vset;
   std::vector<double> iset;
   std::vector<double> maxv;
   std::vector<double> rup;
   std::vector<double> rdw;
   std::vector<double> trip;
   std::vector<double> pdwn;
   std::vector<double> imrange;

   void UpdateSettings()
   {
      fMfe->Msg(MINFO, "UpdateSettings", "Writing settings from ODB to \"%s\"", fHostname.c_str());

      //Exch(mfe, s, "$BD:00:CMD:SET,PAR:BDILKM,VAL:OPEN"); // set interlock mode
      //Exch(mfe, s, "$BD:00:CMD:SET,PAR:BDILKM,VAL:CLOSED");
      WES("BDILKM", "CLOSED");
      
      //Exch(mfe, s, "$BD:00:CMD:SET,PAR:BDCLR"); // clear alarm signal
      
      //Exch(mfe, s, "$BD:00:CMD:SET,CH:4,PAR:VSET,VAL:1;2;3;4");
     
      int nch = fNumChan;

      fOdbEqSettings->RDA("VSET", &vset, true, nch);
      fOdbEqSettings->RDA("ISET", &iset, true, nch);
      fOdbEqSettings->RDA("MAXV", &maxv, true, nch);
      fOdbEqSettings->RDA("RUP",  &rup,  true, nch);
      fOdbEqSettings->RDA("RDW",  &rdw,  true, nch);
      fOdbEqSettings->RDA("TRIP", &trip, true, nch);
      fOdbEqSettings->RDA("PDWN", &pdwn, true, nch);
      fOdbEqSettings->RDA("IMRANGE", &imrange, true, nch);
 
      for (int i=0; i<nch; i++) {
         WED("VSET", i, vset[i]);
         WED("ISET", i, iset[i]);
         WED("MAXV", i, maxv[i]);
         WED("RUP",  i, rup[i]);
         WED("RDW",  i, rdw[i]);
         WED("TRIP", i, trip[i]);
         
         if (pdwn[i] == 1) {
            WES("PDWN", i, "RAMP");
         } else if (pdwn[i] == 2) {
            WES("PDWN", i, "KILL");
         }
         
         if (imrange[i] == 1) {
            WES("IMRANGE", i, "HIGH");
         } else if (imrange[i] == 2) {
            WES("IMRANGE", i, "LOW");
         }
         
#if 0
         double onoff = OdbGetValue(mfe, fEqName, "ONOFF", i, nch);
         if (onoff == 1) {
            WE(mfe, eq, s, "ON", i);
         } else if (onoff == 2) {
            WE(mfe, eq, s, "OFF", i);
         }
#endif
      }
      
      fFastUpdate = TMFE::GetTime() + 30;
   }

   TMFeResult HandleRpc(const char* cmd, const char* args, std::string& response)
   {
      fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);

      int mask = 0;
      int all = 0;
      int chan = 0;
      if (strcmp(args, "all") == 0) {
         all = 1;
         mask = 0xF;
      } else {
         chan = atoi(args);
         mask |= (1<<chan);
      }

      //printf("mask 0x%x\n", mask);

      if (strcmp(cmd, "update_settings")==0) {
         UpdateSettings();

         ::sleep(1);
         ::sleep(1);

         ReadImportant();

         fFastUpdate = TMFE::GetTime() + 30;

         return TMFeOk();
      } else if (strcmp(cmd, "turn_on")==0) {

         UpdateSettings();

         if (all) {
            for (int i=0; i<fNumChan; i++) {
               TurnOn(i);
            }
         } else {
            TurnOn(chan);
         }

         sleep(1);
         sleep(1);

         ReadImportant();

         fFastUpdate = TMFE::GetTime() + 30;

         return TMFeOk();
      } else if (strcmp(cmd, "turn_off")==0) {

         //UpdateSettings();

         if (all) {
            for (int i=0; i<fNumChan; i++) {
               TurnOff(i);
            }
         } else {
            TurnOff(chan);
         }

         sleep(1);
         sleep(1);

         ReadImportant();

         fFastUpdate = TMFE::GetTime() + 30;

         return TMFeOk();
      } else {
         return TMFeOk();
      }
   }

   int fConnectDelayMilliSec = 0;
   bool fFirstTime = true;

   double fLastRead = 0;

   void HandlePeriodic()
   {
      if (!fSocket->fConnected) {
         EqSetStatus("Connecting...", "white");

         KOtcpError e = fSocket->Connect();
         if (e.error) {
            EqSetStatus("Cannot connect, trying again", "red");
            fMfe->Msg(MINFO, "main", "Cannot connect to %s:%s, Connect() error %s", fHostname.c_str(), fPort.c_str(), e.message.c_str());
            if (fConnectDelayMilliSec == 0) {
               fConnectDelayMilliSec = fInitialConnectDelayMilliSec;
            } else if (fConnectDelayMilliSec < 5*60*1000) {
               fConnectDelayMilliSec *= 2;
            }
            fEqConfPeriodMilliSec = fConnectDelayMilliSec;
            return;
         }

         fConnectDelayMilliSec = 0;

         fMfe->Msg(MINFO, "HandlePeriodic", "Connected to %s:%s", fHostname.c_str(), fPort.c_str());
         EqSetStatus("Connected...", "white");
         fFirstTime = true;
         fEqConfPeriodMilliSec = 1000;
         return;
      }

      double start_time = TMFE::GetTime();

      //Exch(mfe, s, "$BD:00:CMD:MON,PAR:BDNAME");
      std::string bdname = RE1("BDNAME"); // mainframe name and type
      std::string bdnch  = RE1("BDNCH"); // channels number
      
      if (fMfe->fShutdownRequested) {
         return;
      }

      if (bdname.length() < 1 || bdnch.length() < 1) {
         fSocket->Close();
         fMfe->Msg(MERROR, "HandlePeriodic", "Cannot read BDNAME or BDNCH, will try to reconnect after 10 sec...");
         fEqConfPeriodMilliSec = 10000;
         return;
      }

      fBdnch = bdnch;
      fNumChan = atoi(bdnch.c_str());

      std::string bdfrel = RE1("BDFREL"); // firmware release
      std::string bdsnum = RE1("BDSNUM"); // serial number

      if (fFirstTime) {
         fMfe->Msg(MINFO, "HandlePeriodic", "Device %s is model %s with %s channels, firmware %s, serial %s", fHostname.c_str(), bdname.c_str(), bdnch.c_str(), bdfrel.c_str(), bdsnum.c_str());
         fFirstTime = false;
      }
      
      ReadImportant();
         
      ReadSettings();

      //time_t end_time = time(NULL);

      double end_time = TMFE::GetTime();

      double elapsed = end_time - start_time;
      double period  = start_time - fLastRead;

      printf("period: %.3f sec, readout time: %.3f sec\n", period, elapsed);

      fLastRead = start_time;

      fOdbEqVariables->WD("readout_time_sec", elapsed);

      // do not write to the power supply unless requested by user
      //if (first_time) {
      //   hv->UpdateSettings();
      //}
         
      if (0) {
         //mfe->SleepMSec(1000);
         
         //Exch(mfe, s, "$BD:00:CMD:SET,PAR:BDILKM,VAL:OPEN");
         Exch("$BD:00:CMD:SET,PAR:BDILKM,VAL:CLOSED");
         
         Exch("$BD:00:CMD:SET,PAR:BDCLR");
         
         //Exch(mfe, s, "$BD:00:CMD:SET,CH:4,PAR:VSET,VAL:1;2;3;4");
         Exch("$BD:00:CMD:SET,CH:0,PAR:VSET,VAL:10");
         Exch("$BD:00:CMD:SET,CH:1,PAR:VSET,VAL:11");
         Exch("$BD:00:CMD:SET,CH:2,PAR:VSET,VAL:12");
         Exch("$BD:00:CMD:SET,CH:3,PAR:VSET,VAL:13");
      }

      if (fFastUpdate != 0) {
         if (TMFE::GetTime() > fFastUpdate)
            fFastUpdate = 0;
      }

      if (fFastUpdate) {
         fEqConfPeriodMilliSec = 2000;
         return;
      }
      
      fEqConfPeriodMilliSec = fPollPeriodMilliSec;
   }

   double fLastPoll = 0;
   double fLastImon = 0;

   bool HandlePoll()
   {
      double now = TMFE::GetTime();
      //double period = now - fLastPoll;
      fLastPoll = now;
      //printf("poll period %.3f\n", period);

      // not connected?
      if (!fSocket->fConnected)
         return false;

      // not initialized?
      if (fNumChan == 0)
         return false;

      // limit how often we bang the power supply
      if (fLastImon != 0 && now - fLastImon < 0.500)
         return false;
      
      std::vector<double> imon = VE("IMON", false); // current actual value, uA

      if (fMaxImon.size() < imon.size()) {
         fMaxImon.resize(imon.size());
      }

      if (0) {
         printf("currents: ");
         for (unsigned i=0; i<imon.size(); i++) {
            printf(" %.6f/%.6f", imon[i], fMaxImon[i]);
         }
         printf("\n");
      }

      for (unsigned i=0; i<imon.size(); i++) {
         if (imon[i] > fMaxImon[i])
            fMaxImon[i] = imon[i];
      }

      fLastImon = now;
      
      return false;
   }
};

#if 0

class EqEverything :
   public TMFeEquipment
{
public:

   void HandleUsage()
   {
      printf("EqEverything::HandleUsage!\n");
   }

   bool HandlePoll()
   {
      //printf("EqEverything::HandlePoll!\n");

      if (!fMfe->fStateRunning) // only poll when running
         return false;

      double r = drand48();
      if (r > 0.999) {
         // return successful poll rarely
         printf("EqEverything::HandlePoll!\n");
         return true;
      }

      return false;
   }

   void HandlePollRead()
   {
      printf("EqEverything::HandlePollRead!\n");

         char buf[1024];

         ComposeEvent(buf, sizeof(buf));
         BkInit(buf, sizeof(buf));
         
         uint32_t* ptr = (uint32_t*)BkOpen(buf, "poll", TID_UINT32);
         for (int i=0; i<16; i++) {
            *ptr++ = lrand48();
         }
         BkClose(buf, ptr);
         
         EqSendEvent(buf, false); // do not write polled data to ODB and history
   }
};

#endif

class FeCaenHv: public TMFrontend
{
public:
   FeCaenHv() // ctor
   {
      printf("FeCaenHv::ctor!\n");
   }

   void HandleUsage()
   {
      printf("FeCaenHv::HandleUsage!\n");
   };
   
   TMFeResult HandleArguments(const std::vector<std::string>& args)
   {
      printf("FeCaenHv::HandleArguments!\n");

      if (args.size() != 1) {
         return TMFeErrorMessage("Should be given one argument: hostname of power supply");
      }

      std::string hvhost = args[0];
      
      FeSetName((std::string("fecaen_") + hvhost).c_str());
      FeAddEquipment(new R14xxet((std::string("CAEN_") + hvhost).c_str(), __FILE__, hvhost));
      return TMFeOk();
   };
   
   TMFeResult HandleFrontendInit(const std::vector<std::string>& args)
   {
      printf("FeCaenHv::HandleFrontendInit!\n");
      fMfe->DeregisterTransitions();
      return TMFeOk();
   };
   
   TMFeResult HandleFrontendReady(const std::vector<std::string>& args)
   {
      printf("FeCaenHv::HandleFrontendReady!\n");
      //fMfe->StartPeriodicThread();
      //fMfe->StartRpcThread();
      return TMFeOk();
   };
   
   void HandleFrontendExit()
   {
      printf("FeCaenHv::HandleFrontendExit!\n");
   };
};

// boilerplate main function

int main(int argc, char* argv[])
{
   FeCaenHv fe_caenhv;
   return fe_caenhv.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
