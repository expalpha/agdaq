#!/usr/bin/perl -w

foreach my $core (@ARGV) {
  print "core $core\n";
  next unless -r $core;
  my $core_age = -M $core;
  my $file = `file $core`;
  print "file $file\n";
  my ($execfn) = $file =~ /execfn: \'(.*)\',/;
  print "execfn $execfn\n";
  next unless -r $execfn;
  my $execfn_age = -M $execfn;
  print "age: core: $core_age execfn: $execfn_age\n";
  if ($core_age > $execfn_age) {
    printf("core file is older than executable\n");
    next;
  }
  # (gdb) bt
  # (gdb) bt full
  # (gdb) info threads
  # (gdb) thread apply all bt
  # (gdb) thread apply all bt full
  #my $gdbcmd = "-ex bt -ex \"bt full\" -ex \"info threads\" -ex \"thread apply all bt\"";
  my $gdbcmd = "-ex bt -ex \"info threads\" -ex \"thread apply all bt\"";
  #my $gdbcmd = "-ex bt";
  my $gdb = "gdb $execfn $core -batch $gdbcmd";
  print "gdb: $gdb\n";
  system $gdb;
}

#end
