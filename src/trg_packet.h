// unpacking of TRG UDP packets

#ifndef atpacket_H
#define atpacket_H

struct TrgPacket
{
   uint32_t num_bytes = 0;
   uint32_t packet_no = 0; // 31-bit
   uint32_t header    = 0;
   uint32_t footer    = 0;
   bool     is_trig = false;
   bool     is_fifo = false;
   bool     is_error = false;
   uint32_t trig_no_header = 0; // 28 bits
   uint32_t trig_no_footer = 0; // 28 bits
   uint32_t fifo_no_header = 0; // 28 bits
   uint32_t fifo_status    = 0;
   uint32_t fifo_woffset   = 0;
   uint32_t fifo_num_words = 0;
   uint32_t fifo_no_footer = 0; // 28 bits
   uint32_t ts_625 = 0;
   uint32_t fw_rev = 0;

   void Print() const;
   void Unpack(const char* buf, int bufsize);
};

void TrgPacket::Print() const
{
   if (is_trig) {
      printf("TrgPacket: packet %d, trig %d, ts 0x%08x, fw_rev 0x%08x, %d bytes", packet_no, trig_no_header, ts_625, fw_rev, num_bytes);
      if (trig_no_footer != trig_no_header) {
         printf(", trig_no mismatch header 0x%08x footer 0x%08x", header, footer);
      }
   } else if (is_fifo) {
      printf("TrgPacket: packet %d, fifo %d, status 0x%08x, %d fifo words, %d bytes", packet_no, fifo_no_header, fifo_status, fifo_num_words, num_bytes);
      if (fifo_no_footer != fifo_no_header) {
         printf(", fifo_no mismatch header 0x%08x footer 0x%08x", header, footer);
      }
   } else {
      printf("TrgPacket: packet %d, header 0x%08x, %d bytes", packet_no, header, num_bytes);
   }

   if (is_error) {
      printf(", ERROR!");
   }
}

void TrgPacket::Unpack(const char* buf, int bufsize)
{
   const uint32_t *p32 = (uint32_t*)buf;

   packet_no = p32[0] & 0x7FFFFFFF; // 31-bit
   header = p32[1];
   num_bytes = bufsize;

   if ((header & 0xF0000000) == 0x80000000) {
      if (bufsize == 80) {
         is_trig = true;
         trig_no_header = header & 0xFFFFFFF;
         ts_625 = p32[2];
         fw_rev = p32[18];
         footer = p32[19];
         trig_no_footer = footer & 0xFFFFFFF;
      } else if (bufsize == 76) {
         is_trig = true;
         trig_no_header = header & 0xFFFFFFF;
         ts_625 = p32[2];
         footer = p32[18];
         trig_no_footer = footer & 0xFFFFFFF;
      } else if (bufsize == 40) {
         is_trig = true;
         trig_no_header = header & 0xFFFFFFF;
         ts_625 = p32[2];
         footer = p32[9];
         trig_no_footer = footer & 0xFFFFFFF;
      }
      if (fifo_no_footer != fifo_no_header) {
         is_error = true;
      }
   } else if ((header & 0xF0000000) == 0xC0000000) {
      is_fifo = true;
      fifo_no_header = header & 0xFFFFFFF;
      fifo_status = p32[2];
      int hbytes = 4 + 4 + 4 + 4; // packet no, 0xC fifo no, fifo status and 0xE fifo no
      if (bufsize > hbytes) {
         fifo_woffset = 3; // packet no, 0xC fifo no, fifo status
         fifo_num_words = (bufsize - hbytes)/4;
         footer = p32[bufsize/4-1];
         fifo_no_footer = footer & 0xFFFFFFF;
      }
      if (fifo_no_footer != fifo_no_header) {
         is_error = true;
      }
   } else if ((header & 0xF0000000) == 0xD0000000) {
      is_fifo = true;
      fifo_no_header = header & 0xFFFFFFF;
      uint32_t deadbeef = p32[2];
      fifo_status = p32[3];
      if (deadbeef != 0xDEADBEEF)
         is_error = true;
      int hbytes = 4 + 4 + 4 + 4 + 4; // packet no, 0xC fifo no, 0xDEADBEEF, fifo status and 0xE fifo no
      if (bufsize > hbytes) {
         fifo_woffset = 4; // packet no, 0xC fifo no, 0xDEADBEEF, fifo status
         fifo_num_words = (bufsize - hbytes)/4;
         footer = p32[bufsize/4-1];
         fifo_no_footer = footer & 0xFFFFFFF;
      }
      if (fifo_no_footer != fifo_no_header) {
         is_error = true;
      }
   } else {
      is_error = true;
   }
}

#endif
/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
