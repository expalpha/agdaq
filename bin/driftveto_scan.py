#!/usr/bin/python3

import socket
import sys
import requests
import json
import time

from pythonMidas import setValue, getString

from statistics import median

import numpy as np
import matplotlib.pyplot as plt

path = "/home/agdaq/online/driftveto_scan/driftveto_scan_"

def SetROperiod1():
    key='/Equipment/CTRL/Settings/PeriodScalers'
    setValue(key,1)
    UpdateTrg()

def SetROperiod10():
    key='/Equipment/CTRL/Settings/PeriodScalers'
    setValue(key,10)
    UpdateTrg()

def UpdateADC():
    headers={'Content-Type':'application/json','Accept':'application/json'}
    par={'client_name':'fectrl','cmd':'init_adc_all','args':''}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id':0}    
    url='http://localhost:8080?mjsonrpc'
    res=requests.post(url, json=payload, headers=headers).json()
    return res['result']['reply']

def UpdateTrg():
    headers={'Content-Type':'application/json','Accept':'application/json'}
    par={'client_name':'fectrl','cmd':'init_trg','args':''}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id':0}    
    url='http://localhost:8080?mjsonrpc'
    res=requests.post(url, json=payload, headers=headers).json()
    if(res['result']['status'] != 1):
        print("InitTrg: ", res);
    par={'client_name':'fectrl','cmd':'start_trg','args':''}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id':0}    
    res=requests.post(url, json=payload, headers=headers).json()
    if(res['result']['status'] != 1):
        print("StartTrg: ", res);

    return res['result']['reply']

def SetADC16threshold(thr):
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    setValue(key,thr)
    UpdateADC()

def ReadADC16threshold():
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    thr=getString( key )
    return int(thr)

def ResetADC16threshold():
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    setValue(key,8192)
    UpdateADC()

def SetADC32threshold(thr):
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    setValue(key,thr)
    UpdateADC()

def SetDriftVeto(width):
    key='/Equipment/CTRL/Settings/TRG/DriftWidthClk'
    setValue(key,width)
    UpdateTrg()

def ResetDriftVeto():
    SetDriftVeto(300)

def ReadADC32threshold():
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    thr=getString( key )
    return int(thr)

def ResetADC32threshold():
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    setValue(key,-8192)
    UpdateADC()

def ReadRates16():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=getString( key ).split()
    rates=[float(r) for r in rates[80:96]]
    return rates

def ReadRates32():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=getString( key ).split()
    rates=[float(r) for r in rates[32:48]]
    return rates

def AvgRates(adc):
    if adc == 'A16':
        rates = ReadRates16()
    elif adc == 'A32':
        rates = ReadRates32()
    else:
        print('Unknown adc type',adc)
    return sum(rates)/float(len(rates))

def ReadRate(index):
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=getString( key ).split()
    return float(rates[index])

def ReadRateArray():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=getString( key ).split()
    rates=[float(r) for r in rates]
    return rates

def TimeMedianRates(scalers):
    Nmeas=5
    data = np.zeros((len(scalers),Nmeas))
    med = np.zeros(len(data))
    for r in range(Nmeas):
        time.sleep(3)
        rates = ReadRateArray()
        for i in range(len(scalers)):
            data[i][r] = float(rates[scalers[i]])
            #print(i,r,data[i][r],flush=True)
    for i in range(len(data)):
        med[i] = median(data[i])
        #print("median: ",i,med[i],data[i],flush=True)
    return med

def plot_scan(fname):
    data=np.loadtxt(fname, delimiter='\t', unpack=True)

    plot_title = 'ADC Threshold Scan'
    plot_label = 'Trigger Rate'
    if 'AW' in fname:
        thr,raw,trig = data
        plot_title += ' TPC AW'
        plot_label += ' MLU'
        x_ticks=range(-31000,1000,1000)
        plt.gca().invert_xaxis()
        raw_label = 'ADC time avg.'
    elif 'BV' in fname:
        thr = data[0]
        raw = data[5]
        trig = data[6]
        plot_title += 'Barrel Veto'
        plot_label += ' BV mult'
        raw_label = 'BV grandOR'
        x_ticks=range(0,31000,1000)

    #plt.plot(thr,raw,'.',label='ADC time avg.')
    #plt.plot(thr,trig,'.',label=plot_label)
    plt.semilogy(thr,raw,'.',label=raw_label)
    plt.semilogy(thr,trig,'.',label=plot_label)
    plt.title(plot_title)
    plt.xlabel('Threshold: ADC counts above pedestal')
    plt.xticks(x_ticks,fontsize=8)
    plt.ylabel('Counting Rate in Hz')
    plt.grid(True)
    plt.legend(loc='best')

    fig=plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    fig.tight_layout()
    fig.savefig(fname[:-4]+'.png')
    plt.show()


def DriftVetoScan():
    # width_list = [700, 600, 500, 400, 350, 300, 250, 200, 150, 100, 50]
    width_list = [700, 600, 500, 400, 300, 200, 100, 80, 60, 50, 40, 30, 20, 10]

    # for high counting rate, fectrl scaler readout period 
    # has to be changed from 10 sec to 1 sec
    SetROperiod1()

    print('Start...',flush=True)

    fname=path+time.strftime("%Y%b%d_%H%M", time.localtime())+'.dat'
    print(fname,flush=True)

    f=open(fname, 'w')
    # f.write("#gnuplot plot [][] 'tmp.dat' u 1:8 w lp t 'adc09', '' u 1:9 w lp t 'adc10', '' u 1:10 w lp t 'adc11', '' u 1:11 w lp t 'adc12', '' u 1:12 w lp t 'adc13', '' u 1:13 w lp t 'adc14', '' u 1:14 w lp t 'adc18', '' u 1:15 w lp t 'adc16'\n")
    f.write("#driftwidth\tns\tMLU\tBsc_gOR\tCoinc\ttrg_in\ttrg_drift\ttrg_out\n")
    for width in width_list:
        print('set:',width, end=" ",flush=True)
        
        # set the new threshold and wait for plateau
        SetDriftVeto(width)

        print(" done, sleeping....",flush=True)
        time.sleep(10)
        
        # read the trigger rates and write them to file
        # rates = TimeAverageBVmult()
        scalers = (
            67,                 # MLU
            72,                 # Bsc_gOR
            74,                 # Coinc
            2,                  # trg_in
            64,                 # trg_drift
            1,                  # trg_out
        )
        # rates = TimeMedianBVmult()
        rates = TimeMedianRates(scalers)
        # string = str(a16thr)
        # s = (str(a16thr)+' \t'+' \t'.join(['%.1f']*len(rates))) % tuple(rates)
        s = '{0: >5}'.format(width) + ' {0: >6}'.format(width*16)
        for i in range(len(rates)):
            s = s + ' {0: >8}'.format(round(rates[i],2))
        f.write(s+"\n")
        f.flush()
        print('  read: ', width, ", rates: ", rates,flush=True)

    # reset the scaler readout period
    SetROperiod10()
    # reset the threshold
    ResetDriftVeto()
        
    f.close()
    print(fname)
    print('Finished!')
    return fname    

################################################################################

if __name__ == "__main__":
    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())

    fname = DriftVetoScan()
