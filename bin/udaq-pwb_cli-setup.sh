#!/bin/bash

stty -echoctl # hide ^C

if [[ $1 == "" ]]; then
    PWB="pwb32"
else
    PWB="$1"
fi

echo "Testing ${PWB}..."

img=$(esper-tool read ${PWB} update image_selected)
if [[ "${img}" == "[0]" ]]; then
    echo "Factory Image selected"
    echo "Rebooting into User Page"
    esper-tool write -d 1 ${PWB} update image_selected
    esper-tool write -d 1 ${PWB} update reconfigure
fi

on=$(esper-tool read ${PWB} system device)
if [[ "${on}" == "\"PWB\"" ]]; then 
    echo "${on} test can proceed"
else
    echo "${on} test cannot proceed"
    exit 1
fi


ctrl_c() {
        echo "** Trapped CTRL-C"
	esper-tool write -d true ${PWB} board reset_nios
	echo "killing $MATE1 $MATE2"
	kill $MATE1
	sleep 1
	exit 0
}


#set -x

##################################################
# OFFLOAD                                        #
##################################################

echo "Setup UDP forwarding"
esper-tool write -d 50005 ${PWB} offload dst_port
echo "UDP port " `esper-tool read ${PWB} offload dst_port`

esper-tool write -d true ${PWB} offload enable
echo "UDP offload Enabled?" `esper-tool read ${PWB} offload enable`
echo "UDP offload Status: " `esper-tool read ${PWB} offload status`

# start UDP offloader
echo "start UDP offloader"
mate-terminal --hide-menubar --geometry=80x24 --working-directory="$HOME/andrea/edevtool/pwb-chunk/src" -e "python3 pwb_chunk.py 192.168.1.1:50005 192.168.1.1:1985" &
MATE1=$!
################################################################
# Attempting to setup UDP server at 192.168.1.1:50005
# Attempting to setup 0MQ publisher at tcp://192.168.1.1:1985
# Creating Queue...
# UDP Thread Started...
################################################################

# start viewer
echo "start viewer"
mate-terminal --hide-menubar --geometry=80x24 --working-directory="$HOME/andrea/edevtool/pwb-wave/src" -e "python3 pwb_wave.py 192.168.1.1:1985" &
MATE2=$!
################################################################
# Attempting to setup 0MQ subscriber at 192.168.1.1:1985
# qt.qpa.xcb: X server does not support XInput 2
# qt.qpa.xcb: QXcbConnection: XCB error: 1 (BadRequest), sequence: 169, resource id: 132, major code: 130 (Unknown), minor code: 47
### are all these errors relevant?
### Waveform should be displayed now
################################################################


##################################################
# SCAx                                           #
##################################################

echo "Set SCA in test functionality mode"
echo "Enable only channel 0 in each SCA"
inject="[true, "; inject="$inject"$(for ch in `seq 70`; do echo "false,";done); inject="${inject} false]"
for x in `seq 0 3`; do
    esper-tool write -d 3 ${PWB} sca${x} test
    t=$(esper-tool read ${PWB} sca${x} test)
    if [[ "${t}" == '[3]' ]]; then
	echo "test functionality mode enabled for SCA-${x}"
    fi	
    esper-tool write -d "${inject}" ${PWB} sca${x} select_ch
done


##################################################
# SIGNALPROC                                     #
##################################################

# I believe that this is 1Hz
pulse_inter=62500000 
# with 2 SCAs or 144 channels good (with few dropped triggers)
# up to 50 Hz pulse_inter=1250000

pulse_width=255 # this, together with the trigger delay makes
# one nice (negative) pulse 


echo "Enable Internal Test Pulse"
esper-tool write -d [true,true,true,true] ${PWB} signalproc test_pulse_ena
esper-tool read ${PWB} signalproc test_pulse_ena

set -x

# enalbe periodic test pulse
esper-tool write -d true ${PWB} signalproc test_pulse_interval_ena
# set pulsing period
esper-tool write -d ${pulse_inter}  ${PWB} signalproc test_pulse_interval
# set pulse width
esper-tool write -d [${pulse_width},${pulse_width},${pulse_width},${pulse_width}] ${PWB} signalproc test_pulse_wdt

##################################################
# TRIGGER                                        #
##################################################

trig_del=255 # between 60 and 500 is OK

# disable data suppression
esper-tool write -d [0xFFFFFFFF,0xFFFFFFFF,0x7FFFF] ${PWB} signalproc sca_a_ch_force_bitmap
esper-tool write -d [0xFFFFFFFF,0xFFFFFFFF,0x7FFFF] ${PWB} signalproc sca_b_ch_force_bitmap
esper-tool write -d [0xFFFFFFFF,0xFFFFFFFF,0x7FFFF] ${PWB} signalproc sca_c_ch_force_bitmap
esper-tool write -d [0xFFFFFFFF,0xFFFFFFFF,0x7FFFF] ${PWB} signalproc sca_d_ch_force_bitmap

# enable trigger
esper-tool write -d true ${PWB} signal_proc force_run

# enable trigger
esper-tool write -d true ${PWB} trigger enable_all

# periodic trigger
esper-tool write -d true ${PWB} trigger intp_trig_ena
esper-tool write -d ${trig_del} ${PWB} trigger intp_trig_delay


set +x

echo "Visit my webpage at:"
echo "https://daq16.triumf.ca/proxy/${PWB}/"

while : ; do
    pkg=$(esper-tool read ${PWB} offload tx_cnt)
    trig_req=$(esper-tool read ${PWB} trigger total_requested)
    trig_acc=$(esper-tool read ${PWB} trigger total_accepted)
    trig_drop=$(esper-tool read ${PWB} trigger total_dropped)
    echo -ne "$pkg packets sent\tTrigger( $trig_req $trig_acc $trig_drop )( requested accepted dropped )\r"
    trap ctrl_c SIGINT
    sleep 1
done


