#!/usr/bin/python3

import socket
import sys
import requests
import json
import time

from pythonMidas import setValue, getString

from statistics import median

import numpy as np
import matplotlib.pyplot as plt

path = "/home/agdaq/online/threshold_scan/threshold_scan_"

def SetROperiod1():
    key='/Equipment/CTRL/Settings/PeriodScalers'
    setValue(key,1)
    UpdatePeriod()

def SetROperiod10():
    key='/Equipment/CTRL/Settings/PeriodScalers'
    setValue(key,10)
    UpdatePeriod()

def UpdateADC():
    headers={'Content-Type':'application/json','Accept':'application/json'}
    par={'client_name':'fectrl','cmd':'init_adc_all','args':''}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id':0}    
    url='http://localhost:8080?mjsonrpc'
    res=requests.post(url, json=payload, headers=headers).json()
    return res['result']['reply']

def UpdatePeriod():
    headers={'Content-Type':'application/json','Accept':'application/json'}
    par={'client_name':'fectrl','cmd':'init_trg','args':''}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id':0}    
    url='http://localhost:8080?mjsonrpc'
    res=requests.post(url, json=payload, headers=headers).json()
    print("UpdatePeriod: ", res);
    return res['result']['reply']

def SetADC16threshold(thr):
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    setValue(key,thr)
    UpdateADC()

def ReadADC16threshold():
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    thr=getString( key )
    return int(thr)

def ResetADC16threshold():
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    setValue(key,8192)
    UpdateADC()

def SetADC32threshold(thr):
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    setValue(key,thr)
    UpdateADC()

def ReadADC32threshold():
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    thr=getString( key )
    return int(thr)

def ResetADC32threshold():
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    setValue(key,-8192)
    UpdateADC()

def ReadRates16():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=getString( key ).split()
    rates=[float(r) for r in rates[80:96]]
    return rates

def ReadRates32():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=getString( key ).split()
    rates=[float(r) for r in rates[32:48]]
    return rates

def AvgRates(adc):
    if adc == 'A16':
        rates = ReadRates16()
    elif adc == 'A32':
        rates = ReadRates32()
    else:
        print('Unknown adc type',adc)
    return sum(rates)/float(len(rates))

def ReadRate(index):
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=getString( key ).split()
    return float(rates[index])

def ReadRateArray():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=getString( key ).split()
    return rates

def ReadMLUrate():
    return ReadRate(67)

def Read4orMore():
    return ReadRate(11)

def ReadBVgOR():
    return ReadRate(72)

def ReadBVmult():
    return ReadRate(73)

def TimeAverageRate(scaler):
    time_avg=0.0
    Nmeas=10
    for r in range(Nmeas):
        if scaler == 'A16':
            time_avg += AvgRates('A16')
        elif scaler == 'A32':
            time_avg += AvgRates('A32')
        elif scaler == 'mlu':
            time_avg += ReadMLUrate()
        elif scaler == '4':
            time_avg += Read4orMore()
        else:
            print("don't know this scaler", scaler)
        time.sleep(2)
    return time_avg/float(Nmeas)

def TimeAverageBVmult():
    time_avg = [0.0]*6
    Nmeas=10
    for r in range(Nmeas):
        for i in range(8,12):
            time_avg[i-8] += ReadRate(i)/float(Nmeas)
        time_avg[4] += ReadBVgOR()/float(Nmeas)
        time_avg[5] += ReadBVmult()/float(Nmeas)
        time.sleep(11)
    return time_avg

def TimeMedianBVmult():
    Nmeas=7
    data = np.zeros((6,Nmeas))
    med = np.zeros(6)
    for r in range(Nmeas):
        for i in range(8,12):
            data[i-8][r] = ReadRate(i)
        data[4][r] = ReadBVgOR()
        data[5][r] = ReadBVmult()
        time.sleep(11)
    for i in range(len(data)):
        med[i] = median(data[i])
    return med

def TimeMedianRates(scalers):
    Nmeas=5
    data = np.zeros((len(scalers),Nmeas))
    med = np.zeros(len(data))
    for r in range(Nmeas):
        time.sleep(3)
        rates = ReadRateArray()
        for i in range(len(scalers)):
            data[i][r] = float(rates[scalers[i]])
            #print(i,r,data[i][r],flush=True)
    for i in range(len(data)):
        med[i] = median(data[i])
        #print("median: ",i,med[i],data[i],flush=True)
    return med

def Verbose():
    print('==================================================')
    print('ADC16 threshold:',ReadADC16threshold(),'ADC16 avg. rates: %1.0f Hz'%AvgRates('A16'))
    print('ADC32 threshold:',ReadADC32threshold(),'ADC32 avg. rates: %1.0f Hz'%AvgRates('A32'))
    print('==================================================')


def plot_scan(fname):
    data=np.loadtxt(fname, delimiter='\t', unpack=True)

    plot_title = 'ADC Threshold Scan'
    plot_label = 'Trigger Rate'
    if 'AW' in fname:
        thr,raw,trig = data
        plot_title += ' TPC AW'
        plot_label += ' MLU'
        x_ticks=range(-31000,1000,1000)
        plt.gca().invert_xaxis()
        raw_label = 'ADC time avg.'
    elif 'BV' in fname:
        thr = data[0]
        raw = data[5]
        trig = data[6]
        plot_title += 'Barrel Veto'
        plot_label += ' BV mult'
        raw_label = 'BV grandOR'
        x_ticks=range(0,31000,1000)

    #plt.plot(thr,raw,'.',label='ADC time avg.')
    #plt.plot(thr,trig,'.',label=plot_label)
    plt.semilogy(thr,raw,'.',label=raw_label)
    plt.semilogy(thr,trig,'.',label=plot_label)
    plt.title(plot_title)
    plt.xlabel('Threshold: ADC counts above pedestal')
    plt.xticks(x_ticks,fontsize=8)
    plt.ylabel('Counting Rate in Hz')
    plt.grid(True)
    plt.legend(loc='best')

    fig=plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    fig.tight_layout()
    fig.savefig(fname[:-4]+'.png')
    plt.show()


def BVscan():
    # thr_list = [780, 820, 860, 900, 940, 980, 1050, 1100, 
    #             1200, 1400, 1600, 1800, 2000, 2500, 3000, 
    #             4000, 5000, 6000, 8000, 8192, 11000, 15000, 
    #             20000, 30000]

    # thr_list = [30000, 20000, 1024*16, 1024*8, 1024*7, 1024*6, 1024*5, 1024*4, 1024*3, 512*5, 512*4, 256*7, 256*6, 256*5, 256*4, 128*7, 64*13, 64*12, 64*11, 64*10, 64*9, 64*8, 64*7, 64*6]
    thr_list = [8000, 4000, 2000, 1600, 1400, 1200, 1000, 950, 900, 850, 800, 750, 700, 650, 600, 550, 540, 530, 520, 510, 500, 490, 480, 470, 460, 450, 440, 430, 420, 410, 400, 390, 380, 370, 360, 350, 300, 200]
    #thr_list = [1600, 1000, 600, 400, 200, 100, 10]
    #thr_list = [400, 400, 400]

    # for high counting rate, fectrl scaler readout period 
    # has to be changed from 10 sec to 1 sec
    SetROperiod1()

    print('Start...',flush=True)

    fname=path+'BV_'+time.strftime("%Y%b%d_%H%M", time.localtime())+'.dat'
    print(fname,flush=True)

    f=open(fname, 'w')
    f.write("#gnuplot plot [][] 'tmp.dat' u 1:8 w lp t 'adc09', '' u 1:9 w lp t 'adc10', '' u 1:10 w lp t 'adc11', '' u 1:11 w lp t 'adc12', '' u 1:12 w lp t 'adc13', '' u 1:13 w lp t 'adc14', '' u 1:14 w lp t 'adc18', '' u 1:15 w lp t 'adc16'\n")
    f.write("#thr\t1+\t2+\t3+\t4+\tBVgOR\tBVmult\tadc16rates\n")
    for thr in thr_list:
        print('set:',thr, end=" ",flush=True)
        
        # set the new threshold and wait for plateau
        SetADC16threshold(thr)

        print(" done, sleeping....",flush=True)
        time.sleep(10)
        
        # read the trigger rates and write them to file
        a16thr=ReadADC16threshold()
        # rates = TimeAverageBVmult()
        scalers = (
            8,                  # adc16 mult1
            9,                  # adc16 mult2
            10,                 # adc16 mult3
            11,                 # adc16 mult4
            72,                 # Bsc grandOR
            73,                 # Bsc mult
            16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31)
        # rates = TimeMedianBVmult()
        rates = TimeMedianRates(scalers)
        string = str(a16thr)
        s = (str(a16thr)+' \t'+' \t'.join(['%.1f']*len(rates))) % tuple(rates)
        f.write(s+"\n")
        print('  read: ', a16thr, ", rates: ", rates,flush=True)

    # reset the scaler readout period
    SetROperiod10()
    # reset the threshold
    ResetADC16threshold()
        
    f.close()
    print(fname)
    print('Finished!')
    return fname

def AWscan():
    thr_list = [-10000, -9000, -8000, -7000, -6000, -5000, -4000, -3000, -2000, -1900, -1800, -1700, -1600, -1500, -1400, -1300, -1200, -1100, -1000]
#-780, -820, -860, -900, -940, -980, -1050, -1100,
                #-1200, -1400, -1600, -1800, -2000, -2500, -3000,
                #-4000, -5000, -6000, -8000, -8192, -11000, -15000,
                #-20000, -30000]

    # for high counting rate, fectrl scaler readout period 
    # has to be changed from 10 sec to 1 sec
    SetROperiod1()

    print('Start...')

    fname=path+'AW_'+time.strftime("%Y%b%d_%H%M", time.localtime())+'.dat'
    print(fname)

    f=open(fname, 'w')
    f.write("#thr\tadc32gOR\tawmlu\tcoinc\tadc32rates\n")
    for thr in thr_list:
        print('set:',thr, end=" ")
        
        # set the new threshold and wait for plateau
        SetADC32threshold(thr)
        time.sleep(20)
        
        # read the trigger rates and write them to file
        a32thr=ReadADC32threshold()
        # a32rate=TimeAverageRate('A32')
        # trig=ReadMLUrate()
            
        # f.write('%d\t%1.1f\t%1.1f\n'%(a32thr,a32rate,trig))
        # print('  read: %d    ADC32 time avg.rate: %1.1f Hz   mlu rate: %1.1f Hz'%(a32thr,a32rate,trig))
        scalers = (
            5,                  # adc32_grand_or
            67,                 # aw mlu
            74,                 # coincidence
            32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47 # indiv. adc32 scalers
        )

        # rates = TimeMedianBVmult()
        rates = TimeMedianRates(scalers)
        string = str(a32thr)
        s = (str(a32thr)+' \t'+' \t'.join(['%.1f']*len(rates))) % tuple(rates)
        f.write(s+"\n")
        print('  read: ', a32thr, ", rates: ", rates,flush=True)

    # reset the scaler readout period
    SetROperiod10()
    # reset the threshold
    ResetADC32threshold()
        
    f.close()
    print(fname)
    print('Finished!')
    return fname
    

################################################################################

if __name__ == "__main__":
    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())

    Verbose()

    adc='a32'
    if len(sys.argv) == 2:
        adc = sys.argv[1]

    print('Scanning',adc)

    if adc == 'aw' or adc == 'AW' or adc == 'FMC' or adc == 'FMC32' or adc == 'fmc' or adc == 'fmc32' or adc == 'a32':
        fname = AWscan()
    elif adc == 'a16' or adc == 'A16' or adc == 'bv' or adc == 'BV':
        fname = BVscan()
    elif '.dat' in adc:
        print("Just plotting existing file.")
        plot_scan(adc)
        sys.exit()
    else:
        sys.exit('Unknown adc %s'%adc)

    # plot_scan(fname)

    Verbose()
