#!/usr/bin/env python

from __future__ import print_function
import sys
import socket
import requests
import json
import os
import time
from pythonMidas import (getValue, getString)
import smtplib

def send_email(subject, text):
    try:
        sender = 'alphag@alphagdaq.cern.ch'
        receivers = ['lmartin@triumf.ca', 'acapra@triumf.ca', 'fujiwara@triumf.ca', 'eojmckenna@gmail.com', 'olchansk@triumf.ca',# 'nmassacret@triumf.ca'
'ina.carli@cern.ch', 'daniel.duque@cern.ch', 'pooja.woosaree@ucalgary.ca']
        
        message = """From: alphag@alphagdaq.cern.ch
Subject: %s

%s
""" % ( subject, text )
        
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, message)         
        print("Successfully sent email")
        return True
    # except SMTPException:
    except: # return false on any error, not just smtp
        print("Failed to send email")
        return False

def hvOff():
    url='http://localhost:8080?mjsonrpc'
    cmd='turn_off'
    args='all'
    headers={'Content-Type':'application/json','Accept':'application/json'}
    
    par={'client_name':'fecaen_hvps01','cmd':cmd,'args':args}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id': 0}
    resa=requests.post(url, json=payload, headers=headers).json()
    print('turn_off reply:', resa['result']['reply'])
    return(resa['result']['reply'] == 'OK')

def get_lock(process_name):
    # Without holding a reference to our socket somewhere it gets garbage
    # collected when the function exits
    get_lock._lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    logdir = getString("/Logger/Message dir")
    
    with open(logdir+"/hvOff.log", 'a') as log:
        log.write(time.asctime())
        try:
            get_lock._lock_socket.bind('\0' + process_name)
            print('I got the lock')
            log.write(" New lock\n")
            return True
        except socket.error:
            print('lock exists')
            log.write(" Lock exists\n")
            return False

####################################################################################

if __name__ == '__main__':

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())

    interval = getValue("/Alarms/Alarms/Gas Flow/Check Interval")
    interval = max(interval, getValue("/Alarms/Classes/CriticalHV/Execute Interval"))

    logdir = getString("/Logger/Message dir")
    log = open(logdir+"/hvOff.log", 'a')

    use_lock_system = False         # using averaging in frontend, so no need for persistent script

    if(use_lock_system and get_lock('midas_hv_interlock')):
        time.sleep(interval*1.1)
    else:
        log.write(time.asctime())
        # print 'There is one of me already'
        print('Turning off HV')
        mailsuccess = False
        if(hvOff()):
            print('Success!')
            log.write(" HV turned off.\n")
            mailsuccess = send_email('Gas flow low', 'Gas flow was too low, HV turned off. System safe.')
        else:
            print('Failed!!')
            log.write(" HV didn't turn off!\n")
            mailsuccess = send_email('ALARM! Gas flow low and HV on', 'Gas flow was too low, failed to turn off HV!\nPlease turn off manually by pressing the "Turn all off" button on https://alphacpc05.cern.ch/agdaq/?cmd=custom&page=HVPS')

        log.write(time.asctime())
        if(mailsuccess):
            log.write(" Mail sent.\n")
        else:
            log.write(" Mail error.\n")
