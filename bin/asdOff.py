#!/usr/bin/env python3

import requests
import json


def ASDoff():
    url='http://localhost:8080?mjsonrpc'
    cmd='turn_off'
    headers={'Content-Type':'application/json','Accept':'application/json'}

    cha='9'
    chb='11'

    par={'client_name':'fewiener_lvps01','cmd':cmd,'args':cha}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id': 0}
    resa=requests.post(url, json=payload, headers=headers).json()
    print(f"ch: {cha} {resa['result']['reply']}")

    par={'client_name':'fewiener_lvps01','cmd':cmd,'args':chb}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id': 0}
    resb=requests.post(url, json=payload, headers=headers).json()
    print(f"ch: {chb} {resb['result']['reply']}")

    return (resa['result']['reply'] == 'OK') and (resb['result']['reply'] == 'OK')


################################################################################


import sys
import socket

from hvOff import send_email


if __name__ == '__main__':

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())

    if ASDoff():
        print('RPC Success!')
        mailsuccess = send_email('ASD Temperature Critical', 'ASD are now off. Please investigate.')
    else:
        print('RPC Failed!!')
        mailsuccess = send_email('ALARM! ASD Temperature Critical', 'Please turn off manually by pressing the "BS ASD off" button on https://alphacpc05.cern.ch/agdaq/?cmd=custom&page=LVPS')

    
    if(mailsuccess):
        print(" Mail sent.\n")
    else:
        print(" Mail error.\n")
