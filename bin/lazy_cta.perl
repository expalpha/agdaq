#!/usr/bin/perl -w
my $confCTAPath = "/eos/ctapublicdisk/archive/alpha/alphag_2022/data";
#my $confCTAPath = "/eos/ctapublicdisk/archive/lep/alpha/alphag_2022/data";
my $confXrdcpPath = "root://eosctapublicdisk/" . $confCTAPath;

$| = 1;

my $KiB = 1024;
my $MiB = 1024*1024;
my $GiB = 1024*1024*1024;

my $eos = "/usr/bin/eos"; # note - overwritten below
die "Program eos [$eos] does not exist: $!\n" unless -x $eos;

# eosls should happen from alphacdr
$eosls = "ssh -x alphacdr\@alphagdaq \"export EOS_MGM_URL=root://eosctapublicdisk && /usr/bin/eos ls";

my $xrdcp = "/usr/bin/xrdcp";
die "Program xrdcp [$xrdcp] does not exist: $!\n" unless -x $xrdcp;

# rfcp has to run from alphacdr to satisfy CTA access permission - files are owned by alphacdr, not alpha.
$xrdcp = "ssh -x alphacdr\@alphagdaq /usr/bin/xrdcp --nopbar";

my $file = shift @ARGV;
my @file = split(/\//, $file);
my $fname = pop @file;

die "File [$file] is not readable: $!\n" unless -r $file;

my $size = -s $file;

print "Backup $fname $size bytes $file\n";

open(MYOUTFILE, ">>/tmp/file.out");
print MYOUTFILE "Backup $fname $size bytes $file\n";

my $isThere = checkFile($fname, $size);
 
if ($isThere)
  {
    # nothing to do
    print "File $fname already in CTA!\n";
    exit 0;
  }

my $dfile = $confXrdcpPath . "/" . $fname;

print "Backup $fname $size bytes $file to $dfile\n";

my $cmd = "/usr/bin/time $xrdcp $file $dfile 2>&1";
print "Run $cmd\n";
system $cmd;

my $check = checkFile($fname, $size);

if (!$check)
  {
    print "Cannot confirm that file was copied to CTA!\n";
    exit 1;
  }

exit 0;

sub checkFile
  {
    my $file = shift @_;
    my $size = shift @_;

    my $cmd = "$eosls -l $confCTAPath/$file \"";
    print "Run $cmd\n";
    my $ls = `$cmd 2>&1`;
#    print "[$ls]\n";

    return 0 if ($ls =~ /No such file or directory/);

    my ($perm, $nlink, $uid, $gid, $xsize) = split(/\s+/, $ls);
#    print "size [$size] [$xsize]\n";
    return 1 if ($size == $xsize);
    
    # file not found, or wrong size
    return 0;
  }

#end
