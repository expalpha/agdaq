#!/bin/sh

rsync -avz --progress --exclude 'agdaq/.local' --exclude 'core.*' --exclude '.nfs*' --exclude 'agdaq/online/bin/google_speech/cache' --exclude 'tree*.root' --exclude 'output*.root' --exclude 'run*.mid.lz4' --exclude 'agdaq/.cache' --exclude 'agdaq/.ccache' --exclude 'agtdc/.ssh' --exclude 'agtdc/.emacs.d' --exclude 'agtdc/.dbus' --exclude 'agtdc/.config' --exclude 'agtdc/.pki' --exclude 'agtdc/.Xauthority' --exclude 'agtdc/.bash_history' --exclude 'agtdc/.viminfo' --delete-after --delete-excluded /zssd/home1/agdaq /zssd/home1/agtdc alpha@alpha00.triumf.ca:/z6tb/cern_backups/alphagdaq-home

#
